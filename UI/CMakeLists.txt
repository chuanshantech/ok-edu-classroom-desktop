project(UI)

set(${PROJECT_NAME}_SOURCES
        WindowManager.h
        WindowManager.cpp
        resources.cpp

        ./dialog/VideoPlayerDialog.cpp
        ./dialog/DialogUtils.cpp
        ./dialog/progressdialog.cpp

#        ./multimedia/Camera.cpp

        ./dialog/VideoPlayerDialog.ui
        ./window/page/PageClassing.ui

        component/OMainMenu.ui
        component/OMainMenu.h
        component/OMainMenu.cpp




        component/OMainTitleBar.ui
        component/OMainTitleBar.h
        component/OMainTitleBar.cpp
        component/OTitleClose.ui
        component/OTitleClose.cpp
        component/OPlayerController.ui
        component/OPlayerController.cpp

        component/OVideoPlayer.ui
        component/OVideoPlayer.cpp
        component/OVideoViewport.ui
        component/OVideoViewport.cpp
        component/OVideoWidget.ui
        component/OVideoWidget.cpp
        component/OTitleBar.ui
        component/OTitleBar.cpp

        component/OCountDown.ui
        component/OCountDown.cpp

        component/OPainterToolBox.ui
        component/OPainterToolBox.cpp
        component/OPainterViewport.ui
        component/OPainterViewport.h
        component/OPainterViewport.cpp
        component/OPlayerWidget.ui
        component/OPlayerWidget.cpp
        component/OPainterColorPanel.ui
        component/OPainterColorPanel.cpp

        )

add_subdirectory(core)
add_subdirectory(window)
add_subdirectory(widget)
# add_subdirectory(resources)

add_library(${PROJECT_NAME} ${${PROJECT_NAME}_SOURCES})

if(MSVC)
    set_property(TARGET ${PROJECT_NAME} PROPERTY MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
endif (MSVC)