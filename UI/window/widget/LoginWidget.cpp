﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/

#include "LoginWidget.h"

#include <QShortcut>
#include <QTranslator>
#include <QWidget>
#include <memory>
#include <QPaintEvent>

#include "base/basic_types.h"
#include "base/logs.h"
#include "base/widgets.h"
#include "core/SettingManager.h"
#include "session/AuthSession.h"
#include "settings/translator.h"
#include "src/persistence/settings.h"

#include "ui_LoginWidget.h"

namespace UI {
namespace widget {

using namespace core;
using namespace network;
using namespace session;


LoginWidget::LoginWidget(QWidget *parent)
    : QWidget(parent), //                                        //
      ui(new Ui::LoginWidget), //
      _session(session::AuthSession::Instance()),  //
      m_loginKey(nullptr), //
      delayCaller_(std::make_unique<base::DelayedCallTimer>()) //
{
  qDebug() << "初始化开始...";

  ui->setupUi(this);

  //==========国际化==========//
  // 先获取当前语言
  Settings &s = Settings::getInstance();
  qDebug() << "Settings translation:" << s.getTranslation();

  for (int i = 0; i < s.getLocales().size(); ++i) {
    auto &locale = s.getLocales().at(i);
    QString langName = QLocale(locale).nativeLanguageName();
    ui->language->addItem(langName);
  }
  qDebug() << "Added languages ComboBox count is:" << ui->language->count();

  // 当前语言状态
  auto i = s.getLocales().indexOf(s.getTranslation());
  qDebug() << "Set current index for language=>" << i;

  if (i >= 0 && i < ui->language->count())
    ui->language->setCurrentIndex(i+1);

  settings::Translator::registerHandler(std::bind(&LoginWidget::retranslateUi, this), this);
  retranslateUi();
  //==========国际化==========//



  connect(static_cast<AuthSession *>(_session), &AuthSession::loginResult, //
          this, &LoginWidget::onConnectResult);

  m_loginKey = new QShortcut(QKeySequence(Qt::Key_Return), this);
  connect(m_loginKey, SIGNAL(activated()), //
          this, SLOT(on_loginBtn_released()));

  // 初始化
  init();

  show();
  qDebug() << "初始化完成";
}

LoginWidget::~LoginWidget() {
  DEBUG_LOG(("~LoginWidget"))

  //卸载语言处理器
  settings::Translator::unregister(this);


  disconnect(m_loginKey);
  delete m_loginKey;

  disconnect(static_cast<AuthSession *>(_session));
  delete static_cast<AuthSession *>(_session);
}

void LoginWidget::init() {
  m_settingManager = SettingManager::InitGet();
  m_settingManager->getAccount([=](QString acc, QString password) {
    ui->rember->setChecked(!acc.isEmpty());
    ui->accountInput->setText(acc);
    ui->passwordInput->setText(password);
  });
}

void LoginWidget::doLogin() {

  // 登录功能
  DEBUG_LOG(("doLogin..."));
  auto sess = static_cast<AuthSession *>(_session);
  if (!sess) {
    return;
  } // 对登录时状态判断
  auto status = sess->status();
  DEBUG_LOG(("status:%1").arg(status));
  switch (status) {
  case SUCCESS: {
    DEBUG_LOG(("SUCCESS ..."));
    return;
  }
  case CONNECTING: {
    DEBUG_LOG(("CONNECTING ..."));
    //    sess->interrupt();
    return;
  }
  default: {
    DEBUG_LOG(("Login ..."));
    // 登陆
    QString account(ui->accountInput->text());
    QString password(ui->passwordInput->text());
    // 对账号和密码判断
    if (account.isEmpty()) {
      return;
    }
    if (ui->rember->isCheckable()) {
      m_settingManager->saveAccount(account, password);
    } else {
      m_settingManager->clearAccount();
    }

    sess->doLogin(SignInInfo{account, password});
    break;
  }
  }
}

void LoginWidget::onConnectResult(session::SignInInfo &info,
                                  session::LoginResult &result) {

  DEBUG_LOG(("status:%1 msg:%2").arg(result.status).arg(result.msg));
  switch (result.status) {
  case NONE:
    break;
  case CONNECTING: {
    DEBUG_LOG(("%1=>CONNECTING").arg(CONNECTING));
    ui->loginMessage->setText(tr("..."));
    QString account(ui->accountInput->text());
    QString password(ui->passwordInput->text());
    emit loginFailed(account, password);
    break;
  }
  case SUCCESS: {
    DEBUG_LOG(("%1=>SUCCESS").arg(SUCCESS));
    ui->loginMessage->setText(tr("login success"));
    QString account(ui->accountInput->text());
    QString password(ui->passwordInput->text());
    emit loginSuccess(account, password);
    break;
  }
  case FAILURE:
    ui->loginMessage->setText(result.msg);
    break;
  }
  emit loginResult(info, result);
}

void LoginWidget::on_loginBtn_released() { doLogin(); }

void LoginWidget::on_language_currentIndexChanged(int index) {
  qDebug() << "Select language:" << index;

  if(index == 0) {
    return ;
  }

  Settings &s = Settings::getInstance();
  const QString &locale = s.getLocales().at(index-1);
  qDebug() << "Selected locale:" << locale;

  s.setTranslation(locale);

  qDebug() << "Translate locale:" << locale;
  settings::Translator::translate(OK_UIWindow_MODULE, locale);

}

void LoginWidget::retranslateUi() {
  ui->retranslateUi(this);
}

void LoginWidget::onError(const QString& msg) {
  ui->loginMessage->setText(msg);
}

} // namespace widget
} // namespace UI
