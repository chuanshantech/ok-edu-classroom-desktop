﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/

#pragma once

#include "core/SettingManager.h"
#include "session/AuthSession.h"
#include "window/widget/OWidget.h"

class QShortcut;
class QPaintEvent;

namespace Ui {
class LoginWidget;
}

namespace UI {

namespace widget {

using namespace network;
using namespace core;

class LoginWidget : public QWidget {
  Q_OBJECT
public:
  explicit LoginWidget(QWidget *parent = nullptr);
  ~LoginWidget() override;
  void onError(const QString &msg);

private:
  Ui::LoginWidget *ui;

  QShortcut *m_loginKey;

  void *_session;

  SettingManager *m_settingManager = nullptr;
  std::unique_ptr<base::DelayedCallTimer> delayCaller_;

public:
  void init();
  void showMainWindow();

protected:
  void retranslateUi();

signals:
  void loginSuccess(QString name, QString password);
  void loginFailed(QString name, QString password);
  void loginTimeout(QString name);
  void loginResult(session::SignInInfo &info, session::LoginResult &result);

private slots:
  void doLogin();
  void onConnectResult(session::SignInInfo &info, session::LoginResult &result);
  void on_loginBtn_released();
  void on_language_currentIndexChanged(int index);
};

} // namespace widget
} // namespace UI
