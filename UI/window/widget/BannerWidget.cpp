/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/

#include "BannerWidget.h"
#include <QWidget>

#include "r.h"
#include "ui_BannerWidget.h"
#include "window/BaseWindow.h"
#include <QPainter>
#include <QSvgRenderer>
#include <base/logs.h>
#include <base/widgets.h>

namespace UI {

namespace widget {

BannerWidget::BannerWidget(QWidget *parent)
    : QWidget(parent), ui(std::make_unique<Ui::BannerWidget>()) {
  ui->setupUi(this);

  ui->imgBox->setGeometry(rect());

  setLogo();
}

BannerWidget::~BannerWidget() {}

void BannerWidget::setLogo() {
  // Prepare a QImage with desired characteritisc
  QImage image = QImage(226, 226, QImage::Format_ARGB32);
  image.fill(Qt::transparent);

  QString path = ":/resources/logo/main.svg";
  QSvgRenderer renderer(path);

  QPainter painter(&image);
  renderer.render(&painter);

  QPixmap pix = QPixmap::fromImage(image);
  pix = pix.scaled(image.size(),
                   Qt::KeepAspectRatio,
                   Qt::SmoothTransformation);

  ui->imgBox->setFixedSize(image.size());
  ui->imgBox->setPixmap(pix);
}

} // namespace widget
} // namespace UI
