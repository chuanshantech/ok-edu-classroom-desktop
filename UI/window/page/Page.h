﻿
#pragma once

#include "UI/core/ui.h"
#include <QWidget>

namespace UI {

namespace page {

class Page : public QWidget {
  Q_OBJECT

public:
  Page(QWidget *parent);
  ~Page();
};

} // namespace page
} // namespace UI
