﻿
#ifndef PAGEFACTORY_H
#define PAGEFACTORY_H

#include <QWidget>


namespace UI {
namespace page {

class PageFactory{

public:
    template<typename T>
    static T *Create(QWidget *parent = Q_NULLPTR){
        return new T(parent);
    }

};

}
}

#endif // PAGEFACTORY_H
