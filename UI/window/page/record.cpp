﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#include "record.h"

#include <QWidget>
#include <QShowEvent>
#include <QUrl>

#include <base/basic_types.h>

#include "UI/core/ui.h"
#include "window/widget/OWebView.h"

#include "r.h"

namespace UI
{
	namespace page
	{

		using namespace widget;

#define RECORD_URL (BACKEND_BASE_URL "/record.do")

		RecordViewer::RecordViewer(QWidget *parent) : OWebView(parent)
		{
			if (parent)
				setGeometry(parent->contentsRect());
		}

		RecordViewer::~RecordViewer()
		{
		}

		void RecordViewer::showEvent(QShowEvent *e)
		{
			Q_UNUSED(e);

			QUrl url(RECORD_URL);
			openWeb(url);
		}

		Record::Record(QWidget *parent) : Page(parent)
		{
			setObjectName(qsl("Page:%1").arg(static_cast<int>(PageMenu::record)));

			if (parent)
			{
				setGeometry(parent->geometry());
			}
			// setFixedHeight(MAIN_HEIGHT - TOP_BAR_HEIGHT);
			// setFixedWidth(500);

			_viewer = new RecordViewer(this);
			_viewer->setGeometry(geometry());
		}

		Record::~Record()
		{
		}

	}

}
