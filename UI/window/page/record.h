
#pragma once

#include <QWidget>

#include "window/page/Page.h"
#include "window/page/record.h"
#include "window/widget/OWebView.h"

namespace UI
{
	namespace page
	{

		using namespace widget;

		class RecordViewer : public OWebView
		{
		public:
			RecordViewer(QWidget *parent = nullptr);
			~RecordViewer();

		protected:
			virtual void showEvent(QShowEvent *e) override;

		private:
		};

		class Record : public Page
		{
			Q_OBJECT
		public:
			Record(QWidget *parent = nullptr);
			~Record();

		private:
			RecordViewer *_viewer;
		};

	}
}
