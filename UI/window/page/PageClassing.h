﻿#ifndef PAGECLASSING_H
#define PAGECLASSING_H

#include <QFrame>


class OVideoViewport;

namespace Ui {
class PageClassing;
}


namespace UI {
namespace page {

class PageClassing : public QFrame
{
    Q_OBJECT

public:
    explicit PageClassing(QWidget *parent = nullptr);
    ~PageClassing();

    virtual const OVideoViewport* videoViewport() const;

    void toggleChat(bool checked);

private:
    Ui::PageClassing *ui;
};


}
}

#endif // PAGECLASSING_H
