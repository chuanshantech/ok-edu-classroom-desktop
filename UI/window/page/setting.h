/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#pragma once

#include <memory>
#include <vector>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QWidget>

#include "window/page/Page.h"
#include "window/widget/SettingView.h"

namespace UI
{
	namespace page
	{

		using namespace widget;

		struct SettingMenu
		{
			QString name;
			QString text;
			SettingViewMenu menu;
		};

		class Setting : public Page
		{
			Q_OBJECT
		public:
			Setting(QWidget *parent = nullptr);
			~Setting();

			virtual void showEvent(QShowEvent *);

		private:
			std::vector<SettingMenu> menus;

			bool inited = false;

			std::unique_ptr<QVBoxLayout> _vLayout;
			std::unique_ptr<QHBoxLayout> _hLayout;

			QGridLayout *_gLayout;

			std::unique_ptr<SettingView> _stack_view;

		signals:
			void view(widget::SettingViewMenu menu);

		public slots:
			void onMenu(int id);
		};

	}
}
