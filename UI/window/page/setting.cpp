﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#include "setting.h"

#include <memory>
#include <QHBoxLayout>
#include <QWidget>
#include <QSpacerItem>
#include <QButtonGroup>
#include <QPushButton>
#include <QLabel>

#include <base/basic_types.h>
#include <base/widgets.h>

#include "r.h"
#include "resources.h"

#include "UI/core/ui.h"
#include "window/page/Page.h"


#include "window/widget/SettingItem.h"
#include "window/widget/SettingView.h"

namespace UI
{
	namespace page
	{

		using namespace widget;

		Setting::Setting(QWidget *parent) : Page(parent)
		{
			menus = {
				{("setting_olc"), tr("setting_olc"), SettingViewMenu::OLC},				//
				{("setting_testing"), tr("setting_testing"), SettingViewMenu::TESTING}, //
				{("setting_voice"), tr("setting_voice"), SettingViewMenu::VOICE},		//
				{("setting_theme"), tr("setting_theme"), SettingViewMenu::THEME},		//
				{("setting_help"), tr("setting_help"), SettingViewMenu::HELP},			//
				{("setting_exit"), tr("setting_exit"), SettingViewMenu::EXIT},			//
			};

			if (parent)
			{
				setGeometry(parent->contentsRect());
			}

			setObjectName(qsl("Page:").arg(static_cast<int>(PageMenu::setting)));

			base::Widgets::SetPalette(this, QPalette::Background, QColor(DEFAULT_BG_COLOR));

			_vLayout = std::make_unique<QVBoxLayout>(this);
			_vLayout->setMargin(0);
			_vLayout->setSpacing(0);
			_vLayout->setDirection(QBoxLayout::TopToBottom);

			QLabel *header = new QLabel(this);
			header->setFixedWidth(this->width());
			header->setFixedHeight(PAGE_HEADER_HEIGHT);
			header->setText(tr("setting_header"));
			header->setAlignment(Qt::AlignCenter);
			header->setStyleSheet("background-color: #535353; color:white; font-weight: bold;");

			_vLayout->addWidget(header);

			_hLayout = std::make_unique<QHBoxLayout>(this);
			_hLayout->setDirection(QBoxLayout::LeftToRight);
			_hLayout->setMargin(0);
			_hLayout->setSpacing(0);

			_vLayout->addLayout(_hLayout.get());

			_gLayout = new QGridLayout(this);
			_gLayout->setMargin(0);
			_gLayout->setSpacing(0);

			QButtonGroup *buttonGroup = new QButtonGroup();
			buttonGroup->setExclusive(true);

			int count = menus.size();
			for (int i = 0; i < count; i++)
			{

				int r = i / 2 + 1;
				int c = i % 2 + 1;

				SettingItem *_settingBtn = new SettingItem(this);
				_settingBtn->setObjectName(menus[i].name);
				_settingBtn->setText(menus[i].text);

				buttonGroup->addButton(_settingBtn, i);

				_gLayout->addWidget(_settingBtn, r, c);
			}

			//左侧网格
			_hLayout->addLayout(_gLayout);

			_hLayout->addSpacerItem(new QSpacerItem(10, 10));

			//右侧试图
			_stack_view = std::make_unique<SettingView>(this);
			_hLayout->addWidget(_stack_view.get(), Qt::AlignLeft);
			_hLayout->addStretch();

			_vLayout->addStretch();

			//连接信号
			connect(buttonGroup, SIGNAL(buttonClicked(int)), this, SLOT(onMenu(int)));
			connect(this, SIGNAL(view(UI::widget::SettingViewMenu)), _stack_view.get(), SLOT(onSwitchView(UI::widget::SettingViewMenu)));

			//设置样式
			QString qss = utils::Resources::loadQss(utils::QSS::setting);
			setStyleSheet(qss);
		}

		Setting::~Setting()
		{
		}

		void Setting::showEvent(QShowEvent *)
		{
			if (inited)
				return;
		}

		void Setting::onMenu(int id)
        {//信号槽
			SettingMenu menu = menus[id];
			_stack_view->onSwitchView(menu.menu);
		}
	}
}
