﻿#include "PageClassing.h"
#include "ui_PageClassing.h"

#include "UI/core/ui.h"
#include <base/basic_types.h>

namespace UI {
namespace page {

PageClassing::PageClassing(QWidget *parent)
    : QFrame(parent), ui(new Ui::PageClassing) {
  ui->setupUi(this);
  setObjectName(qsl("Page:%1").arg(static_cast<int>(PageMenu::classing)));
}

PageClassing::~PageClassing() { delete ui; }

const OVideoViewport *PageClassing::videoViewport() const {
  return ui->video_viewport;
}

void PageClassing::toggleChat(bool checked) {
  ui->painter_viewport->toggleChat(checked);
}

} // namespace page
} // namespace UI
