﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#include "OPainterToolBox.h"
#include "ui_OPainterToolBox.h"

#include "base/logs.h"

OPainterToolBox::OPainterToolBox(QWidget *parent)
    : QWidget(parent), ui(new Ui::OPainterToolBox),
      _delayCaller(std::make_unique<base::DelayedCallTimer>()) {
  ui->setupUi(this);

  connect(ui->color_panel_pen, &OPainterColorPanel::colorChange, this,
          &OPainterToolBox::onPenColorChange);

  connect(ui->color_panel_pen, &OPainterColorPanel::weightChange, this,
          &OPainterToolBox::onPenWeightChange);

  connect(ui->color_panel_text, &OPainterColorPanel::colorChange, this,
          &OPainterToolBox::onTextColorChange);

  connect(ui->color_panel_text, &OPainterColorPanel::weightChange, this,
          &OPainterToolBox::onTextWeightChange);

  ui->toolbox_pen->installEventFilter(this);
  ui->toolbox_text->installEventFilter(this);


  setMouseTracking(true);
}

OPainterToolBox::~OPainterToolBox() { delete ui; }

void OPainterToolBox::init() {
  DEBUG_LOG_S(L_INFO) << "init";

  _delayCaller->call(100, [this]() {
    // init
    ui->color_panel_pen->init();
    ui->color_panel_text->init();

    ui->color_panel_pen->hide(true);
    ui->color_panel_text->hide(true);

    ui->toolbox_move->click();
  });
}

void OPainterToolBox::on_toolbox_move_clicked(bool checked) {
  if (!checked)
    return;
  DEBUG_LOG_S(L_INFO) << checked;

  ui->toolbox_move->setChecked(checked);
  ui->toolbox_text->setChecked(false);
  ui->toolbox_pen->setChecked(false);
  ui->toolbox_delete->setChecked(false);
  ui->toolbox_cutter->setCheckable(false);

  emit toolChange(painter::ToolboxType::P_MOVE);
}

void OPainterToolBox::on_toolbox_text_clicked(bool checked) {
  if (!checked)
    return;
//   DEBUG_LOG_S(L_INFO) << checked;

  ui->toolbox_move->setChecked(false);
  ui->toolbox_text->setChecked(checked);
  ui->toolbox_pen->setChecked(false);
  ui->toolbox_delete->setChecked(false);
  ui->toolbox_cutter->setCheckable(false);

  emit toolChange(painter::ToolboxType::P_TEXT);
}

void OPainterToolBox::on_toolbox_pen_clicked(bool checked) {
  if (!checked)
    return;
  // DEBUG_LOG_S(L_INFO) << checked;
  ui->toolbox_move->setChecked(false);
  ui->toolbox_text->setChecked(false);
  ui->toolbox_pen->setChecked(checked);
  ui->toolbox_delete->setChecked(false);
  ui->toolbox_cutter->setCheckable(false);

  emit toolChange(painter::ToolboxType::P_PEN);
}

void OPainterToolBox::on_toolbox_delete_clicked(bool checked) {
  // DEBUG_LOG_S(L_INFO) << checked;
  emit toolChange(painter::ToolboxType::P_REMOVE);
}

void OPainterToolBox::on_toolbox_cutter_clicked(bool checked) {
  // DEBUG_LOG_S(L_INFO) << checked;
  emit toolChange(painter::ToolboxType::P_MOVE);
}

void OPainterToolBox::on_toolbox_cloud_clicked(bool checked) {
  // DEBUG_LOG_S(L_INFO) << checked;
  emit toolChange(painter::ToolboxType::P_CLOUD);
}

void OPainterToolBox::onTextColorChange(QColor color) {
  // DEBUG_LOG_S(L_INFO) << "sender:" << sender() << " color:" << color;
  on_toolbox_text_clicked(true);
  emit textColorChange(color);
}

void OPainterToolBox::onTextWeightChange(int weight) {
  // DEBUG_LOG_S(L_INFO) << "sender:" << sender() << " weight:" << weight;
  on_toolbox_text_clicked(true);
  emit textWeightChange(weight);
}

void OPainterToolBox::onPenColorChange(QColor color) {
  // DEBUG_LOG_S(L_INFO) << "sender:" << sender() << " color:" << color;
  on_toolbox_pen_clicked(true);
  emit penColorChange(color);
}

void OPainterToolBox::onPenWeightChange(int weight) {
  // DEBUG_LOG_S(L_INFO) << "sender:" << sender() << " weight:" << weight;
  on_toolbox_pen_clicked(true);
  emit penWeightChange(weight);
}

bool OPainterToolBox::eventFilter(QObject *target, QEvent *event) {
  //找到信号发送者
  QToolButton *button = qobject_cast<QToolButton *>(target);
  if (button) {
    if (button == ui->toolbox_text) {
      if (event->type() == QEvent::Type::HoverEnter) {
        ui->color_panel_text->show();
        ui->color_panel_pen->hide(true);
      } else if (event->type() == QEvent::Type::HoverLeave) {
        _delayCaller->call(400, [this] { ui->color_panel_text->hide(false); });
      }
    } else if (button == ui->toolbox_pen) {
      if (event->type() == QEvent::Type::HoverEnter) {
        ui->color_panel_pen->show();
        ui->color_panel_text->hide(true);
      } else if (event->type() == QEvent::Type::HoverLeave) {
        _delayCaller->call(400, [this] { ui->color_panel_pen->hide(false); });
      }
    }
  }
  return QWidget::eventFilter(target, event);
}

void OPainterToolBox::enterEvent(QEvent *event) {}

void OPainterToolBox::leaveEvent(QEvent *event) {}

void OPainterToolBox::mouseMoveEvent(QMouseEvent *e) {

  if (this->parentWidget()) {
    this->setAttribute(Qt::WA_TransparentForMouseEvents, true);
    QPoint pt = this->mapTo(this->parentWidget(), e->pos());
    QWidget *w = this->parentWidget()->childAt(pt);
    if (w) {
      pt = w->mapFrom(this->parentWidget(), pt);
      QMouseEvent *event = new QMouseEvent(e->type(), pt, e->button(),
                                           e->buttons(), e->modifiers());
      QApplication::postEvent(w, event);
    }
    this->setAttribute(Qt::WA_TransparentForMouseEvents, false);
  }
}
