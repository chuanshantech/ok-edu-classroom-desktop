﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#include "OMainMenu.h"
#include "qsize.h"
#include "ui_OMainMenu.h"

#include <QUrl>

#include "lib/network/ImageLoader.h"
#include "lib/network/NetworkManager.h"
#include <memory>

#include "WindowManager.h"

#include "application.h"
#include "lib/network/backend/PushService.h"
#include "lib/network/backend/UserService.h"
#include "modules/im/src/nexus.h"
#include "window/page/PageClassing.h"
#include <QMessageBox>

OMainMenu::OMainMenu(QWidget *parent)
    : QFrame(parent), ui(new Ui::OMainMenu), _menu(PageMenu::welcome),
      _showTimes(0) {

  ui->setupUi(this);

  // LOGO


//  QPixmap p(":/resources/icon/logo.png");
//  ui->label_logo->setPixmap(
//      (p.scaled(60, 60, Qt::KeepAspectRatio, Qt::SmoothTransformation)));

//  connect(this,&OMainMenu::menuPushed,[this](const QByteArray &buf) {
//    QPixmap _pixmap;
//    _pixmap.loadFromData(buf, nullptr, Qt::AutoColor);
//    _pixmap.scaled(3, 3);
//    ui->label_avatar->setPixmap(_pixmap);
//  });

  //
//  connect(this, &OMainMenu::updateAvatar,
//          [this](const QByteArray &buf) {
//    QPixmap _pixmap;
//    _pixmap.loadFromData(buf, nullptr, Qt::AutoColor);
//    _pixmap.scaled(3, 3);
//    ui->label_avatar->setPixmap(_pixmap);
//    emit updateAvatar(buf);
//  });

//    connect(this, &OMainMenu::menuPushed,
//            core::Application::Instance(),
//            &core::Application::onMenuPushed);

  delayCaller_ = (std::make_unique<base::DelayedCallTimer>());
  //  delayCaller_->call(100,[this]() { ui->chatBtn->click(); });
}

OMainMenu::~OMainMenu() { delete ui; }

void OMainMenu::setAvatar(const QPixmap &pixmap){
  ui->label_avatar->setPixmap(pixmap);
}

void OMainMenu::showEvent(QShowEvent *e) {
  Q_UNUSED(e);
  _showTimes++;
  if (_showTimes == 1) {
    updateUI();
  }
}

void OMainMenu::updateUI() {

//  if (_menu == PageMenu::welcome) {
//    onClassing();
//  }

  on_chatBtn_clicked(true);

//  backend::UserService userService;
//  userService.getInfo([this](const backend::UserInfo &info) {
//    if (0 < info.getId()) {
//      const QString &avatarUrl = info.getAvatar();
//      if (!avatarUrl.isEmpty()) {
//        utils::ImageLoader _loader(this);
//        _loader.load(avatarUrl, [this](const QByteArray &buf) { //
//          emit updateAvatar(buf);
//        });
//      }
//    }
//  });

//  backend::PushService pushService;
//  pushService.requestCount([this](const backend::PushInfo &info) {
//    DEBUG_LOG(("pushService.requestCount=>%1").arg(info.getCount()));
//    if (0 < info.getCount()) {
//      ui->label_email_count->setText(QString::number(info.getCount()));
//    }
//  });
}

void OMainMenu::onBook() {}

void OMainMenu::onClassing() {

  ui->classBtn->setChecked(true);
  ui->chatBtn->setChecked(false);
  ui->settingBtn->setChecked(false);

  _menu = PageMenu::classing;
  emit onPage(PageMenu::classing);
}

void OMainMenu::onRecord() {

  ui->classBtn->setChecked(false);
  ui->settingBtn->setChecked(false);
  ui->chatBtn->setChecked(false);

  _menu = PageMenu::record;
  emit onPage(PageMenu::record);
}

void OMainMenu::onCalendar() {

  ui->classBtn->setChecked(false);
  ui->settingBtn->setChecked(false);

  _menu = PageMenu::calendar;
  emit onPage(PageMenu::calendar);
}

void OMainMenu::onEmail() {}

void OMainMenu::on_personalBtn_clicked(bool checked) {}

void OMainMenu::on_chatBtn_clicked(bool checked) {
  DEBUG_LOG(("checked:%1").arg(checked));
  ui->classBtn->setChecked(false);
  ui->chatBtn->setChecked(true);
  emit menuPushed(UI::PageMenu::chat, ui->chatBtn->isChecked());
  emit onPage(UI::PageMenu::chat);
}

void OMainMenu::onSetting() {

  ui->classBtn->setChecked(false);
  ui->settingBtn->setChecked(true);

  _menu = PageMenu::setting;
  emit onPage(PageMenu::setting);
}

void OMainMenu::on_classBtn_clicked(bool checked) { onClassing(); }
