﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#pragma once

#include <QMap>
#include <QObject>
#include <memory>

#include "core/ui.h"
#include "window/page/Page.h"
#include "window/window/MainWindow.h"

namespace UI {

namespace page {
class PageClassing;
}

class WindowManager : public QObject {
  Q_OBJECT
public:
  WindowManager(QObject *parent = nullptr);
  ~WindowManager();

  static WindowManager *Instance();

  void startMainUI();
  void stopMainUI();

  void putPage(PageMenu menu, QFrame *p);

  QFrame *getPage(PageMenu menu);

  page::PageClassing *getPageClassing();

  inline UI::window::MainWindow *window() { return m_mainWindow.get(); }

  QWidget *getContainer(PageMenu menu);

  OMainMenu* getMainMenu();

private:

  std::unique_ptr<UI::window::MainWindow> m_mainWindow;

signals:
  void menuPushed(PageMenu menu, bool checked);
};
} // namespace UI
