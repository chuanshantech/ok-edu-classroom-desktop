﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#include "IMViewport.h"
#include "lib/messenger/message.h"
#include "ui_IMViewport.h"

// #include <base/logs.h>
#include "UI/core/nexus.h"
#include "grouplist.h"
#include "lib/messenger/core.h"
#include "lib/messenger/groupmessagedispatcher.h"
#include "persistence/im/profile.h"
#include "qcommandlineparser.h"
#include <cassert>

namespace UI {
namespace widget {
namespace im {

IMViewport::IMViewport(QWidget *parent)
    : QWidget(parent), ui(new Ui::IMViewport) {

  ui->setupUi(this);
  hide();

  auto core = Nexus::getCore();
  assert(core);

  // uint8_t groupnumber = 1;
  // const auto groupName = tr("Groupchat #%1").arg(groupnumber);
  // // const bool enabled = core->getGroupAvEnabled(groupnumber);
  // lib::IM::GroupId groupId(&groupnumber);

  // lib::IM::Group *newgroup = GroupList::addGroup(
  //     groupnumber, groupId, groupName, true, core->getUsername());

  // lib::IM::MessageProcessor::SharedParams sharedMessageProcessorParams;
  // auto messageProcessor =
  //     lib::IM::MessageProcessor(sharedMessageProcessorParams);

  // auto groupMessageDispatcher =
  //     std::make_shared<lib::IM::GroupMessageDispatcher>(
  //         *newgroup, std::move(messageProcessor), *core, *core);

  auto messenger = lib::IM::Messenger::getInstance();
  auto groupMessageDispatcher = messenger->getGroupMessageDispatcher();

  ui->im_input->setMessageDispatcher(groupMessageDispatcher);
  ui->im_view->setMessageDispatcher(groupMessageDispatcher);
}

IMViewport::~IMViewport() {}

void IMViewport::showEvent(QShowEvent *e) {
  Q_UNUSED(e);
  _showTimes++;
  //   if (_showTimes == 1) {
  //    initIM();
  //   }
}

void IMViewport::on_chat_send_button_clicked(bool checked) {
  IMInputWidget *inputW = ui->im_input;
  inputW->onSend(checked);
}

void IMViewport::on_chat_emoji_button_clicked(bool checked) {
  // if (checked) {
  //   ui->emojiPanel->show();
  // } else
  //   ui->emojiPanel->hide();
}

} // namespace im
} // namespace widget
} // namespace UI
