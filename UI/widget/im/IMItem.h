﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#pragma once

#include <QByteArray>
#include <QHBoxLayout>
#include <QLabel>
#include <QShowEvent>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>
#include <memory>

#include "window/style/MaskLabel.h"

namespace UI {

namespace widget {

enum class ItemLayout {
  NONE = 0,     //无
  AVATAR_LEFT,  //头像左置
  AVATAR_RIGHT, //头像右置
};

class IMItem : public QWidget {
  Q_OBJECT

public:
  IMItem(ItemLayout layout, QString &from, QDateTime &time, QString &text,
         QWidget *parent = nullptr);
  ~IMItem();

  void updateAvatar(QByteArray &buf);

protected:
  virtual void showEvent(QShowEvent *e) override;

private:
  //   std::shared_ptr<QHBoxLayout> _hLayout;
  std::shared_ptr<QVBoxLayout> _vLayout;

  MaskLabel *_avatar = nullptr; //头像
  QLabel *_time = nullptr;      //时间
  QLabel *_text = nullptr;      //消息文本

  QString _from;
};
} // namespace widget
} // namespace UI