﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#pragma once

#include <QBoxLayout>
#include <QFocusEvent>
#include <QFrame>
#include <QLabel>
#include <QScrollArea>
#include <QString>

#include "UI/window/layout/FlowLayout.h"

namespace UI {
namespace widget {

const static QString EMOJI_SIZE = "[24x24x8BPP]";
const static QString EMOJI_SUFFIX = ".gif";
const static int EMOJI_BEGIN = 389;
const static int EMOJI_END = 523;

class IMEmojiPanel : public QWidget {
  Q_OBJECT
public:
  IMEmojiPanel(QWidget *parent = nullptr);
  virtual ~IMEmojiPanel() override;

  // void focusOutEvent(QFocusEvent * e);

protected:
  bool eventFilter(QObject *object, QEvent *e) override;
  void showEvent(QShowEvent *e) override;

private:
  std::unique_ptr<QFrame> _wrap;
  // std::unique_ptr<FlowLayout> _layout;
  std::unique_ptr<FlowLayout> _layout;

  int _showTimes = 0;

  void init();
signals:
  void emojiChange(QString);
  void onEmoji(QString);

public slots:
};

} // namespace widget
} // namespace UI
