﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#include "IMEmojiPanel.h"

#include <QBoxLayout>
#include <QFocusEvent>
#include <QFrame>
#include <QLabel>
#include <QPixmap>
#include <QScrollArea>
#include <QString>
#include <QWidget>
#include <memory>

#include "UI/window/layout/FlowLayout.h"

#include <base/logs.h>
#include <base/utils.h>

namespace UI {
namespace widget {

IMEmojiPanel::IMEmojiPanel(QWidget *parent) : QWidget(parent) {

  if (parent) {
    parent->installEventFilter(this);
  }

  setGeometry(QRect(0, 190, 400, 260));

  //    _wrap = std::make_unique<QFrame>(this);
  //    _wrap->setWindowFlags(Qt::FramelessWindowHint);
  //    _wrap->setFixedWidth(width());
  //    _wrap->setStyleSheet("background-color:white");

  //	_wrap->setWindowFlags(Qt::FramelessWindowHint);
  // base::Widgets::SetPalette(_frame.get(), QPalette::Background,
  // Qt::white);

  // TODO
  //  _layout = std::make_unique<FlowLayout>(this, -1, -1, -1);
  //  _layout->setSizeConstraint(QVBoxLayout::SetMinAndMaxSize);

  // for (int i = EMOJI_BEGIN; i <= EMOJI_END; i++) {
  //   QLabel *e = new QLabel(_wrap.get());
  //   e->setMinimumSize(QSize(24, 24));
  //   e->setFixedSize(QSize(24, 24));

  //   QString name = QString("%1").arg(i, 5, 10, QChar('0'));
  //   e->setObjectName(name);
  //   e->setPixmap(QPixmap(qsl(":/emoji/resources/emoji/%1%2%3")
  //                            .arg(name)
  //                            .arg(EMOJI_SIZE)
  //                            .arg(EMOJI_SUFFIX)));

  //   e->installEventFilter(this);

  //   _layout->addWidget(e);
// }

//    _wrap->setLayout(_layout.get());
//    setWidget(_wrap.get());

//    setStyleSheet("QWidget{border:1px solid #efefef; color: #d5d5d5;
//    font-size: 20px}");

// hide();
}

IMEmojiPanel::~IMEmojiPanel() {}

bool IMEmojiPanel::eventFilter(QObject *object, QEvent *e) {
  QWidget *pw = qobject_cast<QWidget *>(object);
  if (pw) {
    if (e->type() == QEvent::Resize) {
      QResizeEvent *re = static_cast<QResizeEvent *>(e);
      setGeometry(0, 0, re->size().width(), re->size().height());
      // _wrap->setFixedSize(re->size());
      // DEBUG_LOG_S(L_INFO) << re->size();
    }
    return true;
  }

  QLabel *o = qobject_cast<QLabel *>(object);
  if (o) {
    if (e->type() == QEvent::MouseButtonPress) {
      QString name = o->objectName();
      DEBUG_LOG(("onEmojiClicked:%1").arg(name));
      emit emojiChange(name);
      hide();
    }
  }

  return QWidget::eventFilter(object, e);
}

void IMEmojiPanel::showEvent(QShowEvent *e) {
  Q_UNUSED(e);
  _showTimes++;
  if (_showTimes == 1) {
    init();
  }
}

void IMEmojiPanel::init() {}

} // namespace widget
} // namespace UI
