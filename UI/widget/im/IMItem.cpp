﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#include "IMItem.h"

#include <QByteArray>
#include <QHBoxLayout>
#include <QLabel>
#include <QShowEvent>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>
#include <qsizepolicy.h>

#include <base/basic_types.h>
#include <base/logs.h>

#include <network/ImageLoader.h>

#include "UI/widget/im/IMEmojiPanel.h"
#include "window/style/MaskLabel.h"


namespace UI {

namespace widget {

IMItem::IMItem(ItemLayout layout, QString &from, QDateTime &time, QString &text,
               QWidget *parent)
    : QWidget(parent), _from(from) {
  //   _hLayout = std::make_shared<QHBoxLayout>(this);
  //   setLayout(_hLayout.get());

  _vLayout = std::make_shared<QVBoxLayout>(this);
  setLayout(_vLayout.get());
  _vLayout->setSizeConstraint(QVBoxLayout::SetMinAndMaxSize);
  _vLayout->setAlignment(Qt::AlignTop);

  _avatar = new MaskLabel(this);
  _avatar->setFixedWidth(45);
  _avatar->setFixedHeight(45);
  _avatar->setPixmap(QPixmap("://resources/avatar.png"));
  _avatar->setAlignment(Qt::AlignTop);

  _time = new QLabel(this);
  _time->setObjectName("chat_time");
  _time->setText(time.toString("hh:mm:ss"));

  _text = new QLabel();
  _text->setObjectName("chat_text");
  _text->setText(text);
  _text->setWordWrap(true);
  _text->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
  _text->setTextInteractionFlags(Qt::TextSelectableByMouse |
                                 Qt::LinksAccessibleByMouse);

  QRegExp regexp("(\\[:(\\d+)\\]){1}");
  while (0 <= text.indexOf(regexp)) {
    QString emoji = regexp.cap(2);
    text = text.replace(QRegExp(qsl("\\[:%1\\]").arg(emoji)),
                        qsl("<img src=':/emoji/resources/emoji/%1%2%3'/>")
                            .arg(emoji)
                            .arg(EMOJI_SIZE)
                            .arg(EMOJI_SUFFIX));
    _text->setText(text);
  }

  _vLayout->addWidget(_time);
  _vLayout->addWidget(_text);

  QString txtBgColor;
  QString txtBdColor;

  //   switch (layout) {
  //   case ItemLayout::AVATAR_LEFT:
  //     txtBgColor = ("#f3f3f3");
  //     txtBdColor = ("#ededed");

  //     _time->setAlignment(Qt::AlignLeft);

  //     _hLayout->addWidget(_avatar);
  //     _hLayout->addLayout(_vLayout.get());
  //     break;
  //   case ItemLayout::AVATAR_RIGHT:
  //     txtBgColor = ("#ffe1e1");
  //     txtBdColor = ("#fcc6c6");

  //     _time->setAlignment(Qt::AlignRight);

  //     _hLayout->addLayout(_vLayout.get());
  //     _hLayout->addWidget(_avatar);
  //     break;
  //   }

  setStyleSheet(qsl("QLabel#chat_time{ color:#cccccc }	\
						QLabel#chat_text{background-color: %1; border:2px solid %2; border-radius:5px; color: #1d1e2c;  padding: 5; }	\
					")
                    .arg(txtBgColor)
                    .arg(txtBdColor));

  //   utils::ImageLoader imageLoader(this);
  //   imageLoader.loadByUin(
  //       _from, [=](QByteArray byteArray) { this->updateAvatar(byteArray); });
}

IMItem::~IMItem() {
  delete _avatar;
  delete _time;
  delete _text;
}

void IMItem::showEvent(QShowEvent *e) {}

void IMItem::updateAvatar(QByteArray &byteArray) {
  QPixmap _pixmap;
  _pixmap.loadFromData(byteArray, nullptr, Qt::AutoColor);
  _avatar->setPixmap(_pixmap);
}
} // namespace widget
} // namespace UI
