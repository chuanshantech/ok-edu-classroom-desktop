﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#include "IMInputWidget.h"

#include <QLabel>
#include <QTextEdit>
#include <QVBoxLayout>

#include <base/logs.h>
#include <base/widgets.h>

#include "lib/network/NetworkManager.h"

#include "qdatetime.h"
#include "r.h"

namespace UI {

namespace widget {

IMInputWidget::IMInputWidget(QWidget *parent) : QWidget(parent) {
  _vLayout = std::make_unique<QVBoxLayout>();
  setLayout(_vLayout.get());
  base::Widgets::SetNoMargins(_vLayout.get());

  //	_header = new QLabel(this);
  //	_header->setObjectName("chat_input_header");
  //	_header->setFixedHeight(18);
  //	_header->setStyleSheet("background:qlineargradient(spread:pad,
  // x1:0,y1:0, x2:0,y2:5, stop:0 rgba(255,255,255,0), stop:1
  // rgba(100,100,100,255))"); _header->setStyleSheet("background:red");
  //	_header->setAlignment(Qt::AlignCenter);

  //	_vLayout->addWidget(_header);

  _input = std::make_unique<QTextEdit>(this);
  _input->setText("");
  _input->setFrameStyle(QFrame::NoFrame);
  _input->setAttribute(Qt::WA_InputMethodEnabled, true);
  _vLayout->addWidget(_input.get());
}

IMInputWidget::~IMInputWidget() {}

void IMInputWidget::showEvent(QShowEvent *e) {}

void IMInputWidget::keyPressEvent(QKeyEvent *e) {
  if (e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter) {
    onSend(true);
  }
}

void IMInputWidget::setMessageDispatcher(
    lib::IM::IMessageDispatcher *dispacher) {
  messageDispatcher = dispacher;
}

void IMInputWidget::onSend(bool checked) {

  DEBUG_LOG_S(L_INFO) << checked;

  QString text = _input->toPlainText();
  if (text.size() == 0) {
    return;
  }

  network::NetworkManager *networkManager = network::NetworkManager::Get();
  if (!networkManager)
    return;
  const auto messenger = lib::IM::Messenger::getInstance();
  if (!messenger) {
    return;
  }

  // DispatchedMessageId msgId(1);
  // Message msgContent = {false, text, QDateTime::currentDateTime()};
  // emit messageSent(msgId, msgContent);

  //    Group(QString groupId, const GroupId persistentGroupId, const QString& name,
  //    bool isAvGroupchat,
  // const QString& selfName, ICoreGroupQuery& groupQuery, ICoreIdHandler&
  // idHandler);

  // QString g("test");
  // bool sent = messenger->sendToGroup(g, text);
  // if (sent)
  //   _input->clear();

  messageDispatcher->sendMessage(false, text);
  _input->clear();
}

void IMInputWidget::onEmoji(QString emoji) {
  QTextCursor c = _input->textCursor();
  c.insertText(qsl("[:%1]").arg(emoji));
}

// ChatMessage::Ptr IMInputWidget::createMessage(const QString &displayName,
// bool isSelf,
//                              bool colorizeNames,
//                              const ChatLogMessage &chatLogMessage) {
//   auto messageType = chatLogMessage.message.isAction
//                          ? ChatMessage::MessageType::ACTION
//                          : ChatMessage::MessageType::NORMAL;

//   const bool bSelfMentioned =
//       std::any_of(chatLogMessage.message.metadata.begin(),
//                   chatLogMessage.message.metadata.end(),
//                   [](const MessageMetadata &metadata) {
//                     return metadata.type == MessageMetadataType::selfMention;
//                   });

//   if (bSelfMentioned) {
//     messageType = ChatMessage::MessageType::ALERT;
//   }

//   const auto timestamp = chatLogMessage.message.timestamp;
//   return ChatMessage::createChatMessage(
//       displayName, chatLogMessage.message.content, messageType, isSelf,
//       chatLogMessage.state, timestamp, colorizeNames);
// }

} // namespace widget
} // namespace UI
