﻿
#include "IMView.h"

#include <QDateTime>
#include <QImage>
#include <QLabel>
#include <QPalette>
#include <QPushButton>
#include <QScrollArea>
#include <QScrollBar>
#include <QShowEvent>
#include <memory>

#include "lib/network/NetworkManager.h"
#include "qwidget.h"
#include "r.h"

#include <base/logs.h>
#include <base/widgets.h>

// #include <network/session/AuthSession.h>

// #include "lib/messenger/IM.h"
// #include <lib/messenger/IMMessage.h>

#include "IMItem.h"

namespace UI {
namespace widget {

using namespace network;

IMView::IMView(QWidget *parent) : QScrollArea(parent) {

  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  _delayCaller = std::make_unique<base::DelayedCallTimer>();

  _vLayout = std::make_unique<QVBoxLayout>(this);
  _vLayout->setSizeConstraint(QVBoxLayout::SetMinAndMaxSize);
  _vLayout->setAlignment(Qt::AlignTop);
  _vLayout->setMargin(0);

  // _wrap = std::make_unique<QWidget>(this);
  // _wrap->setLayout(_vLayout.get());
  // setWidget(_wrap.get());
  setLayout(_vLayout.get());
  setStyleSheet("{border:none}");

  network::NetworkManager *nm = network::NetworkManager::Get();
  connect(nm->GetIM(), &lib::IM::IM::receiveMsg, this,
          &IMView::receiveMsg);

  // QTimer *timer = new QTimer(this);
  // connect(timer, SIGNAL(timeout()), this, SLOT(timesUp()));
  // timer->start(1000);
}

IMView::~IMView() {}

void IMView::timesUp() { DEBUG_LOG_S(L_INFO) << "....."; }

// bool IMView::event(QEvent *e) {
//   DEBUG_LOG_S(L_INFO) << e;
//   return QScrollArea::event(e);
// }

void IMView::showEvent(QShowEvent *e) { Q_UNUSED(e); }

void IMView::slideToBottom() {
  _delayCaller->call(400, [this] {
    QScrollBar *vScrollbar = verticalScrollBar();
    if (vScrollbar) {
      vScrollbar->setSliderPosition(vScrollbar->maximum() + 100);
    }
  });
}

void IMView::receiveMsg(lib::IM::IMMessage msg) {
  DEBUG_LOG_S(L_INFO) << msg.body();

  session::AuthSession *session = session::AuthSession::Instance();

  QString from = msg.from();
  ItemLayout layout = ItemLayout::AVATAR_LEFT;
  if (from.compare(session->uin()) == 0) {
    layout = ItemLayout::AVATAR_RIGHT;
  }

  QDateTime dt = QDateTime::currentDateTime();
  QString txt = msg.body();

  IMItem *item = new IMItem(layout, from, dt, txt, this);
  addMessageItem(item);
}

void IMView::addMessageItem(IMItem *item) {
  item->setParent(this);
  item->setFixedWidth(width());
  _vLayout->addWidget(item);
  slideToBottom();
}

} // namespace widget
} // namespace UI
