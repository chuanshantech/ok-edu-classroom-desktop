/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#pragma once

#include <QLabel>
#include <QPushButton>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>
#include <memory>

#include "UI/widget/im/IMEmojiPanel.h"
#include "UI/widget/im/chatlog/chatmessage.h"
#include "lib/messenger/IMMessage.h"
#include "lib/messenger/imessagedispatcher.h"

namespace UI {
namespace widget {

class IMInputWidget : public QWidget {
  Q_OBJECT
public:
  IMInputWidget(QWidget *parent);
  ~IMInputWidget();

  void setEmojiPanel(IMEmojiPanel *emojiPanel) { _emojiPanel = emojiPanel; };

  inline QTextEdit *input() { return _input.get(); };

  void setMessageDispatcher(lib::IM::IMessageDispatcher *dispacher);

protected:
  virtual void showEvent(QShowEvent *e);

  virtual void keyPressEvent(QKeyEvent *e);

private:
  QLabel *_header = nullptr;
  lib::IM::IMessageDispatcher *messageDispatcher = nullptr;

  IMEmojiPanel *_emojiPanel = nullptr;

  std::unique_ptr<QVBoxLayout> _vLayout;

  std::unique_ptr<QTextEdit> _input;

signals:
  void send(QString msg);
  void messageSent(DispatchedMessageId id, const lib::IM::Message &message);

public slots:
  void onSend(bool);
  void onEmoji(QString emoji);
};

} // namespace widget
} // namespace UI
