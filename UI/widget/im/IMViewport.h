﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#pragma once

// #include "lib/messenger/IMMessage.h"
// #include "lib/messenger/groupmessagedispatcher.h"

#include <QWidget>

namespace Ui {
class IMViewport;
}

namespace UI {
namespace widget {
namespace im {

class IMViewport : public QWidget {
  Q_OBJECT

public:
  IMViewport(QWidget *parent = nullptr);
  ~IMViewport();

protected:
  void showEvent(QShowEvent *e);

private:
  Ui::IMViewport *ui;
  // lib::IM::MessageProcessor::SharedParams sharedMessageProcessorParams;
  // std::shared_ptr<lib::IM::GroupMessageDispatcher> groupMessageDispatcher;
  int _showTimes = 0;

private slots:
  void on_chat_send_button_clicked(bool checked);
  void on_chat_emoji_button_clicked(bool checked);

};

} // namespace im
} // namespace widget
} // namespace UI
