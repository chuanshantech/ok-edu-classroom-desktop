﻿#include "AuthSession.h"

#include <list>
#include <memory>
#include <string>
#include <thread>

#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QThread>
#include <QTimer>
#include <QVariant>

#include "base/logs.h"
#include "network/NetworkHttp.h"
#include "network/backend/UserService.h"

namespace session {

using namespace network;
using namespace backend;

/*AuthSession*/
static AuthSession *self = nullptr;

AuthSession::AuthSession(QObject *parent)
    : QObject(parent),
      m_networkManager(std::make_unique<network::NetworkHttp>(this)) {
  DEBUG_LOG(("begin"));
  DEBUG_LOG(("end"));
}

AuthSession::~AuthSession() { DEBUG_LOG(("...")); }

AuthSession *AuthSession::Instance() {
  if (!self)
    self = new AuthSession(nullptr);
  return self;
}

bool AuthSession::Exists() { return self != nullptr; }

bool AuthSession::authenticated() const { return _status == SUCCESS; }

void AuthSession::initTimer() {
  //		_timer = std::make_unique<QTimer>(this);
  //		_timer->start(10 * 1000);
  //		QObject::connect(_timer.get(), SIGNAL(timeout()), this,
  // SLOT(timerUp()));
}

void AuthSession::timerUp() {
  DEBUG_LOG(("AuthSession::timerUp"));
  //  if (_status == CONNECT_STATUS::DISCONNECTED) {
  //    DEBUG_LOG(("CONNECT_STATUS::DISCONNECTED doConnect..."));
  //    doConnect();
  //  }
}

void AuthSession::doConnect() {
  DEBUG_LOG(("doConnect..."));

  QUrl url(QString(LOGIN_URL));

  //{"loginId":"gaojie", "password":"xxxxxx"}
  QJsonObject data;
  data.insert(("loginId"), m_signInInfo.account);
  data.insert(("password"), m_signInInfo.password);

  _status = CONNECTING;

  m_networkManager->postJSON(url, data, [&](const QJsonObject &res) {
    DEBUG_LOG_S(L_INFO) << "postJSON=>" << (res);

    if (!res.value(("success")).toBool()) {
      DEBUG_LOG(("Login is failure:%1=>%2")
                    .arg(m_signInInfo.account)
                    .arg(res.value("data").toString()));
      _status = FAILURE;
      LoginResult result{_status, res.value("msg").toString(), ""};
      emit loginResult(m_signInInfo, result); // LoginResult
      return;
    }

    /**登录成功
     * @brief 获取token
     *
     */
    token_ = res.value("data").toString();
    DEBUG_LOG(("Login success token:%1").arg(token_));

    _status = SUCCESS;
    LoginResult result{_status, res.value("msg").toString(), token_};
    emit loginResult(m_signInInfo, result);
  });
}

void AuthSession::doLogin(const SignInInfo &signInInfo) {
  DEBUG_LOG(("account:%1 password:%2")
                .arg(signInInfo.account)
                .arg(signInInfo.password));

  m_signInInfo = signInInfo;

  if (_status == CONNECTING) {
    DEBUG_LOG(("The connection is connecting."));
    return;
  }

  if (_status == SUCCESS) {
    DEBUG_LOG(("The connection is connected."));
    return;
  }

  if (_mutex.try_lock()) {
    _status = CONNECTING;

    LoginResult result{_status, "登录中...", ""};
    emit loginResult(m_signInInfo, result);

    // 初始化定时器
    initTimer();

    // 建立连接
    doConnect();

    _mutex.unlock();
  }
}

LOGIN_STATUS AuthSession::status() { return _status; }

} // namespace session
