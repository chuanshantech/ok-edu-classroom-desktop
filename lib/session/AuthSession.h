#pragma once

#include <QObject>
#include <QThread>
#include <QTimer>
#include <memory>

#include <base/OJSON.h>
#include <base/basic_types.h>

#include "network/NetworkHttp.h"
#include "network/network.h"
#include <QUrl>

namespace session {

enum LOGIN_STATUS {
  NONE = 0,
  CONNECTING,
  SUCCESS,
  FAILURE,
};

struct LoginResult {
    LOGIN_STATUS status;
    QString msg;
    QString token;
};

class AuthInfo : OJSON {
public:
  AuthInfo() = default;

  AuthInfo(AuthInfo &info) {
    token_ = info.getToken();
    clientName_ = info.getClientName();
  }

  ~AuthInfo() = default;

  const QString &getToken() const { return token_; }

  const QString &getClientName() const { return clientName_; }

  void fromJSON(const QJsonObject &data) {
    token_ = data.value("token").toString();
    clientName_ = data.value("clientName").toString();
  }

  QJsonObject toJSON() {
    QJsonObject qo;
    qo.insert("token", token_);
    qo.insert("clientName", clientName_);
    return qo;
  }

private:
  QString token_;
  QString clientName_;
};

/**
 * 登录信息
 */
struct SignInInfo {
  //帐号
  QString account;
  //密码
  QString password;

};

class AuthSession : public QObject {
  Q_OBJECT
public:
  AuthSession(QObject *parent);
  ~AuthSession();

  static AuthSession *Instance();

  static bool Exists();

  bool authenticated() const;

  void doLogin(const SignInInfo &signInInfo);

  void doConnect();

  SignInInfo m_signInInfo;

  [[nodiscard]] const QString &getUsername() const { return m_signInInfo.account; };

  [[nodiscard]] const QString &getPassword() const { return m_signInInfo.password; };

  [[nodiscard]] const QString &getToken() const { return token_; };

  const AuthInfo &authInfo() const { return _authInfo; }

  //  virtual void interrupt();

  LOGIN_STATUS status();

protected:

private:

  void initTimer();

  std::unique_ptr<QTimer> _timer;

  std::unique_ptr<network::NetworkHttp> m_networkManager;

  LOGIN_STATUS _status = LOGIN_STATUS::NONE;

  AuthInfo _authInfo;

  QString token_;

  std::mutex _mutex;

signals:
  void loginResult(SignInInfo&, LoginResult&); //LoginResult

public slots:
  void timerUp();

};
} // namespace session
