﻿#pragma once

#include <QList>
#include <QMap>
#include <QObject>
#include <mutex>
#include <vector>

//#include "gloox/jid.h"

#include "lib/network/backend/domain/UserInfo.h"
//#include "lib/session/AuthSession.h"
#include "UserObserver.h"

namespace lib {
namespace IM {

//using namespace session;
using namespace backend;

typedef QMap<QString, UserInfo> UserMap;
typedef QList<network::UserObserver *> UserObserverList;

class UserManager : public QObject {
  Q_OBJECT

public:
  UserManager();
  ~UserManager();

  inline virtual UserInfo *myInfo() const { return _myInfo.get(); }

  inline virtual UserType myType() const {
    return static_cast<UserType>(_myInfo.get() ? _myInfo->getUserType() : 0);
  }

  inline bool isTeacher() const { return myType() == UserType::Teacher; }

  inline bool isTeacher(UserInfo &info) const {
    return static_cast<UserType>(info.getUserType()) == UserType::Teacher;
  }

  inline bool isMy(const UserInfo &info) const {

    return myInfo() &&
           myInfo()->getUin().compare(info.getUin(), Qt::CaseInsensitive) == 0;
    //            return false;
  }

  inline bool isMember() { return myType() == UserType::Member; }

  virtual int userSize();

  bool addUserUin(const UserJID &jid);

  bool removeUserUin(const UserJID &jid);

  void setPeerStatus(UserJID &jid, PeerStatus &status);

  void registerObserver(network::UserObserver *userObserver);

  //        inline void setUserInfo(std::shared_ptr<UserInfo> info){
  //            _myInfo = info;
  //        }

//  void addUserCapabilities(QString &uin, const Capabilities *caps);

private:
//  AuthSession *client = nullptr;

  std::unique_ptr<UserInfo> _myInfo;

  std::shared_ptr<UserMap> _userMap;
  std::shared_ptr<UserObserverList> _userObservers;

  std::mutex _mutex;

signals:

  void joined(const backend::UserJID &jid);
  void left(const backend::UserJID &jid);

  void statusEvent(const backend::UserJID &jid,
                   const backend::PeerStatus &status);


public slots:
//  void onConnectResult(network::CONNECT_STATUS);
};

} // namespace messenger
} // namespace network
