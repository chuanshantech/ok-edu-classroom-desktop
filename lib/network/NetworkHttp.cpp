﻿#include "NetworkHttp.h"

#include <memory>

#include <QByteArray>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

#include <QDesktopServices>
#include <QHttpMultiPart>
#include <QNetworkAccessManager>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QSslSocket>
#include <QUrlQuery>

#include <base/files.h>
#include <base/logs.h>


namespace network {

NetworkHttp::NetworkHttp(QObject *parent) : QObject(parent) {
  DEBUG_LOG(("begin"));

  bool supportsSsl = QSslSocket::supportsSsl();
  DEBUG_LOG(("supportsSsl:%1").arg(supportsSsl));
  QString buildVersion = QSslSocket::sslLibraryBuildVersionString();
  DEBUG_LOG(("buildVersion:%1").arg(buildVersion));
  QString libraryVersion = QSslSocket::sslLibraryVersionString();
  DEBUG_LOG(("libraryVersion:%1").arg(libraryVersion));

  //  QNetworkAccessManager
  _manager = new QNetworkAccessManager(this);
  auto schemes = _manager->supportedSchemes();
  DEBUG_LOG(("supportedSchemes:%1").arg(schemes.join(" ")));
}

NetworkHttp::~NetworkHttp() {
  delete _manager;
  DEBUG_LOG(("NetworkHttp been destroyed!"));
}


FileResult NetworkHttp::parseDownloadResult(QJsonObject &result) {
  FileResult _result;
  bool success = result.value("success").toBool(false);

  _result.success = success;
  if (success) {
    _result.url = result.value("data").toString();

    QJsonObject extra = result.value("extra").toObject();
    _result.key = extra.value("baseKey").toString();
    _result.name = extra.value("name").toString();
    _result.extension = extra.value("extension").toString();
    _result.contentType = extra.value("contentType").toString();
  }

  return _result;
}

QJsonObject NetworkHttp::byteArrayToJSON(const QByteArray &buf) {
  QString content = QString::fromUtf8(buf);
  //  DEBUG_LOG(("buf:%1").arg(content));
  QJsonObject jsonObject;
  QJsonDocument jsonDocument = QJsonDocument::fromJson(content.toUtf8().data());
  if (!jsonDocument.isNull()) {
    jsonObject = jsonDocument.object();
  }
  return jsonObject;
}

void NetworkHttp::load(const QUrl &url, Fn<void(QByteArray &)> fn) {

  DEBUG_LOG(("Request URL: %1").arg(url.toString()));

  QNetworkRequest request(url);
  wrapRequest(request, url.host());

  DEBUG_LOG_S(L_INFO) << ("using _manager:") << _manager;
  auto *_reply = _manager->get(request);
  connect(_reply, &QNetworkReply::finished, this, &NetworkHttp::httpFinished);

  QEventLoop loop;
  connect(_reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
  loop.exec();

  QByteArray byteArr = _reply->readAll();

  int size = byteArr.size();
  DEBUG_LOG(("Received QByteArray:%1").arg(size));

  auto cs = _manager->cookieJar()->cookiesForUrl(url);
  if (cs.size() > 0) {
    for (auto c : cs) {
      DEBUG_LOG(("cookie:%1=%2@%3%4")
                    .arg(QString::fromLatin1(c.name()))
                    .arg(QString::fromLatin1(c.value()))
                    .arg(c.domain())
                    .arg(c.path()));
    }
  }

  fn(byteArr);
}

QByteArray NetworkHttp::get(const QUrl &url) {

  DEBUG_LOG(("Request URL: %1").arg(url.toString()));

  QNetworkRequest request(url);
  wrapRequest(request, url.host());

  DEBUG_LOG_S(L_INFO) << ("using _manager:") << _manager;
  std::unique_ptr<QNetworkReply> _reply(_manager->get(request));
  connect(_reply.get(), &QNetworkReply::finished,
          this, &NetworkHttp::httpFinished);

  QEventLoop loop;
  connect(_reply.get(), &QNetworkReply::finished,
          &loop, &QEventLoop::quit);
  loop.exec();

  QByteArray byteArr = _reply->readAll();

  int size = byteArr.size();
  DEBUG_LOG(("Received QByteArray:%1").arg(size));

  return byteArr;
}

void NetworkHttp::loadJSON(const QUrl &url, Fn<void(const QJsonObject &)> fn) {
  load(url, [&](const QByteArray &buf) {
    QJsonObject j = byteArrayToJSON(buf);
    fn(j);
  });
}

void NetworkHttp::postJSON(const QUrl &url, const QJsonObject &data,
                           Fn<void(const QJsonObject &)> fn) {
  post(url, QString(QJsonDocument(data).toJson()), [&](const QByteArray buf) {
    QJsonObject j = byteArrayToJSON(buf);
    fn(j);
  });
}

void NetworkHttp::postJSON(const QUrl &url, const QString &data,
                           Fn<void(const QJsonObject &)> fn) {
  post(url, data, [&](QByteArray buf) {
    QJsonObject j = byteArrayToJSON(buf);
    fn(j);
  });
}

void NetworkHttp::post(const QUrl &url, const QString &data,
                       Fn<void(QByteArray)> fn) {

  QNetworkRequest request(url);
  request.setHeader(QNetworkRequest::ContentTypeHeader,
                    QVariant("application/json"));
  wrapRequest(request, url.host());

  auto postData = QByteArray::fromStdString(data.toStdString());
  auto _reply =_manager->post(request, postData);
  _reply->ignoreSslErrors();

  connect(_reply, &QNetworkReply::finished, this, &NetworkHttp::httpFinished);

  QEventLoop loop;
  connect(_reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
  loop.exec();

  QByteArray byteArr = _reply->readAll();

  int size = byteArr.size();
  DEBUG_LOG(("Received QByteArray:%1").arg(size));
  fn(byteArr);

}

QByteArray NetworkHttp::post(const QUrl &url, const QString &data) {
  DEBUG_LOG(("Request URL:%1").arg(url.toString()));
  DEBUG_LOG(("Request Data:%1").arg(data));
  if (data.isEmpty()) {
    DEBUG_LOG_S(L_WARN) << ("data isEmpty.");
    return QByteArray::fromStdString("");
  }

  QNetworkRequest request(url);
  request.setHeader(QNetworkRequest::ContentTypeHeader,
                    QVariant("application/json"));
  request.setRawHeader("Accept", "application/json");
  wrapRequest(request, url.host());

  auto postData = QByteArray::fromStdString(data.toStdString());
  DEBUG_LOG_S(L_INFO) << "using _manager:" << _manager;
  std::unique_ptr<QNetworkReply> _reply(_manager->post(request, postData));
  _reply->ignoreSslErrors();

  connect(_reply.get(), &QNetworkReply::finished, this, &NetworkHttp::httpFinished);

  QEventLoop loop;
  connect(_reply.get(), &QNetworkReply::finished, &loop, &QEventLoop::quit);
  loop.exec();

  QByteArray byteArr = _reply->readAll();

  int size = byteArr.size();
  DEBUG_LOG(("Received QByteArray:%1").arg(size));
  return (byteArr);
}

void NetworkHttp::PostFormData(
    const QUrl &url, const QByteArray &byteArray, const QString &contentType,
    const QString &filename,
    Fn<void(int bytesSent, int bytesTotal)> uploadProgress,
    Fn<void(const QJsonObject &)> readyRead) {

  if (url.isEmpty()) {
    DEBUG_LOG_S(L_ERROR) << "url_ is nullptr!";
    return;
  }

  if (byteArray.size() <= 0) {
    DEBUG_LOG_S(L_ERROR) << "byteArray is empty!";
    return;
  }

  DEBUG_LOG_S(L_INFO) << "url:" << url << "byteArray:" << byteArray.size();

  // 添加认证信息

  QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

  /* type */
  QUrlQuery uQuery(url.query());
  // uQuery.addQueryItem("type", (UploadFileFolders[(int)folder]));
  const_cast<QUrl &>(url).setQuery(uQuery);

  DEBUG_LOG_S(L_INFO) << "url:" << url.toString();

  // file
  QHttpPart imagePart;
  imagePart.setHeader(QNetworkRequest::ContentTypeHeader,
                      QVariant(contentType));
  imagePart.setHeader(
      QNetworkRequest::ContentDispositionHeader,
      QVariant("form-data; name=\"file\"; filename=\"" + filename + "\""));

  imagePart.setBody(byteArray);
  multiPart->append(imagePart);

  QNetworkRequest request(url);

  QList<QNetworkCookie> cookies = _manager->cookieJar()->cookiesForUrl(url);
  // cookies.append(QNetworkCookie(QString("ticket").toUtf8(),
  // client->token().toUtf8()));
  QVariant var;
  var.setValue(cookies);

  for (auto c : cookies) {
    DEBUG_LOG_S(L_INFO) << "cookie name: " << c.name()
                        << " value: " << c.value();
  }

  request.setHeader(QNetworkRequest::CookieHeader, var);
  QNetworkReply *reply = _manager->post(request, multiPart);
  multiPart->setParent(reply);
  connect(reply, &QNetworkReply::uploadProgress, uploadProgress);
}

/**
  Open the url
 * @brief NetworkHttp::openUrl
 * @param url
 */
void NetworkHttp::openUrl(const QUrl &url) { QDesktopServices::openUrl(url); }

/**上传表单数据
 * @brief NetworkHttp::PostFile
 * @param url
 * @param file
 * @param fn
 */
void NetworkHttp::PostFormData(
    const QUrl &url, QFile *file,
    Fn<void(int bytesSent, int bytesTotal)> uploadProgress,
    Fn<void(const QJsonObject &)> readyRead) {
  if (url.isEmpty()) {
    DEBUG_LOG_S(L_ERROR) << "url_ is nullptr!";
    return;
  }

  if (!file) {
    DEBUG_LOG_S(L_ERROR) << "file_ is nullptr!";
    return;
  }

  DEBUG_LOG_S(L_INFO) << "url:" << url << "file:" << file->fileName();

  QString contentType = base::Files::GetContentTypeStr(file->fileName());

  file->open(QIODevice::ReadOnly);

  QByteArray byteArray = file->readAll();

  PostFormData(url, byteArray, base::Files::GetContentTypeStr(file->fileName()),
               file->fileName(), uploadProgress, readyRead);
}

void NetworkHttp::wrapRequest(const QNetworkRequest &request, const QUrl &url) {

  auto cs = _manager->cookieJar()->cookiesForUrl(url);
  if (!cs.empty()) {
    for (auto &c : cs) {
      DEBUG_LOG_S(L_INFO) << "cookie: " << c.domain() << "/" << c.path() << "|"
                          << c.name() << "=" << c.value();
    }
  }
}

void NetworkHttp::httpFinished() { DEBUG_LOG_S(L_INFO) << "..."; }
} // namespace network
