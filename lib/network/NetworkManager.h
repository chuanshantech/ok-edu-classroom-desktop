﻿#pragma once

#include <QByteArray>
#include <QFile>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QObject>
#include <QUrl>
#include <memory>

#include "network/NetworkHttp.h"
#include "network/network.h"

#include <base/basic_types.h>
#include <base/timer.h>

namespace network {

static QString UploadFileFolders[] = {
    "SmartBoard",
    "misc",
};

enum class UploadFileFolder { SmartBoard = 0 };

class NetworkManager : public QObject {
  Q_OBJECT

public:
  ~NetworkManager();

  static NetworkManager *Get();

  QJsonObject byteArrayToJSON(const QByteArray &buf);

  void load(const QUrl &url, Fn<void(QByteArray &)> fn);

  void loadJSON(const QUrl &url, Fn<void(QJsonObject &)> fn);

//  session::AuthSession *session() const;

//  messenger::UserManager *userManager() const;

  // virtual messenger::IM *GetIM();

//  virtual webrtc::OkRtcManager *InitGetOkRtcManager();

  virtual NetworkHttp *http();

  virtual void
  PostFormData(const QUrl &url, UploadFileFolder type, QFile *file,
               Fn<void(int bytesSent, int bytesTotal)> uploadProgress,
               Fn<void(QJsonObject &json)> readyRead);

  void appendAuthInfo(const QUrl &url);

  virtual void GetURL(QFile *file, Fn<void(QJsonObject &result)> fn);

  virtual void GetURL(const ByteInfo &byteInfo,
                      Fn<void(QJsonObject &result)> fn);

  virtual void PostFormData(const QUrl &url, UploadFileFolder folder,
                            QByteArray byteArray, const QString &contentType,
                            const QString &filename,
                            Fn<void(int, int)> uploadProgress,
                            Fn<void(QJsonObject &)> readyRead);

  virtual void openUrl(const QUrl &url);

  // protected:
  //   virtual void onLogin() override;
  //   virtual void onLogout() override;

private:
  NetworkManager(QObject *parent = nullptr);

  std::unique_ptr<base::DelayedCallTimer> _deylayCaller;

  std::unique_ptr<QNetworkAccessManager> _manager;

//  std::unique_ptr<session::AuthSession> client;

//  std::unique_ptr<messenger::UserManager> _userManager;

  // std::unique_ptr<messenger::IM> _im;

  //        std::unique_ptr<IMJingle> _jingle;
  //        std::unique_ptr<smartboard::IMSmartBoard> _smartBoard;

//  std::unique_ptr<webrtc::OkRtcManager> _rtcManager;

  std::unique_ptr<NetworkHttp> _networkHttp;

  std::mutex _get_mutex;
};

} // namespace network
