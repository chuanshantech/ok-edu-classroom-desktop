﻿
#include "ImageLoader.h"

#include <QObject>
#include <QPixmap>
#include <QString>
#include <QUrl>
#include <QtGui/QtGui>
#include <memory>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

#include <base/basic_types.h>

#include <network/backend/UserService.h>
#include <network/backend/domain/UserInfo.h>

namespace utils {

using namespace backend;

ImageLoader::ImageLoader(QObject *parent) : QObject(parent) {}

ImageLoader::~ImageLoader() {}

void ImageLoader::load(const QString &url, Fn<void(const QByteArray &)> fn) {

  QNetworkAccessManager manager;
  QEventLoop loop;

  QNetworkReply *reply = manager.get(QNetworkRequest(url));
  QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
  loop.exec();

  fn(reply->readAll());
}

void ImageLoader::loadByUin(const QString &uin,
                            Fn<void(const QByteArray &)> fn) {
  UserService userService;
  userService.getByUin(uin, [this, fn](const UserInfo &info) {
    if (0 < info.getId()) {
      QString avatarUrl = info.getAvatar();
      if (!avatarUrl.isEmpty()) {
        load(avatarUrl, fn);
      }
    }
  });
}

} // namespace utils
