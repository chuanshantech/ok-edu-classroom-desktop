/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/

#ifndef NETWORK_H
#define NETWORK_H

#include <QString>

namespace network {

enum PUBSUB {
    SMARTBOARD
};

///**
// * @brief 连接状态
// *
// */
//enum CONNECT_STATUS
//{
//    NONE = 0,
//    CONNECTING,
//    CONNECTED,
//    DISCONNECTED,
//    TIMEOUT,
//    SUCCESS,
//    FAILURE,
//};


typedef struct
{
    bool success;        //: true/false
    QString url;         // data: "http://ftp.chuanshaninfo.com:8000/oos/avatar/OWI0MjBmOTEtMmFhYS00YWM5LWE5N2MtOGZmNTE3ZmQwMGIw.jpg"
    QString key;         // baseKey: "OWI0MjBmOTEtMmFhYS00YWM5LWE5N2MtOGZmNTE3ZmQwMGIw"
    QString contentType; // contentType: "image/jpeg"
    QString extension;   // extension: "jpg"
    QString name;        // name: "caef76094b36acafcdb225aa71d98d1001e99c12.jpg"
} FileResult;

}


#endif // NETWORK_H