﻿#include "NetworkManager.h"

#include <memory>

#include <QByteArray>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

#include <QDesktopServices>
#include <QHttpMultiPart>
#include <QNetworkAccessManager>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>

#include <base/files.h>
#include <base/logs.h>

#include <task/otaskupload.h>
#include <task/taskmanager.h>
#include <task/taskupload.h>

#include "r.h"

namespace network {

static NetworkManager *self = nullptr;

NetworkManager::NetworkManager(QObject *parent)
    : QObject(parent),   //
      _manager(nullptr), //
      _networkHttp(nullptr) {
  DEBUG_LOG(("begin"));

  // QNetworkAccessManager
  _manager = std::make_unique<QNetworkAccessManager>(this);
  // // AuthSession
  // client = std::make_unique<session::AuthSession>(this);

  // TODO UserManager
  //  _userManager = std::make_unique<messenger::UserManager>(client.get());
  // _userManager->registerObserver(this);

  // _im = std::make_unique<messenger::IM>(client.get(), _userManager.get());

  //  _rtcManager = std::make_unique<webrtc::OkRtcManager>(this);

  DEBUG_LOG(("end"));
}

NetworkManager::~NetworkManager() {
  DEBUG_LOG(("NetworkManager should never be destroy!"));
}

NetworkManager *NetworkManager::Get() {
  if (!self) {
    self = new NetworkManager;
  }
  return self;
}

QJsonObject NetworkManager::byteArrayToJSON(const QByteArray &buf) {
  QString content = QString::fromUtf8(buf);
  DEBUG_LOG(("buf:%1").arg(content));

  QJsonObject jsonObject;
  QJsonDocument jsonDocument = QJsonDocument::fromJson(content.toUtf8().data());
  if (!jsonDocument.isNull()) {
    jsonObject = jsonDocument.object();
  }
  return jsonObject;
}

void NetworkManager::load(const QUrl &url, Fn<void(QByteArray &)> fn) {

  appendAuthInfo(url);

  DEBUG_LOG(("Request URL:%1").arg(url.toString()));

  QNetworkRequest request(url);
  QNetworkReply *reply = _manager->get(request);
  QEventLoop loop;
  QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
  loop.exec();

  QByteArray byteArr = reply->readAll();

  int size = byteArr.size();
  DEBUG_LOG(("Received QByteArray:%1").arg(size));

  fn(byteArr);
}

void NetworkManager::loadJSON(const QUrl &url, Fn<void(QJsonObject &)> fn) {
  load(url, [&](QByteArray &buf) {
    QJsonObject j = byteArrayToJSON(buf);
    fn(j);
  });
}

/**添加认证信息
 * @brief NetworkManager::appendAuthInfo
 * @param url
 */
void NetworkManager::appendAuthInfo(const QUrl &url) {
  QUrlQuery uQuery(url.query());
  //  uQuery.addQueryItem("uin", client->uin());
  //  uQuery.addQueryItem("token", client->authInfo().getToken());
  //  uQuery.addQueryItem("client_name", client->authInfo().getClientName());
  (const_cast<QUrl &>(url)).setQuery(uQuery);
}

void NetworkManager::GetURL(QFile *file, Fn<void(QJsonObject &result)> fn) {
  DEBUG_LOG_S(L_INFO) << ("file:") << file;
  if (!file) {
    return;
  }

  // 上传文件
  task::TaskUpload *uploadTask = new task::TaskUpload();

  QUrl url("http://classroom.chuanshaninfo.com/classroom/v1/upload/getURL.do");
  uploadTask->setUrl(url);
  uploadTask->setFile(file);
  uploadTask->setFolder(UploadFileFolder::SmartBoard);

  task::TaskManager::Get()->addTask(uploadTask, fn);
}

void NetworkManager::GetURL(const ByteInfo &byteInfo,
                            Fn<void(QJsonObject &)> fn) {

  // 上传文件
  task::OTaskUpload *uploadTask = new task::OTaskUpload();

  QUrl url("http://classroom.chuanshaninfo.com/classroom/v1/upload/getURL.do");
  uploadTask->setUrl(url);
  uploadTask->setFile(byteInfo);
  uploadTask->setFolder(UploadFileFolder::SmartBoard);

  task::TaskManager::Get()->addTask(uploadTask, fn);
}

void NetworkManager::PostFormData(
    const QUrl &url, UploadFileFolder folder, QByteArray byteArray,
    const QString &contentType, const QString &filename,
    Fn<void(int bytesSent, int bytesTotal)> uploadProgress,
    Fn<void(QJsonObject &)> readyRead) {

  if (url.isEmpty()) {
    DEBUG_LOG_S(L_ERROR) << "url_ is nullptr!";
    return;
  }

  if (byteArray.size() <= 0) {
    DEBUG_LOG_S(L_ERROR) << "byteArray is empty!";
    return;
  }

  DEBUG_LOG_S(L_INFO) << "url:" << url << "byteArray:" << byteArray.size();

  // 添加认证信息
  appendAuthInfo(url);

  QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

  /* type */
  QUrlQuery uQuery(url.query());
  uQuery.addQueryItem("type", (UploadFileFolders[(int)folder]));
  const_cast<QUrl &>(url).setQuery(uQuery);

  DEBUG_LOG_S(L_INFO) << "url:" << url.toString();

  // file
  QHttpPart imagePart;
  imagePart.setHeader(QNetworkRequest::ContentTypeHeader,
                      QVariant(contentType));
  imagePart.setHeader(
      QNetworkRequest::ContentDispositionHeader,
      QVariant("form-data; name=\"file\"; filename=\"" + filename + "\""));

  imagePart.setBody(byteArray);
  multiPart->append(imagePart);

  QNetworkRequest request(url);

  QList<QNetworkCookie> cookies = _manager->cookieJar()->cookiesForUrl(url);
  //  cookies.append(QNetworkCookie(QString("ticket").toUtf8(),
  //  client->token().toUtf8()));
  QVariant var;
  var.setValue(cookies);

  for (auto c : cookies) {
    DEBUG_LOG_S(L_INFO) << "cookie name: " << c.name()
                        << " value: " << c.value();
  }

  request.setHeader(QNetworkRequest::CookieHeader, var);
  QNetworkReply *reply = _manager->post(request, multiPart);
  multiPart->setParent(reply);

  // delete the multiPart with the reply // here connect signals etc.
  // 更新上传进度
  //        QObject::connect(reply, &QNetworkReply::uploadProgress, [&](int
  //        bytesSent, int bytesTotal) {
  //            DEBUG_LOG_S(L_INFO) << "bytesSent: "<< bytesSent << "bytesTotal:
  //            " << bytesTotal ;
  //        });

  QObject::connect(reply, &QNetworkReply::uploadProgress, uploadProgress);

  // 文件关闭很多种，但是切记不能提前关闭，否则上传进度卡死。
  //  QObject::connect(reply, &QNetworkReply::readyRead, [=]()
  //                   {
  //                      QByteArray buf = reply->readAll();
  //                      DEBUG_LOG_S(L_INFO) << "reply:"<< buf;
  //                      QJsonObject j = byteArrayToJSON(buf);
  //                      readyRead(j);
  //                      // file->close();
  //                  });
}

/**
  Open the url
 * @brief NetworkManager::openUrl
 * @param url
 * @details http://classroom.chuanshaninfo.com/jump.do?url={url}
 */
void NetworkManager::openUrl(const QUrl &url) {

  QUrl jumpUrl(qsl(BACKEND_BASE_URL "/%1").arg("jump.do"));

  // url
  QUrlQuery uQuery(jumpUrl);
  uQuery.addQueryItem("url", url.toString());
  jumpUrl.setQuery(uQuery);

  // token
  appendAuthInfo(jumpUrl);

  QDesktopServices::openUrl(jumpUrl);
}

/**上传表单数据
 * @brief NetworkManager::PostFile
 * @param url
 * @param file
 * @param fn
 */
void NetworkManager::PostFormData(
    const QUrl &url, UploadFileFolder folder, QFile *file,
    Fn<void(int bytesSent, int bytesTotal)> uploadProgress,
    Fn<void(QJsonObject &)> readyRead) {
  if (url.isEmpty()) {
    DEBUG_LOG_S(L_ERROR) << "url_ is nullptr!";
    return;
  }

  if (!file) {
    DEBUG_LOG_S(L_ERROR) << "file_ is nullptr!";
    return;
  }

  DEBUG_LOG_S(L_INFO) << "url:" << url << "file:" << file->fileName();

  QString contentType = base::Files::GetContentTypeStr(file->fileName());

  file->open(QIODevice::ReadOnly);

  QByteArray byteArray = file->readAll();

  PostFormData(url, folder, byteArray,
               base::Files::GetContentTypeStr(file->fileName()),
               file->fileName(), uploadProgress, readyRead);
}

// messenger::UserManager *NetworkManager::userManager() const {
//   return _userManager.get();
// }
//
// session::AuthSession *NetworkManager::session() const { return client.get();
// }

NetworkHttp *NetworkManager::http() { return _networkHttp.get(); }

// messenger::IM *NetworkManager::GetIM() { return _im.get(); }

// webrtc::OkRtcManager *NetworkManager::InitGetOkRtcManager() {
//
//   return _rtcManager.get();
// }

// void NetworkManager::onLogin() {}

// void NetworkManager::onLogout() {}

} // namespace network
