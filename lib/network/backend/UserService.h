﻿#pragma once

#include <QJsonObject>
#include <QObject>
#include <QString>

#include <base/basic_types.h>

#include <network/backend/BaseService.h>
#include <network/backend/HttpService.h>
#include <network/backend/domain/AuthInfo.h>
#include <network/backend/domain/UserInfo.h>

#include "network/NetworkManager.h"
#include "session/AuthSession.h"

namespace backend {

class UserService : public BaseService {
  Q_OBJECT
public:
  UserService(QObject *parent = nullptr);
  ~UserService();

  void login(const QString &username, const QString &password,
             Fn<void(const session::AuthInfo &)> callback);

  void getInfo(Fn<void(const UserInfo &)> callback);
  void getByUin(const QString &uin, Fn<void(const UserInfo &)> callback);

  void setUserType(const QString &userType);
};

} // namespace backend
