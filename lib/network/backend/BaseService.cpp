﻿#include "BaseService.h"

#include <QObject>
#include <QString>

#include "network/NetworkHttp.h"
#include "r.h"

#include <algorithm>
#include <base/singleton.h>
#include <session/AuthSession.h>

namespace backend {

BaseService::BaseService(QObject *parent)
    : QObject(parent),
      m_networkManager(std::make_unique<network::NetworkHttp>(this)),
      _baseUrl(BACKEND_BASE_URL) {
}

BaseService::~BaseService() {}

} // namespace backend
