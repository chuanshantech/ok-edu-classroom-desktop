#include "ChatService.h"

#include <QObject>
#include <QUrl>
#include <string>

#include "../backend/domain/RoomInfo.h"
#include "lib/network/NetworkManager.h"
#include "qjsonarray.h"
#include "qjsonobject.h"
#include <network/backend/BaseService.h>
#include <network/backend/HttpService.h>

namespace backend {

ChatService::ChatService(QObject *parent) : BaseService(parent) {}

ChatService::~ChatService() {}

QString ChatService::current() {
  QUrl url(QString(_baseUrl + "/conference/current"));
  QJsonObject obj = m_networkManager->byteArrayToJSON(m_networkManager->get(url));
  return obj.value("data").toString();
}
void ChatService::getRoomInfo(Fn<void(RoomInfo &)> callback) {
  QUrl url(QString(_baseUrl + "/conference/info"));
  m_networkManager->loadJSON(url, [&, callback](QJsonObject res) {
    if (res.value("success").isNull() || !res.value("success").toBool(false)) {
      return;
    }

    QJsonObject jo = res.value("data").toObject();

    RoomInfo info;
    info.fromJSON(jo);

    callback(info);
  });
}

void ChatService::listRoomInfos(Fn<void(std::list<RoomInfo> &)> callback) {

  QUrl url(QString(_baseUrl + "/conference/list"));
  m_networkManager->loadJSON(url, [&, callback](QJsonObject res) {
    if (res.value("success").isNull() || !res.value("success").toBool(false)) {
      return;
    }

    QJsonArray list = res.value("data").toArray();
    std::list<RoomInfo> rooms;
    for (QJsonValue val : list) {
      auto obj = val.toObject();
      RoomInfo info;
      info.fromJSON(obj);
      rooms.push_back(info);
    }
    callback(rooms);
  });
}

void ChatService::saveSn(const QString &sn, Fn<void(bool)> callback) {
  QUrl url(QString(_baseUrl + "/conference/save"));
  m_networkManager->postJSON(url, (sn), [&, this](QJsonObject res) {
    DEBUG_LOG_S(L_INFO) << "received:" << (res);
    if (res.value("success").isNull() || !res.value("success").toBool(false)) {
      return;
    }
    callback(true);
  });
}
} // namespace backend
