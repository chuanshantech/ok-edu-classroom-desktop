﻿#include "ClassingService.h"

#include <memory>
#include <string>
#include <QString>
#include <QJsonObject>
#include <QJsonDocument>

#include "r.h"
#include <session/AuthSession.h>

#include <base/logs.h>
#include <base/basic_types.h>

#include <network/backend/HttpService.h>
#include <network/backend/BaseService.h>

namespace backend
{

	using namespace session;

	ClassingService::ClassingService(QObject *parent) : BaseService(parent)
	{
	}

	ClassingService::~ClassingService()
	{
	}

	bool ClassingService::requestInfo(Fn<void(QJsonObject)> callback)
	{
		QString baseUrl = _baseUrl + "/welcome/info.do";
		QMap<QString, QString> params;

		HttpService httpService;
		return httpService.request(baseUrl, params, callback);
	}

}
