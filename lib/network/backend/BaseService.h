﻿#pragma once

#include <QObject>
#include <QString>
#include <string>

#include "network/NetworkHttp.h"
#include "network/NetworkManager.h"
#include "network/backend/BaseService.h"
#include "r.h"

namespace backend {

class BaseService : public QObject {
  Q_OBJECT
public:
  BaseService(QObject *parent = nullptr);
  ~BaseService();

  inline const QString &baseUrl() const { return _baseUrl; }

  //  inline const AuthSession *session() const { return this->client; }

  inline network::NetworkHttp *networkManager() const { return m_networkManager.get(); }

protected:
  std::unique_ptr<network::NetworkHttp> m_networkManager;
  //  AuthSession *client;
  QString _baseUrl;
};

} // namespace backend
