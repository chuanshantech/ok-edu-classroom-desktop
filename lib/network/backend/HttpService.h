#pragma once

#include <map>
#include <memory>
#include <string>
#include <QMap>
#include <QString>
#include <QJsonObject>
#include <QJsonDocument>
#include <base/logs.h>
#include <base/basic_types.h>
#include <network/backend/BaseService.h>

namespace backend
{

	class HttpService : BaseService
	{
	public:
		HttpService(QObject *parent = nullptr);
		~HttpService();

		bool request(QString &baseUrl, QMap<QString, QString> &params, Fn<void(QJsonObject)> callback);
	};

}