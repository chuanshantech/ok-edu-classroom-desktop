﻿#pragma once

#include <QObject>

#include "base/OJSON.h"
#include <network/backend/BaseService.h>
#include <network/backend/HttpService.h>

namespace backend {

class PushInfo : OJSON {
public:
  int getCount() const { return count; }

  void fromJSON(const QJsonObject &data) {
    count = data.value("count").toInt();
  }

  QJsonObject toJSON() {
    QJsonObject qo;
    qo.insert("count", count);
    return qo;
  }

private:
  int count = 0;
};

class PushService : public BaseService {
  Q_OBJECT

public:
  PushService(QObject *parent = nullptr);
  ~PushService();

  bool requestCount(Fn<void(PushInfo &)> callback);
};
} // namespace backend
