﻿#include "UserService.h"

#include <QJsonObject>
#include <QUrl>

#include <base/basic_types.h>

#include <base/logs.h>

#include "lib/network/NetworkManager.h"

#include <network/backend/BaseService.h>
#include <network/backend/HttpService.h>

#include "qjsonarray.h"
#include "qjsonobject.h"
#include "r.h"

namespace backend {

using namespace network;

UserService::UserService(QObject *parent) : BaseService(parent) {}

UserService::~UserService() {}

void UserService::login(const QString &username, const QString &password,
                        Fn<void(const session::AuthInfo &)> callback) {
  QUrl url(QString(_baseUrl + "/login.do?username=%1&password=%2")
               .arg(username)
               .arg(password));

  m_networkManager->loadJSON(url, [callback](QJsonObject res) {
    session::AuthInfo info;
    if (res.value(("success")).toBool()) {
      info.fromJSON(res.value("data").toObject());
    }
    callback(info);
  });
}

void UserService::getByUin(const QString &uin,
                           Fn<void(const UserInfo &)> callback) {
  QUrl url(QString(_baseUrl + "/user/get?uin=%1").arg(uin));

  m_networkManager->loadJSON(url, [callback](QJsonObject res) {
    if (res.value(("success")).toBool()) {
      QJsonObject jo = res.value("data").toObject();

      UserInfo info;
      info.fromJSON(jo);

      callback(info);
    }
  });
}

void UserService::getInfo(Fn<void(const UserInfo &)> callback) {
  QUrl url(QString(_baseUrl + "/user/info"));
  m_networkManager->loadJSON(url, [callback](QJsonObject res) {
    if (res.value(("success")).toBool()) {
      QJsonObject jo = res.value("data").toObject();

      UserInfo info;
      info.fromJSON(jo);

      callback(info);
    }
  });
}

void UserService::setUserType(const QString &userType) {
  QUrl url(QString(_baseUrl + "/user/setUserType"));
  m_networkManager->post(url, userType, [&](QByteArray res) {
    DEBUG_LOG_S(L_INFO) << "received:" << QString(res);
  });
}

} // namespace backend
