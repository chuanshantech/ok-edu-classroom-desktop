﻿#pragma once

#include <memory>
#include <string>
#include <QJsonObject>
#include <QObject>
#include <QString>

#include <base/basic_types.h>

#include <session/AuthSession.h>
#include <network/backend/BaseService.h>

namespace backend
{

	using namespace session;

	/*上课信息*/
	class ClassingService : BaseService
	{
		Q_OBJECT

	public:
		ClassingService(QObject *parent = nullptr);
		~ClassingService();

		bool requestInfo(Fn<void(QJsonObject)> callback);

	private:
	};
}
