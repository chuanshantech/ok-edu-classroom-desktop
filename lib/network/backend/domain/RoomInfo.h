#pragma once

#include <QJsonObject>
#include <QObject>
#include <base/OJSON.h>

namespace backend {

class RoomInfo : OJSON {
public:
  QString getJid() { return jid; }
  QString getName() { return name; }
  QString getPassword() { return password; }
  QString getSn() { return sn; }

  inline void setJid(QString &_jid) { jid = _jid; }

  inline void setName(QString &_name) { name = _name; }

  inline void setPassword(QString &_password) { password = _password; }

  void fromJSON(const QJsonObject &data) {
    jid = data.value("jid").toString();
    name = data.value("name").toString();
    password = data.value("password").toString();
    sn = data.value("sn").toString();
  }

  QJsonObject toJSON() {
    QJsonObject qo;
    qo.insert("jid", jid);
    qo.insert("name", name);
    qo.insert("password", password);
    return qo;
  }

private:
  QString jid;
  QString name;
  QString password;
  QString sn;
};

} // namespace backend
