﻿#pragma once

#include <QJsonObject>
#include <QJsonValue>
#include <QObject>
#include <QVariant>

#include <base/OJSON.h>
#include <base/qbasic_types.h>

#include "../User.h"

namespace backend {

enum class UserType {
  NONE = 0,
  Member = 1,
  Teacher = 2,
};

class UserInfo : OJSON {
public:
  explicit UserInfo() {}

  explicit UserInfo(const UserInfo &info)
      : id(info.id), userType(info.userType), jid(info.jid), uin(info.uin),
        name(info.name), username(info.username), avatar(info.avatar) {}

  virtual ~UserInfo() {}

  int64 getId() const { return id; }

  const UserJID &getJID() const { return jid; }

  void setJID(const UserJID &_jid) { jid = _jid; }

  const QString &getUin() const { return uin; }

  const QString &getUsername() const { return username; }

  const QString &getName() const { return name; }

  const QString &getAvatar() const { return avatar; }

  int getUserType() const { return userType; }

  void fromJSON(const QJsonObject &data) {
    id = data.value("id").toInt();
    uin = data.value("uin").toString();
    name = data.value("name").toString();
    userType = data.value("userType").toInt();
    username = data.value("username").toString();
    avatar = data.value("avatar").toString();
  }

  QJsonObject toJSON() {
    QJsonObject qo;
    qo.insert("id", QJsonValue(id));
    qo.insert("uin", uin);
    qo.insert("name", name);
    qo.insert("avatar", avatar);
    qo.insert("userType", userType);
    return qo;
  }

private:
  int64 id = 0;
  int userType = -1;
  UserJID jid;
  QString uin;
  QString name;
  QString username;
  QString avatar;
};

} // namespace backend
