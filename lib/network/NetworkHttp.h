﻿#pragma once

#include <QByteArray>
#include <QFile>
#include <QJsonObject>
#include <QObject>
#include <QString>
#include <QUrl>
#include <memory>


#include <base/basic_types.h>
#include <base/timer.h>

#include "network.h"

class QNetworkAccessManager;
class QNetworkRequest;
class QNetworkCookie;

namespace network {

typedef struct {
  QByteArray byteArray;
  QString fileName;
  QString contentType;
} ByteInfo;

class NetworkHttp : public QObject {
  Q_OBJECT

public:

 explicit NetworkHttp(QObject *parent = nullptr);
  ~NetworkHttp() override;

  static FileResult parseDownloadResult(QJsonObject &result);
  void httpFinished();
  // void httpReadyRead();

  QJsonObject byteArrayToJSON(const QByteArray &buf);

  void wrapRequest(const QNetworkRequest &request, const QUrl &url);

  void load(const QUrl &url, Fn<void(QByteArray &)> fn);

  QByteArray get(const QUrl &url);

  void post(const QUrl &url, const QString &str, Fn<void(QByteArray)> fn);
  QByteArray post(const QUrl &url, const QString &str);

  void loadJSON(const QUrl &url, Fn<void(const QJsonObject &)> fn);
  void postJSON(const QUrl &url, const QJsonObject &data,
                Fn<void(const QJsonObject &)> fn);
  void postJSON(const QUrl &url, const QString &data,
                Fn<void(const QJsonObject &)> fn);

  virtual void
  PostFormData(const QUrl &url, QFile *file,
               Fn<void(int bytesSent, int bytesTotal)> uploadProgress,
               Fn<void(const QJsonObject &json)> readyRead);


  virtual void PostFormData(const QUrl &url, const QByteArray& byteArray,
                            const QString &contentType, const QString &filename,
                            Fn<void(int, int)> uploadProgress,
                            Fn<void(const QJsonObject &)> readyRead);

  virtual void openUrl(const QUrl &url);

protected:
  QNetworkAccessManager* _manager;
  QList<QNetworkCookie> _cookies;
};
} // namespace network
