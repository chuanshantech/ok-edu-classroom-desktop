/**
* Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
        http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/

#include "messenger.h"

#include "base/logs.h"

#include "lib/messenger/IM.h"
#include "lib/messenger/IMConference.h"
#include "lib/messenger/IMJingle.h"
#include "r.h"
#include <cassert>
#include <cstddef>
#include <memory>

namespace lib {
namespace IM {

static Messenger *self = nullptr;

Messenger::Messenger(QObject *parent)
    : QObject(parent), //
      _session(session::AuthSession::Instance()),
      _delayer(std::make_unique<base::DelayedCallTimer>()) //
{
  DEBUG_LOG(("begin"));

  connect(this, &Messenger::disconnect, this, &Messenger::onDisconnect);
  connect(this, &Messenger::started, this, &Messenger::onStarted);
  connect(this, &Messenger::stopped, this, &Messenger::onStopped);

  _im = new lib::IM::IM();
  //TODO 需要优化
  connect(_im, &lib::IM::IM::onStarted, [&]() { emit started(); });
  connect(_im, &lib::IM::IM::onStopped, [&]() { emit stopped(); });

  connect(_im, &lib::IM::IM::connectResult,
          [&](lib::IM::CONNECT_STATUS status) {
            DEBUG_LOG(("CONNECT_STATUS:%1").arg(status));
            if (status == lib::IM::CONNECT_STATUS::DISCONNECTED) {
              emit disconnect();
            }
          });

  /**
   * callback
   */
  initCallback();

  // connect the thread with the Core
  // connect(thread, &QThread::started, this, &Messenger::onStarted);
  // thread->setObjectName("IM Core");
  // moveToThread(thread);
  // thread->start();
  // self = this;
  DEBUG_LOG(("end"));
}
Messenger::~Messenger() {}

Messenger *Messenger::getInstance() {
  if (!self)
    self = new Messenger();
  return self;
}

void Messenger::start() {
  auto session = session::AuthSession::Instance();
  _im->start(session);
}

void Messenger::setMute(bool mute){
    _jingle->setMute(mute);
}

void Messenger::setRemoteMute(bool mute){
    _jingle->setRemoteMute(mute);
}

void Messenger::sendChatState(const QString &f, int state){
    _im->sendChatState(_im->wrapJid(f), static_cast<ChatStateType>(state));
}

void Messenger::onConnectResult(lib::IM::CONNECT_STATUS status) {
  DEBUG_LOG(("status:%1").arg(status));
  if (status == lib::IM::CONNECT_STATUS::DISCONNECTED) {
    _delayer->call(1000 * 5, [&]() {
      DEBUG_LOG(("retry connect..."));
      _im->doConnect();
    });
  }
}

void Messenger::onStarted() {
  DEBUG_LOG(("begin..."));

  /**
   * init room
   */
  initRoom();

  if (!_jingle.get()) {
    initJingle();
  } else {
    _jingle->clientChanged(_im->getClient());
  }
  //  _delayer->call(1000 * 3, [&]() {
  _im->sendPresence();
  _im->sendServiceDiscoveryItems();
  //  });
  DEBUG_LOG(("end..."));
}

void Messenger::onStopped() { DEBUG_LOG(("begin...")); }

bool Messenger::initCallback() {

  /**
   * selfHandlers
   */
    connect(_im, &lib::IM::IM::selfNicknameChanged,
            [&](const std::string& nickname) {
              for (auto handler : selfHandlers) {
                handler->onSelfNameChanged(qstring(nickname));
              }
            });
    connect(_im, &lib::IM::IM::selfAvatarChanged,
            [&](QByteArray avatar) {
              for (auto handler : selfHandlers) {
                handler->onSelfAvatarChanged(avatar);
              }
            });
    connect(_im, &lib::IM::IM::selfStatusChanged,
            [&](gloox::Presence::PresenceType type, const std::string& status) {
              for (auto handler : selfHandlers) {
                handler->onSelfStatusChanged(static_cast<Tox_User_Status>(type), status);
              }
            });
    connect(_im, &lib::IM::IM::selfIdChanged,
            [&](QString id) {
              for (auto handler : selfHandlers) {
                handler->onSelfIdChanged(id);
              }
            });
  /**
   * friendHandlers
   */
  connect(_im, &lib::IM::IM::receiveFriend, //
            [&](const FriendId& friendId) {
              for (auto handler : friendHandlers) {
                handler->onFriend(friendId);
              }
            });

  connect(
      _im, &lib::IM::IM::receiveFriendRequest,
      [&](const FriendId& friendId, QString msg) -> void {
        for (auto handler : friendHandlers) {
          handler->onFriendRequest(friendId, msg);
        }
      });
  connect(
      _im, &lib::IM::IM::receiveMessageReceipt,
      [&](QString friendId, QString receipt) -> void {
        for (auto handler : friendHandlers) {
          handler->onMessageReceipt(friendId, receipt);
        }
      });

  connect(
      _im, &lib::IM::IM::receiveFriendStatus,
      [&](QString friendId, gloox::Presence::PresenceType type) -> void {
        for (auto handler : friendHandlers) {
          handler->onFriendStatus(friendId, static_cast<Tox_User_Status>(type));
        }
      });

  connect(
      _im, &lib::IM::IM::receiveFriendChatState,
      [&](QString friendId, int state) -> void {
        for (auto handler : friendHandlers) {
          handler->onFriendChatState(friendId, state);
        }
      });

  connect(_im, &lib::IM::IM::receiveFriendMessage,
          [&](QString friendId, lib::IM::IMMessage msg) -> void {
            for (auto handler : friendHandlers) {
              OkMessage msg1 = {msg.id, msg.from, msg.body, msg.time};
              handler->onFriendMessage(friendId, msg1);
            }
          });

  connect(_im, &lib::IM::IM::receiveNicknameChange,
          [&](QString friendId, QString nickname) {
            for (auto handler : friendHandlers) {
              handler->onFriendNameChanged(friendId, nickname);
            }
          });

  connect(_im, &lib::IM::IM::receiveFriendAvatarChanged,
          [&](QString friendId, QByteArray avatar) {
            for (auto handler : friendHandlers) {
              handler->onFriendAvatarChanged(friendId, avatar);
            }
          });
  /**
   * callHandlers
   */
  connect(_im, &lib::IM::IM::receiveFriendCall,
          [&](QString friendId, QString callId, bool audio, bool video) {
            for (auto handler : callHandlers) {
              handler->onCall(friendId, callId, audio, video);
            }
          });

  connect(_im, &lib::IM::IM::receiveCallStateAccepted,
          [&](PeerId friendId, QString callId, bool video) {
            for (auto handler : callHandlers) {
              handler->receiveCallStateAccepted(friendId, callId, video);
            }
          });

  connect(_im, &lib::IM::IM::receiveCallStateRejected,
          [&](PeerId friendId, QString callId, bool video) {
            for (auto handler : callHandlers) {
              handler->receiveCallStateRejected(friendId, callId, video);
            }
          });

  connect(_im, &lib::IM::IM::receiveFriendHangup,
          [&](QString friendId) {
            for (auto handler : callHandlers) {
              handler->onHangup(
                  friendId,
                  TOXAV_FRIEND_CALL_STATE::TOXAV_FRIEND_CALL_STATE_FINISHED);
            }
          });


  connect(_im, &lib::IM::IM::groupInvite,
          [&](const QString& groupId, const QString &peerId, const QString& message) {
            for (auto handler : groupHandlers) {
              handler->onGroupInvite(groupId, peerId, message);
            }
          });

  connect(_im, &lib::IM::IM::groupListReceived,
          [&](const JID groupId) {
            for (auto handler : groupHandlers) {
              handler->onGroupList(FriendId(groupId));
            }
          });

  connect(_im, &lib::IM::IM::receiveRoomMessage,
          [&](QString groupId, PeerId peerId, lib::IM::IMMessage msg) -> void {
            for (auto handler : groupHandlers) {
              OkMessage msg1 = {msg.id, msg.from, msg.body, msg.time};
              handler->onGroupMessage(groupId, peerId, msg1);
            }
          });

  connect(_im, &lib::IM::IM::groupOccupants,
          [&](const QString &groupId, const uint size) -> void {
            for (auto handler : groupHandlers) {
              handler->onGroupOccupants(groupId, size);
            }
          });

  connect(
      _im, &lib::IM::IM::groupOccupantStatus,
      [&](const QString &groupId, const QString &peerId, bool online) -> void {
        for (auto handler : groupHandlers) {
          handler->onGroupOccupantStatus(groupId, peerId, online);
        }
      });

  connect(_im, &lib::IM::IM::groupRoomName,
          [&](const QString &groupId, const std::string &roomName) -> void {
            for (auto handler : groupHandlers) {
              handler->onGroupRoomName(groupId, roomName);
            }
          });

  connect(_im, &lib::IM::IM::receiveFileChunk,
          [&](const lib::IM::FriendId &friendId, std::string sId, int seq, std::string chunk) -> void {
            for (auto handler : fileHandlers) {
              handler->onFileRecvChunk(friendId.username, qstring(sId), seq, chunk);
            }
          });
  connect(_im, &lib::IM::IM::receiveFileFinished,
          [&](const lib::IM::FriendId &friendId ,std::string sId ) -> void {
            for (auto handler : fileHandlers) {
              handler->onFileRecvFinished(friendId.username, qstring(sId));
            }
          });
  return true;
}

bool Messenger::initJingle() {

  _jingle = std::make_unique<lib::IM::IMJingle>(_im);



  connect(_jingle.get(), &lib::IM::IMJingle::receiveFriendHangup,
          [&](QString friendId, TOXAV_FRIEND_CALL_STATE state) {
            for (auto handler : callHandlers) {
              assert(handler);
              handler->onHangup(friendId, state);
            }
          });

  connect(_jingle.get(), &lib::IM::IMJingle::receiveFriendVideoFrame,
          [&](const QString &friendId, //
              uint16_t w, uint16_t h,  //
              const uint8_t *y, const uint8_t *u, const uint8_t *v,
              int32_t ystride, int32_t ustride, int32_t vstride) {
            emit receiveFriendVideoFrame(friendId, //
                                         w, h,     //
                                         y, u, v,  //
                                         ystride, ustride, vstride);
          });

  connect(_jingle.get(), &lib::IM::IMJingle::receiveSelfVideoFrame,
          [&](uint16_t w, uint16_t h, //
              const uint8_t *y, const uint8_t *u, const uint8_t *v,
              int32_t ystride, int32_t ustride, int32_t vstride) {
            emit receiveSelfVideoFrame(w, h,    //
                                       y, u, v, //
                                       ystride, ustride, vstride);
          });

  connect(_jingle.get(), &lib::IM::IMJingle::receiveFileRequest,
          [&](const QString& friendId,  const FileHandler::File& file) {
            for (auto h : fileHandlers) {
              h->onFileRequest(friendId, file);
            }
          });
  connect(_jingle.get(), &lib::IM::IMJingle::sendFileInfo,
          [&](const QString& friendId, const FileHandler::File& file,
              int m_seq, int m_sentBytes, bool end) {
            for (auto h : fileHandlers) {
              h->onFileSendInfo(friendId, file, m_seq, m_sentBytes, end);
            }
          });
  connect(_jingle.get(), &lib::IM::IMJingle::sendFileAbort,
          [&](const QString& friendId, const FileHandler::File& file,
              int m_sentBytes) {
            for (auto h : fileHandlers) {
              h->onFileSendAbort(friendId, file, m_sentBytes);
            }
          });
  connect(_jingle.get(), &lib::IM::IMJingle::sendFileError,
          [&](const QString& friendId, const FileHandler::File& file,
              int m_sentBytes) {
            for (auto h : fileHandlers) {
              h->onFileSendError(friendId, file, m_sentBytes);
            }
          });

  return true;
}

bool Messenger::initRoom() {
  _im->getGroupList();
  return true;
}

void Messenger::onReceiveGroupMessage(lib::IM::IMMessage msg) {
  emit receivedGroupMessage(msg);
}

bool Messenger::sendToGroup(const QString &g,
                            const QString &msg,
                            QString &receiptNum) {

  DEBUG_LOG(("sendToGroup=>%1 msg:%2").arg(g).arg(msg));
  sentCount++;
  auto y = _im->sendToRoom(g, msg, "");
//  if (y) {
//    for (FriendHandler *fh : friendHandlers) {
//      fh->onMessageReceipt(g, receiptNum);
//    }
//  }
  return y;
}

bool Messenger::sendFileToFriend(const QString &f, const FileHandler::File &file) {
  DEBUG_LOG(("file:%1=>%2").arg(file.name).arg(f));
  return _jingle->sendFile(f, file);
}

bool Messenger::sendToFriend(const QString &f, const QString &msg,
                             QString &receiptNum) {
  DEBUG_LOG(("msg:%1=>%2").arg(msg).arg(f));
  sentCount++;

  auto y = _im->sendTo(_im->wrapJid(f), msg, receiptNum);
  DEBUG_LOG(("sendTo=>%1").arg(y));

  //  if (y) {
  //    for (auto fh : friendHandlers) {
  //      fh->onMessageReceipt(f, sentCount);
  //    }
  //  }
  return y;
}

void Messenger::receiptReceived(const std::string &f, QString receipt) {
  qDebug() <<"friendId:" <<&f<<"receiptNum:"<<receipt;
  return _im->sendReceiptReceived(f, receipt);
}

bool Messenger::callToFriend(const QString &f, const QString& sId, bool video) {
  DEBUG_LOG(("friend:%1 video:%2").arg((f)).arg(video));
  return _jingle->startCall(f, sId, video);
}

bool Messenger::createCallToPeerId(const lib::IM::PeerId &to,
                                   const QString &sId,
                                   bool video){

  DEBUG_LOG(("peerId:%1 video:%2").arg((to.toString())).arg(video));
  return _jingle->createCall(to, sId, video);
}



bool Messenger::answerToFriend(const QString &f,const QString &callId,  bool video) {
  DEBUG_LOG(("friend:%1 video:%2").arg((f)).arg(video));
  return _jingle->answer(f,callId, video);
}

bool Messenger::cancelToFriend(const QString &f, const QString &callId) {
  _jingle->cancelCall(f, callId);
  return true;
}

void Messenger::sendFriendRequest(const QString &f, const QString &message) {
  DEBUG_LOG(("friend:%1 msg:%2").arg((f)).arg(message));
  _im->addRosterItem(_im->wrapJid(f), message);
}

void Messenger::acceptFriendRequest(const QString &f) {
  _im->acceptFriendRequest(f);
}

void Messenger::rejectFriendRequest(const QString &f) {
  _im->rejectFriendRequest(f);
}

bool Messenger::removeFriend(const QString &f) {
  return _im->removeFriend(_im->wrapJid(f));
}

size_t Messenger::getFriendCount() { return _im->getRosterCount(); }

std::list<lib::IM::FriendId> Messenger::getFriendList() {
  std::list<lib::IM::FriendId> list;
  _im->getRosterList(list);
  return list;
}

void Messenger::addSelfHandler(SelfHandler *handler) {
  assert(handler);
  selfHandlers.push_back(handler);
}

void Messenger::addFriendHandler(FriendHandler *handler) {
  assert(handler);
  friendHandlers.push_back(handler);
}

void Messenger::addGroupHandler(GroupHandler *handler) {
  assert(handler);
  groupHandlers.push_back(handler);
}

void Messenger::addCallHandler(CallHandler *handler) {
  assert(handler);
  callHandlers.emplace_back(handler);
}

void Messenger::addFileHandler(FileHandler *handler) {
  assert(handler);
  fileHandlers.emplace_back(handler);
}

void Messenger::stop() { DEBUG_LOG(("..."));
  _im->stop();
}

PeerId Messenger::getSelfId() const {
  assert(_im);
  return _im->getSelfId();
}

Tox_User_Status Messenger::getSelfStatus() const {
  assert(_im);
  auto pt = gloox::Presence::PresenceType::Available;
  // _im->getPresenceType();
  return static_cast<Tox_User_Status>(pt);
}

void Messenger::setSelfNickname(const QString &nickname) {
  _im->setNickname(nickname);
}

QString Messenger::getSelfUsername() const { return _im->getSelfUsername(); }

void Messenger::changePassword(const QString &password){
  _im->changePassword(password);
}

void Messenger::onDisconnect() {
  _delayer->call(1000 * 5, [&]() {
    DEBUG_LOG(("retry connect..."));
    _im->retry();
  });
}

bool Messenger::createGroup(const QString &group) {
  _im->createRoom(_im->wrapRoomJid(group));
  return true;
}

bool Messenger::inviteGroup(const QString &group, const QString &f) {
  return _im->inviteToRoom(_im->wrapRoomJid(group), _im->wrapJid(f));
}

bool Messenger::removeGroup(const QString &group) {
  return _im->removeGroup(_im->wrapRoomJid(group));
}

void Messenger::setRoomName(const QString &group, const QString &nick) {
  _im->setRoomName(stdstring(group), stdstring(nick));
}

bool Messenger::callToGroup(const QString &g) {

  _conference = std::make_unique<lib::IM::IMConference>(_im->getClient(), nullptr);
  _conference->start(_im->wrapRoomJid(g));

  return true;
}

void Messenger::joinGroup(const QString &group) {
  DEBUG_LOG(("group:%1").arg(group));
  _im->joinRoom(_im->wrapRoomJid(group));
}

void Messenger::setSelfAvatar(const QByteArray &avatar) {
  _im->setAvatar(avatar);
}

void Messenger::rejectFileRequest(QString friendId, const FileHandler::File& file) {
  _jingle->rejectFileRequest(friendId, file);
}

void Messenger::acceptFileRequest(QString friendId, const FileHandler::File &file) {
  _jingle->acceptFileRequest(friendId, file);
}

void Messenger::cancelFile(QString fileId) {
  DEBUG_LOG(("fileId:%1").arg(fileId));

}

void Messenger::finishFileRequest(QString friendId, const FileHandler::File &file) {
  DEBUG_LOG(("fileId:%1").arg(friendId));
  _jingle->finishFileRequest(friendId, file);
}

void Messenger::finishFileTransfer(QString friendId,const FileHandler::File& file) {
  DEBUG_LOG(("friendId:%1 file:%2").arg(friendId).arg(file.name));
  _jingle->finishFileTransfer(friendId, file);
}


} // namespace IM
} // namespace lib
