﻿/**
*
Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan PubL v2.
You may obtain a copy of Mulan PubL v2 at:
        http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#pragma once

#include <QString>
#include <QMutex>
#include <memory>

#include "base/timer.h"
#include "messenger.h"
#include "messenger/IMMessage.h"
#include "messenger/IMRoomHelper.h"
#include "network/backend/domain/RoomInfo.h"
#include "session/AuthSession.h"

#include "lib/network/UserManager.h"
#include "lib/network/UserObserver.h"

#include <gloox/bookmarkhandler.h>
#include <gloox/bookmarkstorage.h>
#include <gloox/chatstatefilter.h>
#include <gloox/chatstatehandler.h>
#include <gloox/client.h>
#include <gloox/conference.h>
#include <gloox/connectionlistener.h>
#include <gloox/iqhandler.h>
#include <gloox/jinglecontent.h>
#include <gloox/jinglemessage.h>
#include <gloox/jinglesession.h>
#include <gloox/jinglesessionmanager.h>
#include <gloox/loghandler.h>
#include <gloox/logsink.h>
#include <gloox/messageeventfilter.h>
#include <gloox/messageeventhandler.h>
#include <gloox/messagesessionhandler.h>
#include <gloox/mucroom.h>
#include <gloox/mucroomconfighandler.h>
#include <gloox/mucroomhandler.h>
#include <gloox/personaleventingprotocolfilter.h>
#include <gloox/personaleventingprotocolhandler.h>
#include <gloox/presence.h>
#include <gloox/pubsub.h>
#include <gloox/pubsubitem.h>
#include <gloox/pubsubmanager.h>
#include <gloox/pubsubresulthandler.h>
#include <gloox/registration.h>
#include <gloox/registrationhandler.h>
#include <gloox/rosterlistener.h>
#include <gloox/rostermanager.h>
#include <gloox/vcardhandler.h>
#include <gloox/vcardmanager.h>
#include <set>

namespace lib {
namespace IM {

// using namespace session;
using namespace gloox;
using namespace gloox::PubSub;

class ConnectThread : public QThread {
  Q_OBJECT
public:
  ConnectThread(gloox::Client *client);
  virtual ~ConnectThread() override;
  void retry();
protected:
  virtual void run() override;

private:
  gloox::Client *_client;
};

// class IMJingle;

class IM : public QObject,
           public ConnectionListener,
           public RegistrationHandler,
           public ResultHandler,
           public RosterListener,
           public MUCRoomHandler,
           public MUCRoomConfigHandler,
           public MessageSessionHandler,
           public MessageHandler,
           public MessageEventHandler,
           public PersonalEventingProtocolHandler,
           public ChatStateHandler,
           public DiscoHandler,
           public DiscoNodeHandler,
           public PresenceHandler,
           public LogHandler,
           public VCardHandler,
           public TagHandler,
           public IqHandler,
           public BookmarkHandler,
           public network::UserObserver {
  Q_OBJECT
public:
  IM();
  ~IM();

  static IMMessage from(const gloox::Message &msg);
  // static gloox::Message to(const IMMessage &msg);
  static QString jid2Name(const gloox::JID &jid);

  Client *makeClient(const QString &username, const QString &token);


  /**
   * 个人相关
   * @param nickname
   */

  void setNickname(const QString &nickname);
  void setAvatar(const QByteArray &avatar);
  void changePassword(const QString &password);
  PeerId getSelfId();
  QString getSelfUsername();
  void sendPresence();
  void sendPresence(const JID& to);
  void sendReceiptReceived(const std::string &id,QString receiptNum);

  /**
   * 朋友相关
   */

  void addRosterItem(const gloox::JID &jid, const QString &msg);

  void acceptFriendRequest(const QString &);
  void rejectFriendRequest(const QString &);

  void sendReceiptRecieved(const JID &to, QString receipt);

  size_t getRosterCount();
  void getRosterList(std::list<FriendId> &);

  void retry();
  bool removeFriend(JID jid);

  /**
   * 群组相关
   * @param groupId
   * @param nick
   */

  void setRoomSubject(const std::string &groupId, const std::string &nick);
  void setRoomName(const std::string &groupId, const std::string &roomName);
  MUCRoom *findRoom(const std::string &username) const;
  bool inviteToRoom(const JID &roomJid, const JID &peerId);
  bool removeGroup(JID jid);


  virtual void doAvailablePresence(const Presence &presence,
                                   backend::UserJID &userJID);
  virtual void doUnAvailablePresence(const Presence &presence,
                                     backend::UserJID &userJID);

  // gloox log
  void handleLog(LogLevel level, LogArea area,
                 const std::string &message) override;

  virtual const JID &self() const { return _client->jid(); }

  virtual Client *getClient() const { return _client.get(); }

  virtual void setRole(const backend::UserJID &userJid,
                       const MUCRoomRole &role) const;

  virtual void setAffiliation(const backend::UserJID &userJid,
                              MUCRoomAffiliation aff);

  bool sendTo(const JID &to, const QString &msg, QString &id);

  bool sendToRoom(const QString &to, const QString &msg,
                  const QString &id = "") ;

  void doConnect();

  void doDisconnect();

  void joinRoom(const JID &jid);

  void createRoom(const JID &jid, const std::string &password = "");

  void getGroupList();

  void start(session::AuthSession *session);

  void stop();

  void interrupt();

  Presence::PresenceType getPresenceType();

  /**
   * 获取第一个在线终端resource
   * @param bare
   * @return
   */
  std::string getOnlineResource(const std::string &bare);

  /**
   * 获取全部在线终端
   * @param bare
   * @return
   */
  std::set<std::string> getOnlineResources(const std::string &bare);
  void updateOnlineStatus(const std::string &bare,
                            const std::string &resource,
                            Presence::PresenceType presenceType);

  /**
   * jingle-message
   *    发起呼叫邀请
   */
  void proposeJingleMessage(const QString &friendId,
                          const QString &callId,
                          bool video);

  void rejectJingleMessage(const QString &friendId,
                           const QString &callId);

  void acceptJingleMessage(const QString &friendId,
                            const QString &callId);

  void retractJingleMessage(const QString &friendId,
                            const QString &callId);

  void doJingleMessage(const PeerId &peerId,
                         const gloox::Jingle::JingleMessage *jm);


  void timesUp();

  [[nodiscard]] gloox::JID wrapJid(const QString &f) const;

  [[nodiscard]] gloox::JID wrapRoomJid(const QString &group) const;
  void sendChatState(const JID &to, ChatStateType state) ;
protected:
  /**
   * iq handlers
   * @param iq
   * @return
   */
  bool handleIq(const IQ &iq) override;
  void handleIqID(const IQ &iq, int context) override;

  /**
   * tag handlers
   */
  void handleTag(Tag *tag) override;

  /**
   * connect handlers
   */
  void onConnect() override;
  void onDisconnect(ConnectionError e) override;
  bool onTLSConnect(const CertInfo &info) override;


  /**
   * Registration
   */
  virtual void handleRegistrationFields( const JID& from, int fields,
                                        std::string instructions ) override;

  virtual void handleAlreadyRegistered( const JID& from ) override;

  virtual void handleRegistrationResult( const JID& from, RegistrationResult regResult ) override;

  virtual void handleDataForm( const JID& from, const DataForm& form ) override;

  virtual void handleOOB( const JID& from, const OOB& oob ) override;


  /**
   * vCard
   * @param jid
   * @param vch
   */
  void handleVCard(const JID &jid, const VCard *vcard) override;

  void handleVCardResult(VCardContext context, const JID &jid,
                         StanzaError se = StanzaErrorUndefined) override;

  /**
   * RosterListener
   * @param msg
   * @param session
   */
  void handleItemAdded(const gloox::JID &) override;
  void handleItemSubscribed(const JID &jid) override;
  void handleItemRemoved(const JID &jid) override;
  void handleItemUpdated(const JID &jid) override;
  void handleItemUnsubscribed(const JID &jid) override;
  void handleRoster(const Roster &roster) override;
  void handleRosterPresence(const RosterItem &item, const std::string &resource,
                            Presence::PresenceType presence,
                            const std::string &msg) override;
  void handleSelfPresence(const RosterItem &item, const std::string &resource,
                          Presence::PresenceType presence,
                          const std::string &msg) override;

  bool handleSubscriptionRequest(const JID &jid,
                                 const std::string &msg) override;

  bool handleUnsubscriptionRequest(const JID &jid,
                                   const std::string &msg) override;

  void handleNonrosterPresence(const Presence &presence) override;

  void handleRosterError(const IQ &iq) override;

  // MUC config
  void handleMUCConfigList(MUCRoom *room, const MUCListItemList &items,
                           MUCOperation operation) override;

  void handleMUCConfigForm(MUCRoom *room, const DataForm &form) override;

  void handleMUCConfigResult(MUCRoom *room, bool success,
                             MUCOperation operation) override;

  void handleMUCRequest(MUCRoom *room, const DataForm &form) override;

  // MessageSessionHandler
  void handleMessage(const gloox::Message &msg,
                     MessageSession *session = nullptr) override;
  void handleMessageSession(MessageSession *session) override;
  void handleMessageEvent(const JID &from, MessageEventType event) override;
  void handleChatState(const JID &from, ChatStateType state) override;

  // PersonalEventingProtocolHandler
  void
  handlePersonalEventingProtocol(const JID &from, const std::string &node,
                                 const PubSub::ItemList &items,
                                 const PersonalEventingProtocolFilter *filter);

  // MUC handler
  void handleMUCParticipantPresence(MUCRoom *room,
                                    const MUCRoomParticipant participant,
                                    const Presence &presence) override;

  void handleMUCMessage(MUCRoom *room, const gloox::Message &msg,
                        bool priv) override;

  bool handleMUCRoomCreation(MUCRoom *room) override;

  void handleMUCSubject(MUCRoom *room, const std::string &nick,
                        const std::string &subject) override;

  void handleMUCInviteDecline(MUCRoom *room, const JID &invitee,
                              const std::string &reason) override;

  void handleMUCError(MUCRoom *room, StanzaError error) override;

  void handleMUCInfo(MUCRoom *room, int features, const std::string &name,
                     const DataForm *infoForm) override;

  void handleMUCItems(MUCRoom *room, const Disco::ItemList &items) override;

  // Disco handler
  void handleDiscoInfo(const JID &from, const Disco::Info &,
                       int ontext) override;

  void handleDiscoItems(const JID &from, const Disco::Items &,
                        int context) override;

  void handleDiscoError(const JID &from, const gloox::Error *,
                        int context) override;
  // DiscoNodeHandler
  virtual StringList handleDiscoNodeFeatures( const JID& from, const std::string& node ) override;


  virtual Disco::IdentityList handleDiscoNodeIdentities( const JID& from,
                                                        const std::string& node ) override;


  virtual Disco::ItemList handleDiscoNodeItems( const JID& from, const JID& to,
                                               const std::string& node = EmptyString ) override;

  // reimplemented from DiscoNodeHandler
  virtual void handleDiscoItemsResult(const JID &from, const Disco::ItemList &items) override;


  // Presence handler
  void handlePresence(const Presence &presence) override;

  // UserObserver
  void onLogin() override;
  void onLogout() override;

  //
  void setUserManager(UserManager *userManager);

  /**
   * Receives the payload for an item.
   *
   * @param service Service hosting the queried node.
   * @param node ID of the parent node.
   * @param entry The complete item Tag (do not delete).
   */
  void handleItem(const JID &service, const std::string &node,
                  const Tag *entry) override;

  /**
   * Receives the list of Items for a node.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the queried node.
   * @param node ID of the queried node (empty for the root node).
   * @param itemList List of contained items.
   * @param error Describes the error case if the request failed.
   *
   * @see Manager::requestItems()
   */
  void handleItems(const std::string &id, const JID &service,
                   const std::string &node, const ItemList &itemList,
                   const gloox::Error *error = 0) override;

  /**
   * Receives the result for an item publication.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the queried node.
   * @param node ID of the queried node. If empty, the root node has been
   * queried.
   * @param itemList List of contained items.
   * @param error Describes the error case if the request failed.
   *
   * @see Manager::publishItem
   */
  void handleItemPublication(const std::string &id, const JID &service,
                             const std::string &node, const ItemList &itemList,
                             const gloox::Error *error = 0) override;

  /**
   * Receives the result of an item removal.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the queried node.
   * @param node ID of the queried node. If empty, the root node has been
   * queried.
   * @param itemList List of contained items.
   * @param error Describes the error case if the request failed.
   *
   * @see Manager::deleteItem
   */
  void handleItemDeletion(const std::string &id, const JID &service,
                          const std::string &node, const ItemList &itemList,
                          const gloox::Error *error = 0) override;

  /**
   * Receives the subscription results. In case a problem occured, the
   * Subscription ID and SubscriptionType becomes irrelevant.
   *
   * @param id The reply IQ's id.
   * @param service PubSub service asked for subscription.
   * @param node Node asked for subscription.
   * @param sid Subscription ID.
   * @param jid Subscribed entity.
   * @param subType Type of the subscription.
   * @param error Subscription Error.
   *
   * @see Manager::subscribe
   */
  void handleSubscriptionResult(const std::string &id, const JID &service,
                                const std::string &node, const std::string &sid,
                                const JID &jid,
                                const gloox::PubSub::SubscriptionType subType,
                                const gloox::Error *error = 0) override;

  /**
   * Receives the unsubscription results. In case a problem occured, the
   * subscription ID becomes irrelevant.
   *
   * @param id The reply IQ's id.
   * @param service PubSub service.
   * @param error Unsubscription Error.
   *
   * @see Manager::unsubscribe
   */
  void handleUnsubscriptionResult(const std::string &id, const JID &service,
                                  const gloox::Error *error = 0) override;

  /**
   * Receives the subscription options for a node.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the queried node.
   * @param jid Subscribed entity.
   * @param node ID of the node.
   * @param options Options DataForm.
   * @param sid An optional subscription ID.
   * @param error Subscription options retrieval Error.
   *
   * @see Manager::getSubscriptionOptions
   */
  void handleSubscriptionOptions(const std::string &id, const JID &service,
                                 const JID &jid, const std::string &node,
                                 const DataForm *options,
                                 const std::string &sid = EmptyString,
                                 const gloox::Error *error = 0) override;

  /**
   * Receives the result for a subscription options modification.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the queried node.
   * @param jid Subscribed entity.
   * @param node ID of the queried node.
   * @param sid An optional subscription ID.
   * @param error Subscription options modification Error.
   *
   * @see Manager::setSubscriptionOptions
   */
  void handleSubscriptionOptionsResult(const std::string &id,
                                       const JID &service, const JID &jid,
                                       const std::string &node,
                                       const std::string &sid = EmptyString,
                                       const gloox::Error *error = 0) override;

  /**
   * Receives the list of subscribers to a node.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the node.
   * @param node ID of the queried node.
   * @param list Subscriber list.
   * @param error Subscription options modification Error.
   *
   * @see Manager::getSubscribers
   */
  void handleSubscribers(const std::string &id, const JID &service,
                         const std::string &node, const SubscriptionList &list,
                         const gloox::Error *error = 0) override;

  /**
   * Receives the result of a subscriber list modification.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the node.
   * @param node ID of the queried node.
   * @param list Subscriber list.
   * @param error Subscriber list modification Error.
   *
   * @see Manager::setSubscribers
   */
  void handleSubscribersResult(const std::string &id, const JID &service,
                               const std::string &node,
                               const SubscriberList *list,
                               const gloox::Error *error = 0) override;

  /**
   * Receives the affiliate list for a node.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the node.
   * @param node ID of the queried node.
   * @param list Affiliation list.
   * @param error Affiliation list retrieval Error.
   *
   * @see Manager::getAffiliates
   */
  void handleAffiliates(const std::string &id, const JID &service,
                        const std::string &node, const AffiliateList *list,
                        const gloox::Error *error = 0) override;

  /**
   * Handle the affiliate list for a specific node.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the node.
   * @param node ID of the node.
   * @param list The Affiliate list.
   * @param error Affiliation list modification Error.
   *
   * @see Manager::setAffiliations
   */
  void handleAffiliatesResult(const std::string &id, const JID &service,
                              const std::string &node,
                              const AffiliateList *list,
                              const gloox::Error *error = 0) override;

  /**
   * Receives the configuration for a specific node.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the node.
   * @param node ID of the node.
   * @param config Configuration DataForm.
   * @param error Configuration retrieval Error.
   *
   * @see Manager::getNodeConfig
   */
  void handleNodeConfig(const std::string &id, const JID &service,
                        const std::string &node, const DataForm *config,
                        const gloox::Error *error = 0) override;

  /**
   * Receives the result of a node's configuration modification.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the node.
   * @param node ID of the node.
   * @param error Configuration modification Error.
   *
   * @see Manager::setNodeConfig
   */
  void handleNodeConfigResult(const std::string &id, const JID &service,
                              const std::string &node,
                              const gloox::Error *error = 0) override;

  /**
   * Receives the result of a node creation.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the node.
   * @param node ID of the node.
   * @param error Node creation Error.
   *
   * @see Manager::setNodeConfig
   */
  void handleNodeCreation(const std::string &id, const JID &service,
                          const std::string &node,
                          const gloox::Error *error = 0) override;

  /**
   * Receives the result for a node removal.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the node.
   * @param node ID of the node.
   * @param error Node removal Error.
   *
   * @see Manager::deleteNode
   */
  void handleNodeDeletion(const std::string &id, const JID &service,
                          const std::string &node,
                          const gloox::Error *error = 0) override;

  /**
   * Receives the result of a node purge request.
   *
   * @param id The reply IQ's id.
   * @param service Service hosting the node.
   * @param node ID of the node.
   * @param error Node purge Error.
   *
   * @see Manager::purgeNode
   */
  void handleNodePurge(const std::string &id, const JID &service,
                       const std::string &node,
                       const gloox::Error *error = 0) override;

  /**
   * Receives the Subscription list for a specific service.
   *
   * @param id The reply IQ's id.
   * @param service The queried service.
   * @param subMap The map of node's subscription.
   * @param error Subscription list retrieval Error.
   *
   * @see Manager::getSubscriptions
   */
  void handleSubscriptions(const std::string &id, const JID &service,
                           const SubscriptionMap &subMap,
                           const gloox::Error *error = 0) override;

  /**
   * Receives the Affiliation map for a specific service.
   *
   * @param id The reply IQ's id.
   * @param service The queried service.
   * @param affMap The map of node's affiliation.
   * @param error Affiliation list retrieval Error.
   *
   * @see Manager::getAffiliations
   */
  void handleAffiliations(const std::string &id, const JID &service,
                          const AffiliationMap &affMap,
                          const gloox::Error *error = 0) override;

  /**
   * Receives the default configuration for a specific node type.
   *
   * @param id The reply IQ's id.
   * @param service The queried service.
   * @param config Configuration form for the node type.
   * @param error Default node config retrieval Error.
   *
   * @see Manager::getDefaultNodeConfig
   */
  void handleDefaultNodeConfig(const std::string &id,  //
                               const JID &service,     //
                               const DataForm *config, //
                               const gloox::Error *error = 0) override;

  void handleBookmarks(const BookmarkList &bList, //
                       const ConferenceList &cList) override;

private:
  /**
   * 连接状态
   */
  bool started;

  JID loginJid;
  QString _username;
  QString _password;

//  std::unique_ptr<base::Timer> timer_;

  std::shared_ptr<base::DelayedCallTimer> delayCaller_;

  std::unique_ptr<Client> _client;

  std::unique_ptr<ConnectThread> _connectThread;


  /**
   * k: sessionId
   * v: messageSession
   */
  std::map<std::string, MessageSession *> sessionMap;

  /**
   * k: bare
   * v: sessionId
   */
  std::map<std::string, std::string> sessionIdMap;

  /**
   * 在线
   * key: bare value:[resource，resource,...]
   */
  std::map<std::string, std::set<std::string>> onlineMap;

  session::AuthSession *_session = nullptr;

  std::unique_ptr<VCardManager> vCardManager;
  std::unique_ptr<PubSub::Manager> pubSubManager;
  std::unique_ptr<BookmarkStorage> bookmarkStorage;
  std::unique_ptr<Registration> mRegistration;

  UserManager *_userManager;

  std::unique_ptr<MessageEventFilter> m_messageEventFilter;

  std::map<std::string, ChatStateFilter*> m_chatStateFilters;

  std::map<JID, MUCRoom *> m_roomMap;

  std::unique_ptr<MUCRoom> m_pRoom;

  std::shared_ptr<backend::RoomInfo> m_roomInfo;


  QRecursiveMutex m_clientMutex;
  std::mutex _join_room_mutex;
  QThread *thread;

  // 连接状态
  CONNECT_STATUS _status = CS_NONE;
  Presence::PresenceType selfPresType = gloox::Presence::Unavailable;

  std::string roomName;

  //自己信息
  SelfInfo m_selfInfo;
  QByteArray selfAvatar;

  QString m_addFriendMsg;

  /**
   * TODO 暂时这种
   */
  JID currentJID;

  QMap<PeerId, Jingle::RTP::Medias> mPeerRequestMedias;

  //发送消息的id
  std::list<std::string> sendIds;

  ConferenceList mConferenceList;
  BookmarkList mBookmarkList;

signals:
  void connectResult(CONNECT_STATUS);

  void receiveRooms(std::list<backend::RoomInfo> &);

  void receiveRoomMessage(QString groupId, PeerId friendId,
                          lib::IM::IMMessage);

  // friend events
  void receiveFriend(const FriendId& friendId);

  void receiveFriendRequest(const FriendId& friendId, QString msg);

  //TODO(zhaohui): 修改 QString friendId ==> const FriendId& friendId
  void receiveMessageReceipt(QString friendId, QString receipt);

  void receiveFriendRemoved(QString friendId);


  void receiveFriendStatus(QString friendId, gloox::Presence::PresenceType type);

  void receiveFriendMessage(QString peerId, lib::IM::IMMessage);

  void receiveNicknameChange(QString friendId, QString nickname);

  void receiveFriendAvatarChanged(QString friendId, QByteArray avatar);

  void receiveFriendChatState(QString friendId, int state);

  /**
   * Call events
   * @param friendId
   * @param audio
   * @param video
   */
  void receiveFriendCall(QString friendId, QString callId, bool audio, bool video);
  void receiveFriendHangup(QString friendId);
  //对方状态变化
  void receiveCallStateAccepted(PeerId peerId, QString callId, bool video);
  void receiveCallStateRejected(PeerId peerId, QString callId, bool video);

  void receiveFileChunk(const FriendId &friendId, std::string sId,
                        int seq, std::string chunk);
  void receiveFileFinished(const FriendId &friendId, std::string sId);

  // Self events
  void selfIdChanged(QString id);
  void selfNicknameChanged(const std::string& nickname);
  void selfAvatarChanged(QByteArray avatar);
  void selfStatusChanged(gloox::Presence::PresenceType type, const std::string& status);

  void onStarted();
  void onStopped();

  void groupListReceived(const JID &groupId);
  void groupOccupants(const QString &groupId, const uint size);
  void groupOccupantStatus(const QString &groupId, const QString &peerId,
                           bool online);
  void groupInvite(const QString &groupId, const QString &peerId,
                   const QString &message);
  void groupRoomName(const QString &groupId, const std::string &name);

public slots:
  void sendServiceDiscoveryItems();
  void sendServiceDiscoveryInfo(const JID &item);

  void onSelfNicknameChanged(const std::string& nickname);
  void onRoomReceived(const JID &jid);
};

} // namespace messenger
} // namespace network
