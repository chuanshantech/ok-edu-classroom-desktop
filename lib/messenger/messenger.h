/**
* Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
        http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/

#pragma once

#include "IMMessage.h"
#include "base/timer.h"
#include "tox/tox.h"
#include "tox/toxav.h"



#include <QDateTime>
#include <QString>
#include <array>
#include <cstddef>
#include <memory>


namespace lib {
namespace IM {

class IM;
class IMJingle;
class IMConference;

enum CONNECT_STATUS;
} // namespace messenger
} // namespace network

namespace session {
class AuthSession;
}

namespace lib {
namespace IM {

// NOTE: This could be extended in the future to handle all text processing (see
// ChatMessage::createChatMessage)
enum class MessageMetadataType {
  selfMention,
};

// May need to be extended in the future to have a more varianty type (imagine
// if we wanted to add message replies and shoved a reply id in here)
struct MessageMetadata {
  MessageMetadataType type;
  // Indicates start position within a Message::content
  size_t start;
  // Indicates end position within a Message::content
  size_t end;
};

struct OkMessage {
  QString id;
  QString sender;
  QString content;
  QDateTime timestamp;
};

class SelfHandler {
public:
  virtual void onSelfIdChanged(QString id) = 0;
  virtual void onSelfNameChanged(QString name) = 0;
  virtual void onSelfAvatarChanged(QByteArray avatar) = 0;
  virtual void onSelfStatusChanged(Tox_User_Status status, const std::string &msg) = 0;
};

class FriendHandler {
public:
  virtual void onFriend(const FriendId& friendId) = 0;
  virtual void onFriendRequest(const FriendId& friendId, QString msg) = 0;
  virtual void onFriendRemoved(QString friendId) = 0;
//  virtual void onFriendList(const std::list<QString> &list) = 0;
  virtual void onFriendStatus(QString friendId, Tox_User_Status status) = 0;
  virtual void onFriendMessage(QString friendId, OkMessage message) = 0;
  virtual void onFriendNameChanged(QString friendId, QString name) = 0;
  virtual void onFriendAvatarChanged(QString friendId, QByteArray avatar) = 0;
  virtual void onFriendChatState(QString friendId, int state) = 0;
  virtual void onMessageReceipt(QString friendId, QString receipt) = 0;


};

class GroupHandler {
public:
  virtual void onGroupList(const FriendId &groupId) = 0;

  virtual void onGroupInvite(const QString &groupId, //
                             const QString &peerId,  //
                             const QString &message) = 0;

  virtual void onGroupMessage(const QString &groupId, //
                              const PeerId &peerId,  //
                              const OkMessage &message) = 0;

  virtual void onGroupOccupants(const QString &groupId, uint size) = 0;

  virtual void onGroupRoomName(const QString &groupId, const std::string &roomName) = 0;

  virtual void onGroupOccupantStatus(const QString &groupId, const QString &peerId, bool online) = 0;
};

class CallHandler {
public:
  virtual void onCall(const QString &friendId, const QString &callId,//
                      bool audio, bool video) = 0;

  virtual void receiveCallStateAccepted(PeerId friendId, QString callId, bool video)=0;

  virtual void receiveCallStateRejected(PeerId friendId, QString callId, bool video)=0;

  virtual void onHangup(const QString &friendId, //
                        TOXAV_FRIEND_CALL_STATE state) = 0;

  virtual void onSelfVideoFrame(uint16_t w, uint16_t h, //
                                const uint8_t *y,       //
                                const uint8_t *u,       //
                                const uint8_t *v,       //
                                int32_t ystride,        //
                                int32_t ustride,        //
                                int32_t vstride) = 0;

  virtual void onFriendVideoFrame(const QString &friendId, //
                                  uint16_t w, uint16_t h,  //
                                  const uint8_t *y,        //
                                  const uint8_t *u,        //
                                  const uint8_t *v,        //
                                  int32_t ystride,         //
                                  int32_t ustride,         //
                                  int32_t vstride) = 0;
};

class FileHandler {
public:

  struct File {
    QString id;
    QString name;
    QString sId; //session id
    QString path;
    quint64 size;


    File() = default;
    File(QString id_,
         QString name_,
         QString sId_, //session id,
         QString path = "",
         quint64 size_ = 0)://
    id(id_), name(name_), sId(sId_), path(path), size(size_){

    }
  };

  virtual void onFileRequest(const QString &friendId, const File& file) = 0;
  virtual void onFileRecvChunk(const QString &friendId, const QString& fileId, int seq, const std::string&) = 0;
  virtual void onFileRecvFinished(const QString &friendId, const QString& fileId) = 0;
  virtual void onFileSendInfo(const QString &friendId, const File& file,int m_seq, int m_sentBytes, bool end) = 0;
  virtual void onFileSendAbort(const QString &friendId, const File& file, int m_sentBytes) = 0;
  virtual void onFileSendError(const QString &friendId, const File& file, int m_sentBytes) = 0;

};

class Messenger : public QObject {
  Q_OBJECT
public:
  using Ptr = std::shared_ptr<Messenger>;

  ~Messenger() override;

  static Messenger *getInstance();
  void start();
  void stop();
  PeerId getSelfId() const;
  QString getSelfUsername() const;
  Tox_User_Status getSelfStatus() const;


  void addSelfHandler(SelfHandler *);
  void addFriendHandler(FriendHandler *);
  void addGroupHandler(GroupHandler *);
  void addCallHandler(CallHandler *);
  void addFileHandler(FileHandler *);


  size_t getFriendCount();

  std::list<lib::IM::FriendId> getFriendList();

  bool sendToGroup(const QString &g, const QString &msg, QString &receiptNum);

  bool sendToFriend(const QString &f, const QString &msg, QString &receiptNum);

  void receiptReceived(const std::string &f, QString receipt);

  bool sendFileToFriend(const QString &f, const FileHandler::File &file);

  bool initCallback();

  bool initJingle();


  // ============= setXX============/
  void setSelfNickname(const QString &nickname);
  void changePassword(const QString &password);
  void setSelfAvatar(const QByteArray &avatar);
  //void setMute(bool mute);

  /**
   * Friend (audio/video)
   */
  //添加好友
  void sendFriendRequest(const QString &f, const QString &message);
  //接受朋友邀请
  void acceptFriendRequest(const QString &f);
  //拒绝朋友邀请
  void rejectFriendRequest(const QString &f);

  /**
   * Call
   */
   //发起呼叫邀请
  bool callToFriend(const QString &f,const QString& sId, bool video);

  //创建呼叫
  bool createCallToPeerId(const lib::IM::PeerId &to, const QString &sId, bool video);

  bool answerToFriend(const QString &f, const QString &callId, bool video);
  bool cancelToFriend(const QString &f, const QString &sId);
  bool removeFriend(const QString &f);
  //静音功能
  void setMute(bool mute);
  void setRemoteMute(bool mute);
  void sendChatState(const QString &f, int state);
  /**
   * Group
   */
  bool initRoom();
  bool callToGroup(const QString &g);
  bool createGroup(const QString &group);
  void joinGroup(const QString &group);
  void setRoomName(const QString &group, const QString &nick);
  bool inviteGroup(const QString &group, const QString &f);
  bool removeGroup(const QString &group);

  /**
   * File
   */
  void rejectFileRequest(QString friendId, const FileHandler::File& file);
  void acceptFileRequest(QString friendId, const FileHandler::File& file);
  void finishFileRequest(QString friendId, const FileHandler::File &file);
  void finishFileTransfer(QString friendId, const FileHandler::File& file);
  void cancelFile(QString fileId);

private:
  explicit Messenger(QObject *parent = nullptr);
  session::AuthSession *_session;
  lib::IM::IM *_im;
  std::unique_ptr<lib::IM::IMJingle> _jingle;
  std::unique_ptr<lib::IM::IMConference> _conference;


  std::vector<FriendHandler *> friendHandlers;
  std::vector<SelfHandler *> selfHandlers;
  std::vector<GroupHandler *> groupHandlers;
  std::vector<CallHandler *> callHandlers;
  std::vector<FileHandler *> fileHandlers;

  size_t sentCount = 0;
  std::unique_ptr<base::DelayedCallTimer> _delayer;

signals:
  void started();
  void stopped();
  void disconnect();



  void receivedGroupMessage(lib::IM::IMMessage imMsg);
  void messageSent(const OkMessage &message);

  void receiveSelfVideoFrame(uint16_t w, uint16_t h, //
                             const uint8_t *y,       //
                             const uint8_t *u,       //
                             const uint8_t *v,       //
                             int32_t ystride,        //
                             int32_t ustride,        //
                             int32_t vstride);

  void receiveFriendVideoFrame(const QString &friendId, //
                               uint16_t w, uint16_t h,  //
                               const uint8_t *y,        //
                               const uint8_t *u,        //
                               const uint8_t *v,        //
                               int32_t ystride,         //
                               int32_t ustride,         //
                               int32_t vstride);



private slots:
  void onConnectResult(lib::IM::CONNECT_STATUS);
  void onStarted();
  void onStopped();
  void onReceiveGroupMessage(lib::IM::IMMessage imMsg);
  void onDisconnect();

};

} // namespace IM
} // namespace lib
