﻿#include "lib/network/UserManager.h"

#include <jid.h>
#include <memory>

#include "lib/network/NetworkManager.h"
#include <base/logs.h>
#include <network/backend/UserService.h>
#include <messenger/IM.h>

namespace lib {
namespace IM {

using namespace session;

UserManager::UserManager(AuthSession *session) : _session(session) {

  DEBUG_LOG_S(L_INFO) << "begin...";
  _userMap = (std::make_shared<UserMap>());
  connect(_session, SIGNAL(connectResult(network::CONNECT_STATUS)), this,
          SLOT(onConnectResult(network::CONNECT_STATUS)));
  DEBUG_LOG_S(L_INFO) << "end...";
}

UserManager::~UserManager() {}

int UserManager::userSize() {
  std::lock_guard<std::mutex> lock(_mutex);
  return _userMap.get() ? _userMap->size() : 0;
}

bool UserManager::addUserUin(const backend::UserJID &jid) {
  if (jid.id.username() == "focus") {
    return false;
  }

  const QString uin = qstring(jid.id.username());
  DEBUG_LOG(("addUserUin:%1").arg(uin));

  std::lock_guard<std::mutex> lock(_mutex);

  emit joined(jid);

  //    UserService _userService(_session);
  //    _userService.requestUserInfo(uin, [this, uin, jid](backend::UserInfo&
  //    info) {
  //        if (0 < info.getId()) {
  //            info.setJID(jid);
  //            _userMap->insert(uin, info);
  //        }
  //    });
  return true;
}

bool UserManager::removeUserUin(const backend::UserJID &jid) {
  const QString uin = qstring(jid.id.username());

  if (uin == "focus") {
    return false;
  }

  std::lock_guard<std::mutex> lock(_mutex);
  DEBUG_LOG(("removeUserUin:%1").arg(uin));

  emit left(jid);

  bool b = 0 < _userMap->remove(uin);
  return b;
}

void UserManager::setPeerStatus(backend::UserJID &jid,
                                backend::PeerStatus &status) {
  emit statusEvent(jid, status);
}

void UserManager::registerObserver(network::UserObserver *userObserver) {
  DEBUG_LOG(("begin for:%1").arg(POINTER_TO_STR(userObserver)));
  if (!_userObservers.get()) {
    _userObservers = std::make_shared<UserObserverList>();
  }
  _userObservers->append(userObserver);
  DEBUG_LOG(("end"));
}

void UserManager::onConnectResult(network::CONNECT_STATUS status) {
  DEBUG_LOG_S(L_INFO) << "status:" << (int)status;
  switch (status) {
  case CONNECT_STATUS::CONNECTED: {
    DEBUG_LOG_S(L_INFO) << "CONNECTED";
    UserService _userService(_session);
    _userService.getByUin(
        _session->uin(), [&](const backend::UserInfo &info) {
          DEBUG_LOG(("UserInfo:%1").arg(info.getId()));
          if (0 < info.getId()) {
            _myInfo = std::make_unique<backend::UserInfo>(info);
            UserObserverList *ql = _userObservers.get();
            if (ql) {
              for (network::UserObserver *userObserver : *ql) {
                userObserver->onLogin();
              }
            }
          }
        });
    break;
  }
  case CONNECT_STATUS::DISCONNECTED: {
    DEBUG_LOG_S(L_INFO) << "DISCONNECTED";
    break;
  }
  default:
    DEBUG_LOG_S(L_INFO) << "default.";
    break;
  }
}

void UserManager::addUserCapabilities(QString &uin, const Capabilities *caps) {
  if (!caps) {
    return;
  }
}

} // namespace messenger
} // namespace network
