﻿/**
* Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
        http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/

#include "IM.h"

// TODO resolve conflict DrawText in WinUser.h
#undef DrawText

#include <QCryptographicHash>
#include <QPixmap>
#include <QMutex>
#include <list>
#include <string>
#include <thread>

#include "base/logs.h"
#include "gloox/conference.h"
#include "gloox/receipt.h"

#include "r.h"

#include <QPixmap>
#include <gloox/avatar.h>
#include <gloox/base64.h>
#include <gloox/capabilities.h>
#include <gloox/chatstate.h>
#include <gloox/disco.h>
#include <gloox/inbandbytestream.h>
#include <gloox/message.h>
#include <gloox/nickname.h>
#include <gloox/pubsubevent.h>
#include <gloox/rostermanager.h>
#include <gloox/vcardupdate.h>

namespace lib {
namespace IM {

using namespace gloox;

ConnectThread::ConnectThread(Client *client) : _client(client) {
  setObjectName("IM Connect");
}

ConnectThread::~ConnectThread() {}

void ConnectThread::run() {
  DEBUG_LOG(("XMPP connect..."))
  _client->connect(true);
}

void ConnectThread::retry() {
  DEBUG_LOG(("XMPP retry..."))
  _client->connect(true);
}

#define GROUP_LIST 0
/*
 * 1、加载好友列表
 * 2、加载群聊列表
 * 3、加载好友消息
 * 4、加载群聊消息
 */
IM::IM()
    : loginJid(JID(XMPP_SERVER_HOST)), //
      delayCaller_((std::make_unique<base::DelayedCallTimer>())) {

  DEBUG_LOG(("begin..."));
  connect(this, &IM::selfNicknameChanged, this, &IM::onSelfNicknameChanged, Qt::DirectConnection);
  connect(this, &IM::groupListReceived, this, &IM::onRoomReceived, Qt::DirectConnection);

  // qRegisterMetaType
  qRegisterMetaType<backend::UserId>("backend::UserId&");
  qRegisterMetaType<FileHandler::File>("FileHandler::File");
  qRegisterMetaType<JID>("JID");
  qRegisterMetaType<UserId>("UserId");
  qRegisterMetaType<FriendId>("lib::IM::FriendId");
  qRegisterMetaType<PeerId>("lib::IM::PeerId");
  qRegisterMetaType<lib::IM::IMMessage>("lib::IM::IMMessage");



  DEBUG_LOG(("end..."));
}

IM::~IM() {
  DEBUG_LOG(("IM::~IM..."));

  disconnect(this, &IM::selfNicknameChanged, this, &IM::onSelfNicknameChanged);
  disconnect(this, &IM::groupListReceived, this, &IM::onRoomReceived);

}

void IM::timesUp() {
  // TODO 定时任务暂时关闭

  //  for (const auto &item : m_roomMap) {
  //    auto room = item.second;
  //    room->getRoomInfo();
  //  }
}

Client *IM::makeClient(const QString &username, const QString &token) {
  DEBUG_LOG(("username:%1 token:%2").arg(username).arg(token));
  QMutexLocker ml{&m_clientMutex};

  _username = username;
  _password = token;

  loginJid.setUsername(username.toStdString());
  loginJid.setResource(APPLICATION_NAME);

  DEBUG_LOG(("Login XMPP=>%1").arg(qstring(loginJid.full())));

  /**
   * Client
   */
  auto client = std::make_unique<Client>(loginJid, stdstring(token));
  //  _client->setSasl(false);
  client->setCompression(false);

  /**
   * listeners
   */
  client->registerConnectionListener(this);

  /**
   * handlers
   */
  client->registerPresenceHandler(this);
  client->registerMessageHandler(this);
  client->registerMessageSessionHandler(this);

  /**
   * extensions
   */
  //  client->registerStanzaExtension(new gloox::Nickname(nullptr));
  client->registerStanzaExtension(new Avatar);
  client->registerStanzaExtension(new VCardUpdate);
  client->registerStanzaExtension(new Capabilities);
  client->registerStanzaExtension(new Disco::Items);
  client->registerStanzaExtension(new PubSub::Event());
  client->registerStanzaExtension(new Jingle::JingleMessage());
  client->registerStanzaExtension(new InBandBytestream::IBB);
  client->registerStanzaExtension(new ChatState(nullptr));
  client->registerStanzaExtension(new Receipt(nullptr));

  //  client->registerTagHandler(this, "query", XMLNS_DISCO_ITEMS);
  client->registerIqHandler(this, ExtIBB);

  auto disco = client->disco();
  disco->setVersion("disco", GLOOX_VERSION, "linux");
  disco->setIdentity("client", APPLICATION_ID);
  disco->setIdentity("pubsub", "pep");

  disco->addFeature(XMLNS_CHAT_STATES);
  disco->addFeature(XMLNS_MUC);
  disco->addFeature(XMLNS_MUC_ADMIN);
  disco->addFeature(XMLNS_MUC_OWNER);
  disco->addFeature(XMLNS_MUC_ROOMS);
  disco->addFeature(XMLNS_MUC_ROOMINFO);
  disco->addFeature(XMLNS_MUC_USER);
  disco->addFeature(XMLNS_MUC_UNIQUE);
  disco->addFeature(XMLNS_MUC_REQUEST);

  disco->addFeature(XMLNS_DISCO_INFO);
  disco->addFeature(XMLNS_DISCO_ITEMS);
  disco->addFeature(XMLNS_DISCO_PUBLISH);

  disco->addFeature(XMLNS_CAPS);

  disco->addFeature(XMLNS_PUBSUB);
  disco->addFeature(XMLNS_PUBSUB_EVENT);
  disco->addFeature(XMLNS_PUBSUB_OWNER);
  disco->addFeature(XMLNS_PUBSUB_PUBLISH_OPTIONS);
  disco->addFeature(XMLNS_PUBSUB_AUTO_SUBSCRIBE);
  disco->addFeature(XMLNS_PUBSUB_AUTO_CREATE);

  disco->addFeature(XMLNS_NICKNAME);
  // Namespace.NICK + "+notify",
  disco->addFeature(XMLNS_NICKNAME + "+notify");
  // urn:xmpp:avatar:data
  disco->addFeature(XMLNS_AVATAR);
  // urn:xmpp:avatar:metadata
  disco->addFeature(XMLNS_META_AVATAR);
  disco->addFeature(XMLNS_META_AVATAR + "+notify");
  disco->addFeature(XMLNS_BOOKMARKS);
  disco->addFeature(XMLNS_PRIVATE_XML);
  // XMLNS_RECEIPTS
  disco->addFeature(XMLNS_RECEIPTS);

  // 基本Jingle功能
  disco->addFeature(XMLNS_IBB);
  disco->addFeature(XMLNS_JINGLE);
  disco->addFeature(XMLNS_JINGLE_FILE_TRANSFER);
  disco->addFeature(XMLNS_JINGLE_FILE_TRANSFER4);
  disco->addFeature(XMLNS_JINGLE_FILE_TRANSFER5);
  disco->addFeature(XMLNS_JINGLE_FILE_TRANSFER_MULTI);
  disco->addFeature(XMLNS_JINGLE_IBB);
  disco->addFeature(XMLNS_JINGLE_ERRORS);
  disco->addFeature(XMLNS_JINGLE_ICE_UDP);
  disco->addFeature(XMLNS_JINGLE_APPS_DTLS);
  disco->addFeature(XMLNS_JINGLE_APPS_RTP);
  // audio/video
  disco->addFeature(XMLNS_JINGLE_FEATURE_AUDIO);
  disco->addFeature(XMLNS_JINGLE_FEATURE_VIDEO);
  disco->addFeature(XMLNS_JINGLE_APPS_RTP_SSMA);
  disco->addFeature(XMLNS_JINGLE_APPS_RTP_FB);
  disco->addFeature(XMLNS_JINGLE_APPS_RTP_SSMA);
  disco->addFeature(XMLNS_JINGLE_APPS_RTP_HDREXT);
  disco->addFeature(XMLNS_JINGLE_APPS_GROUP);
  disco->addFeature(XMLNS_JINGLE_MESSAGE);

  // registerDiscoHandler
  disco->registerNodeHandler(this, EmptyString);
  disco->registerDiscoHandler(this);

  // rosterManager
  client->rosterManager()->registerRosterListener(this);

  vCardManager = std::make_unique<VCardManager>(client.get());
  pubSubManager = std::make_unique<PubSub::Manager>(client.get());

  /**
   * bookmark
   */
  bookmarkStorage = std::make_unique<gloox::BookmarkStorage>(client.get());
  bookmarkStorage->registerBookmarkHandler(this);

  /**
   * Registration
   */
  mRegistration = std::make_unique<Registration>(client.get());
  mRegistration->registerRegistrationHandler(this);

#ifdef LOG_XMPP
  client->logInstance().registerLogHandler(LogLevelDebug, LogAreaAll, this);
#endif
  _connectThread = std::make_unique<ConnectThread>(client.get());
  _client = std::move(client);
  return _client.get();
}

void IM::start(session::AuthSession *session) {
  DEBUG_LOG(("IM=>%1").arg(session->getUsername()));

  if (_status == CONNECT_STATUS::CONNECTED) {
    return;
  }

  _session = session;
  makeClient(session->getUsername(), session->getPassword());
  doConnect();
}

void IM::stop() {
  if (_status != CONNECTED) {
    return;
  }
  doDisconnect();
  emit onStopped();
}

/**
 * Connect
 */
// gloox
void IM::onConnect() {
  DEBUG_LOG(("connected"));
  auto res = getClient()->resource();
  DEBUG_LOG(("resource:%1").arg(qstring(res)));
  //  pubSubManager->subscribe(JID(), XMLNS_NICKNAME, this);
  // 请求书签
  bookmarkStorage->requestBookmarks();
  _status = CONNECT_STATUS::CONNECTED;
  emit connectResult(_status);
  emit onStarted();
}

void IM::onDisconnect(ConnectionError e) {
  DEBUG_LOG(("error:%1").arg(e));
  if (e == ConnectionError::ConnStreamClosed) {
    DEBUG_LOG(("链接异常：流被关闭"))
  }
  _status = CONNECT_STATUS::DISCONNECTED;
  emit connectResult(_status);
}

bool IM::onTLSConnect(const CertInfo &info) {
  DEBUG_LOG(("CertInfo:\n"))

  time_t from(info.date_from);
  time_t to(info.date_to);

  DEBUG_LOG(("status: %1\n"   //
             "issuer: %2\n"   //
             "peer: %3\n"     //
             "protocol: %4\n" //
             "mac: %5\n"      //
             "cipher: %6\n"   //
             "compression: %7\n")

                .arg((info.status))                     //
                .arg(qstring(info.issuer.c_str()))      //
                .arg(qstring(info.server.c_str()))      //
                .arg(qstring(info.protocol.c_str()))    //
                .arg(qstring(info.mac.c_str()))         //
                .arg(qstring(info.cipher.c_str()))      //
                .arg(qstring(info.compression.c_str())) //
  )

  DEBUG_LOG(("from: %1").arg(ctime(&from)));
  DEBUG_LOG(("to:   %1").arg(ctime(&to)));

  return true;
}

void IM::handleLog(LogLevel level, LogArea area, const std::string &message) {
  //   DEBUG_LOG(("%1").arg(message.c_str()));
  switch (area) {
  case LogAreaXmlIncoming:
    DEBUG_LOG(("Received XML:\n%1").arg(message.c_str()));
    break;
  case LogAreaXmlOutgoing:
    DEBUG_LOG(("Sent XML:\n%1").arg(message.c_str()));
    break;
  case LogAreaClassConnectionBOSH:
    DEBUG_LOG(("BOSH:%1").arg(message.c_str()));
    break;
  case LogAreaClassClient:
    DEBUG_LOG(("Client: %1").arg(message.c_str()));
    break;
  case LogAreaClassDns:
    DEBUG_LOG(("dns: %1").arg(message.c_str()));
    break;
  default:
    DEBUG_LOG(("level: %1, area: %2 msg: %3")
                  .arg(level)
                  .arg(area)
                  .arg(message.c_str()));
  }
}

void IM::interrupt() {
  _status = CS_NONE;
  emit connectResult(_status);
  if (_connectThread) {
    _connectThread->terminate();
    _connectThread->wait();
  }
}

gloox::JID IM::wrapRoomJid(const QString &group) const {
  return gloox::JID(group.toStdString() + "@" + XMPP_CONF_SERVER_HOST);
}

gloox::JID IM::wrapJid(const QString &f) const {
  return gloox::JID(f.toStdString() + "@" + XMPP_SERVER_HOST);
}

void IM::setUserManager(UserManager *userManager) {
  _userManager = userManager;
}

IMMessage IM::from(const gloox::Message &msg) {
  IMMessage imMsg((IMMsgType)((int)msg.subtype()), //
                  qstring(msg.from().resource()),  //
                  qstring(msg.body()));
  if (!msg.id().empty()) {
    imMsg.id = (qstring(msg.id()));
  }
  return imMsg;
}

QString IM::jid2Name(const gloox::JID &jid) { return qstring(jid.username()); }

void IM::doAvailablePresence(const Presence &presence,  //
                             backend::UserJID &userJID) //
{

  DEBUG_LOG(("UserJID：%1 presence is Available").arg((userJID.id)));
  // 添加用户
  //  _userManager->addUserUin(userJID);

  // 获取设备信息
  PeerStatus status;
  const Devices *devices = presence.devices();
  if (devices) {
    DEBUG_LOG(("Devices audio:%1 video:%2")
                  .arg(devices->audio())
                  .arg(devices->video()));
  }

  const AudioMuted *audioMuted = presence.audioMuted();
  if (audioMuted) {
    DEBUG_LOG(("AudioMuted:%1").arg(audioMuted->muted()));
    status.audioMuted = audioMuted->muted();
  }

  const VideoMuted *videoMuted = presence.videoMuted();
  if (videoMuted) {
    DEBUG_LOG(("VideoMuted:%1").arg(videoMuted->muted()));
    status.videoMuted = videoMuted->muted();
  }

  // const VideoType *videoType = presence.videoType();
  // if (videoType) {
  // DEBUG_LOG(("VideoType:%1").arg(qstring(videoType->type())));
  // status.videoType = videoType->type();
  // }

  // const RaisedHand *raisedHand = presence.raisedHand();
  // if (raisedHand) {
  //   DEBUG_LOG(("RaisedHand:%1").arg(raisedHand->raised()));
  //   status.raisedHand = raisedHand->raised();
  // }
  //  _userManager->setPeerStatus(userJID, status);

  // Capabilities
  //            const Capabilities *caps = presence.capabilities();
  //            if(caps){
  //                Disco *disco = client->client()->disco();
  //                std::string node = caps->node() +"#"+ caps->ver();
  //                disco->getDiscoInfo(*participantJID, node, this, 0);
  //            }

  //  _presence_mutex.lock();
  //  _mucPresences++;
  //  _presence_mutex.unlock();

  //  if (base::str_equals(client->jid().full(),
  //  userJID.id.toStdString(), false)) {
  //    onJoinedRoom(JID(m_roomInfo->getJid().toStdString()), userJID);
  //  }
}

void IM::doUnAvailablePresence(const Presence &presence, UserJID &userJID) {
  DEBUG_LOG(("UserJID：%1 presence is OFF").arg((userJID.id)));
  Q_UNUSED(presence);
  //  _userManager->removeUserUin(userJID);
}

void IM::doConnect() {
  DEBUG_LOG(("..."));
  _connectThread->start();
  DEBUG_LOG(("started=>%1").arg(_connectThread->objectName()));
}

void IM::doDisconnect() {
  assert(_client);
  _client->disconnect();
}

void IM::onLogin() {}

void IM::onLogout() { doDisconnect(); }

// room info
void IM::joinRoom(const JID &jid) {
  DEBUG_LOG(("jid:%1").arg(qstring(jid.full())));

  auto room = findRoom(jid.username());
  if (room) {
    return;
  }

  JID roomJid(jid);
  roomJid.setResource(self().username());
  room = new MUCRoom(_client.get(), roomJid, this, this);
  room->join();

  emit groupListReceived( jid );
}

void IM::setRole(const UserJID &userJid, const MUCRoomRole &role) const {
  DEBUG_LOG(("begin jid:%1").arg((userJid.id)));
  // m_pRoom->setRole(userJid.nickname, role);
  DEBUG_LOG(("end"));
}

void IM::setAffiliation(const UserJID &userJid, MUCRoomAffiliation aff) {
  DEBUG_LOG(("begin jid:%1").arg((userJid.id)));
  m_pRoom->setAffiliation(userJid.id.toStdString(), aff,
                          "affiliation is coming");
  DEBUG_LOG(("end"));
}


bool IM::sendTo(const gloox::JID &to, const QString &msg, QString &id) {
  QMutexLocker ml{&m_clientMutex};
  auto msgId = _client->getID();

  DEBUG_LOG(("msg:%1=>%2").arg(qstring(to.bare())).arg(msg));
  gloox::Message m(gloox::Message::MessageType::Chat, to, msg.toStdString());
  m.setFrom(_client->jid());
  m.setID(msgId);
  m.addExtension(new Receipt(Receipt::Request));

  id.append(qstring(msgId));

  _client->send(m);
  return true;
}

// Handle Message session
void IM::handleMessageSession(MessageSession *session) {
  DEBUG_LOG(("开启新的消息对话：threadId:%1 target:%2")
                .arg(qstring(session->threadID()))
                .arg(qstring(session->target().full())));

  // 放入最新的session

  // m_messageEventFilter = std::make_unique<MessageEventFilter>(session);
  // m_messageEventFilter->registerMessageEventHandler(this);
  // m_pepEventFilter =
  // std::make_unique<PersonalEventingProtocolFilter>(session);

  // 注册类别
  //  m_pepEventFilter->registerPlugin(new SmartBoard::DrawLine());
  //  m_pepEventFilter->registerPlugin(new SmartBoard::DrawText());
  //  m_pepEventFilter->registerPlugin(new SmartBoard::DrawFile());
  //  m_pepEventFilter->registerPlugin(new SmartBoard::DrawMove());
  //  m_pepEventFilter->registerPlugin(new SmartBoard::DrawRemove());

  // m_pepEventFilter->registerPlugin(new SmartBoard::ControllerSelect());
  // m_pepEventFilter->registerPlugin(new SmartBoard::ControllerVoice());

  //    m_pepEventFilter->registerPersonalEventingProtocolHandler(this);
  //  auto jid = session->target();
  //  sessionMap.emplace(std::pair(jid.bare(), session));
  session->registerMessageHandler(this);

  // 聊天状态过滤器，获取：正在中等输入状态
  if (m_chatStateFilters.size() > 1000) {
    return;
  }
  auto csf = new ChatStateFilter(session);
  csf->registerChatStateHandler(this);
  m_chatStateFilters.emplace(session->target().username(), csf);
  // TODO delete csf
}

void IM::handleMessage(const gloox::Message &msg, MessageSession *session) {
  if (!session) {
    DEBUG_LOG(("session is NULL"))
    return;
  }
  currentJID = msg.from();
  auto threadId = qstring(session->threadID());
  auto peerId = qstring(currentJID.full());
  auto friendId = qstring(currentJID.username());
  auto body = qstring(msg.body());

  DEBUG_LOG(("from:%1 subtype:%2 threadId:%3 body:%4")
                .arg(peerId)
                .arg((int)msg.subtype())
                .arg(threadId)
                .arg(body));

  sessionIdMap.emplace(friendId.toStdString(), threadId.toStdString());
  sessionMap.emplace(threadId.toStdString(), session);

  gloox::Message::MessageType msgType = msg.subtype();
  switch (msgType) {
  case gloox::Message::Error: {
    break;
  }
  case gloox::Message::Chat: {
    // 来自朋友的消息
    if (!body.isEmpty()) {
      IMMessage imMsg = IM::from(msg);
      emit receiveFriendMessage(friendId, imMsg);
    }

    // 从 message.receipt 提取接收确认ID
    auto se = msg.findExtension<Receipt>(ExtReceipt);
    if (se) {
      emit receiveMessageReceipt(friendId, qstring(se->id()));
    }
  }
  case gloox::Message::Headline: {
    auto pse = msg.findExtension<PubSub::Event>(ExtPubSubEvent);
    if (pse) {
      for (auto &item : pse->items()) {
        auto nickTag = item->payload->findChild("nick");
        if (nickTag) {
          gloox::Nickname nickname(nickTag);
          if (getSelfId() == friendId) {
            emit selfNicknameChanged((nickname.nick()));
          } else {
            emit receiveNicknameChange(friendId, qstring(nickname.nick()));
          }
          return;
        }

        auto photoTag =
            item->payload->findChild("metadata", "xmlns", XMLNS_META_AVATAR);
        if (photoTag) {
          // TODO 待优化
          std::string itemId = photoTag->findChild("info")->findAttribute("id");

          ItemList items;
          Item *item0 = new Item();
          item0->setID(itemId);
          items.emplace_back(item0);

          pubSubManager->requestItems(msg.from(), XMLNS_AVATAR, "", items,
                                      this);
        }
      }
      return;
    }

    auto jm = msg.findExtension<Jingle::JingleMessage>(ExtJingleMessage);
    if (jm) {
      doJingleMessage(PeerId(currentJID), jm);
    }

    auto mu = msg.findExtension<MUCRoom::MUCUser>(ExtMUCUser);
    if (mu) {
      // 群聊邀请
      if (MUCRoom::OpInviteFrom == mu->operation()) {
        emit groupInvite(qstring(JID(*mu->jid()).username()), friendId, body);
      }
    }
    break;
  }
  }
}

void IM::handleMessageEvent(const JID &from, MessageEventType et) {
  DEBUG_LOG(qsl("handleMessageEvent: JID:%1 MessageEventType:%2")
                .arg(qstring(from.full()))
                .arg((int)et));
}

/**
 * 接收来自朋友聊天状态
 * @param from
 * @param state
 */
void IM::handleChatState(const JID &from, ChatStateType state) {
  DEBUG_LOG(("from:%1 state:%2")
                .arg(qstring(from.full()))
                .arg(static_cast<int>(state)));
  /*
   * 1.参考方法：handleMessage
   * IMMessage imMsg = IM::from(msg);
   * 2.发送来自朋友聊天状态的信号(声明信号)
   * emit receiveFriendChatState(friendId, state);
   * 3.接收信息的槽
   * connect(
  _im, &lib::IM::IM::receiveFriendChatState,
  [&](QString friendId, int state) -> void {
  for (auto handler : friendHandlers) {
    handler->onFriendChatState(friendId, state);
  }
});
   * */
  auto friendId = qstring(from.username());
  emit receiveFriendChatState(friendId, state);
}

/**
 * 发送给朋友当前聊天状态
 * @param to
 * @param state 聊天状态
 */
void IM::sendChatState(const gloox::JID &to, ChatStateType state) {
  DEBUG_LOG(("to:%1 state:%2")           //
                .arg(qstring(to.full())) //
                .arg(static_cast<int>(state)));
  auto csf = m_chatStateFilters[to.username()];
  if (!csf) {
    return;
  }
  csf->setChatState(state);
}

void IM::handlePersonalEventingProtocol(
    const JID &from, const std::string &node, const PubSub::ItemList &items,
    const PersonalEventingProtocolFilter *filter) {
  DEBUG_LOG(("from:%1 node:%2").arg(qstring(from.full())).arg(qstring(node)));
#if 0
  if (node == network::smartboard::PUBSUB_NODE) {

    for (auto item : items) {
      if (item->payload()) {
        DEBUG_LOG(("item=%1").arg(qstring(item->payload()->name())));

        // SmartBoard::SmartBoardDraw *draw = new
        // SmartBoard::SmartBoardDraw(item->payload(), filter); if (draw)
        // {
        //     _smartBoard->receiveDraw(draw);
        // }

        // SmartBoard::Controller *controller = new
        // SmartBoard::Controller(item->payload(), filter); if (controller)
        // {
        //     _smartBoard->receiveController(controller);
        // }
      }
    }
  }
#endif
}


/**
 * 初始化房间并加入
 */
void IM::createRoom(const JID &jid, const std::string &password) {

  DEBUG_LOG(("jid:%1 password:%2") //
                .arg(qstring(jid.full()))
                .arg(qstring(password)));

  // TODO 判断是否重名
  JID &roomJid = const_cast<JID &>(jid);
  roomJid.setResource(self().username());
  DEBUG_LOG(("room:%1").arg(qstring(roomJid.full())));

  m_pRoom = std::make_unique<MUCRoom>(_client.get(), roomJid, this, this);
  if (!password.empty()) {
    // 密码
    m_pRoom->setPassword(password);
  }
  m_pRoom->setName(jid.username());

  m_pRoom->setSubject("--");
  //  {"focus@auth.meet.chuanshaninfo.com":"owner",
  //   "18910221510@meet.chuanshaninfo.com":"owner",
  //   "18510248810@meet.chuanshaninfo.com":"owner"}

  m_pRoom->join();
}

/**
 * 要求成员加入房间
 * @param roomJid
 * @param peerId
 */
bool IM::inviteToRoom(const JID &roomJid, const JID &peerId) {
  auto room = findRoom(roomJid.username());
  if (!room) {
    return false;
  }

  room->invite(peerId, "--");
  return true;
}

/**
 * 加入房间事件
 * @brief IM::onJoinedRoom
 * @param roomJid
 * @param userJID
 */
#ifdef false
void IM::onJoinedRoom(const JID &roomJid, const UserJID &userJID) {

  DEBUG_LOG(("roomJid:%1 userJid:%2")         //
                .arg(qstring(roomJid.full())) //
                .arg((userJID.id))            //
  );

  std::lock_guard<std::mutex> lock(_join_room_mutex);

  //  if (!_joined_room) {
  // 加入会议室，改变成会员角色
  //         if(_userManager->isMember()){
  //             setAffiliation(userJID,
  //             MUCRoomAffiliation::AffiliationMember); setRole(userJID,
  //             MUCRoomRole::RoleVisitor);
  //         }else{
  //             setAffiliation(userJID,
  //             MUCRoomAffiliation::AffiliationOwner); setRole(userJID,
  //             MUCRoomRole::RoleModerator);
  //         }

  // Jingle
  // _jingle->join(roomJid);
  // SmartBoard
  // DEBUG_LOG(("end for:%1").arg(qstring(roomJid.full())));

  //    _joined_room = true;
  //  }
}
#endif

// MUC handler -- handleMUCParticipantPresence
void IM::handleMUCParticipantPresence(gloox::MUCRoom *room,                 //
                                      const MUCRoomParticipant participant, //
                                      const Presence &presence) {

  DEBUG_LOG(("Room:%1").arg(qstring(room->name())));
  JID *nick = participant.nick;
  DEBUG_LOG(("participant:%1 presence=>%2") //
                .arg(qstring(nick->resource()))
                .arg(presence.presence()));

#if 0
//TODO 此处导致界面卡死，暂时注释
  switch (presence.presence()) {
  case Presence::Unavailable:
  case Presence::Invalid:
  case Presence::Error: {
    // 非在线
    emit groupOccupantStatus(roomName, peerId, false);
    break;
  }
  default: {
    // 在线
    emit groupOccupantStatus(roomName, peerId, true);
    break;
  }
  }
#endif
}

void IM::handleMUCMessage(MUCRoom *room, const gloox::Message &msg, bool priv) {
  QString fromUsername = qstring(msg.from().username());
  QString body = qstring(msg.body());
  auto msgId = qstring(msg.id());
  PeerId peerId(msg.from());

  DEBUG_LOG(("room:%1 from:%2 msgId: %3 msgContent:%4")
                .arg(qstring(room->name())) // room
                .arg(peerId.toString())     // from
                .arg(msgId)                 // msg id
                .arg(body));                // msg body

  for (const auto &id : sendIds) {
    if (id == msg.id()) {
      qDebug()<<"自己发出消息，忽略！";
      sendIds.remove(id);
      return;
    }
  }

  auto mu = msg.findExtension<MUCRoom::MUCUser>(ExtMUCUser);
  if (mu) {
    if (mu->flags() & UserRoomConfigurationChanged) {
      room->getRoomInfo();
    }

    /**
     * <message from='test8@conference.meet.chuanshaninfo.com'
     * to='18510248810@meet.chuanshaninfo.com'>
     * <x xmlns='http://jabber.org/protocol/muc#user'>
     *    <invite from='test8@conference.meet.chuanshaninfo.com/高杰2395'>
     *        <reason/>
     *    </invite>
     * </x>
     * <x jid='test8@conference.meet.chuanshaninfo.com'
     * xmlns='jabber:x:conference'/>
     * <body>test8@conference.meet.chuanshaninfo.com/高杰2395
     * invited you to the room test8@conference.meet.chuanshaninfo.com
     * </body>
     * </message>
     */
    if (MUCRoom::OpInviteFrom == mu->operation()) {
      emit groupInvite(qstring(JID(*mu->jid()).username()), fromUsername,
                       qstring(msg.body()));
    }

    return;
  }

  if (body.isEmpty()) {
    return;
  }

  IMMessage imMsg = from(msg);
  const DelayedDelivery *dd = msg.when();
  if (dd) {
    // yyyy-MM-dd HH:mm:ss 20230614T12:11:43Z
    imMsg.time =
        QDateTime::fromString(qstring(dd->stamp()), "yyyy-MM-ddTHH:mm:ssZ");
  }
  emit receiveRoomMessage(fromUsername, peerId, imMsg);
}

bool IM::handleMUCRoomCreation(MUCRoom *room) {
  DEBUG_LOG(("room %1 didn't exist, been created.\n").arg(room->name().c_str()));

  JID roomJid(room->name() + "@" + XMPP_CONF_SERVER_HOST);
  room->requestRoomConfig();


  ConferenceListItem item;
  item.name = room->name();
  item.nick = _client->jid().username();
  item.jid = room->name()+"@"+room->service();
  item.autojoin = true;

  //添加到书签列表
  mConferenceList.emplace_back(item);
  //存储书签列表
  bookmarkStorage->storeBookmarks({}, mConferenceList);

  // 发射群聊增加信号
  emit groupListReceived(JID(room->name()+"@"+room->service()));

  return true;
}

void IM::handleMUCSubject(MUCRoom *room,  //
                          const std::string &nick,  //
                          const std::string &subject) {
  DEBUG_LOG(("MUCRoom:%1").arg(qstring(room->name())));
  DEBUG_LOG(("Nick:%1 subject:%2").arg(qstring(nick)).arg(qstring(subject)));
}

void IM::handleMUCInviteDecline(MUCRoom *room, const JID &invitee,
                                const std::string &reason) {
  DEBUG_LOG(("MUCRoom:%1").arg(qstring(room->name())));
  DEBUG_LOG(("invitee:%1 reason:%2")
                .arg(qstring(invitee.full()))
                .arg(qstring(reason)));
}

void IM::handleMUCError(MUCRoom *room, StanzaError error) {
  DEBUG_LOG(("MUCRoom:%1 error:%2")
                .arg(qstring(room->name()))
                .arg(static_cast<int>(error)));
}

void IM::handleMUCInfo(MUCRoom *room,           //
                       int features,            //
                       const std::string &name, //
                       const DataForm *infoForm) {

  DEBUG_LOG(("MUCRoom name:%1").arg(qstring(name)));
  DEBUG_LOG(("MUCRoom nick:%1").arg(qstring(room->nick())));       //
  DEBUG_LOG(("MUCRoom service:%1").arg(qstring(room->service()))); //
  DEBUG_LOG(("features:%1").arg(features));
  if (infoForm) {
    for (auto field : infoForm->fields()) {
      //    DEBUG_LOG(("field name:%1 value:%2")        //
      //                  .arg(qstring(field->name()))  //
      //                  .arg(qstring(field->value())) //
      //              )                                 //
      if (field->name() == "muc#roominfo_occupants") {
        auto occupants = field->value();
        emit groupOccupants(qstring(room->name()), std::stoi(occupants));
      }

      if (field->name() == "muc#roomconfig_roomname" &&
          !field->value().empty()) {
        emit groupRoomName(qstring(room->name()), field->value());
      }
    }
  }



  //  ItemList items;
  //
  //  Item *item1 = new Item();
  //  item1->setID("current");
  //
  //  Tag *s = new Tag("storage", XMLNS, XMLNS_BOOKMARKS);
  //  for (auto &ci : cl) {
  //    Tag *c = new Tag("conference");
  //    c->addAttribute("name", ci.name);
  //
  //    Tag *nick = new Tag("nick");
  //    nick->setCData(ci.nick);
  //
  //    c->addChild(nick);
  //    c->addAttribute("autojoin", ci.autojoin ? "true" : "false");
  //    c->addAttribute("jid", ci.jid);
  //    if (!ci.password.empty()) {
  //      c->addAttribute("password", ci.password);
  //    }
  //    s->addChild(c);
  //  }
  //
  //  item1->setPayload(s);
  //
  //  items.emplace_back(item1);
  //
  //  auto result = pubSubManager->publishItem(JID(), XMLNS_BOOKMARKS, items,
  //  nullptr, this); DEBUG_LOG(("Result=>%1").arg(qstring(result)));
}

void IM::handleMUCItems(MUCRoom *room, const Disco::ItemList &items) {
  DEBUG_LOG(("MUCRoom:%1").arg(qstring(room->name())));
  DEBUG_LOG(("items:%1").arg(items.size()));
}

// config
void IM::handleMUCConfigList(MUCRoom *room, const MUCListItemList &items,
                             MUCOperation operation) {
  DEBUG_LOG(("IM::handleMUCConfigList"));
}

void IM::handleMUCConfigForm(MUCRoom *room, const DataForm &form) {
  DEBUG_LOG(("room:%1").arg(qstring(room->name())));
  for (auto item : form.fields()) {
    if (item->name() == "muc#roomconfig_roomname" && !roomName.empty()) {
      // 设置房间名称
      item->setValue(roomName);
      // 设置名称需要清空
      roomName = "";
    } else if (item->name() == "muc#roomconfig_persistentroom") {
      // 设置持久房间
      item->setValue("1");
    }
  }
  auto *mform = new DataForm(form);
  mform->setType(FormType::TypeSubmit);
  room->setRoomConfig(mform);
};

void IM::handleMUCConfigResult(MUCRoom *room, bool success,
                               MUCOperation operation) {
  DEBUG_LOG(("room:%1 success:%2").arg(qstring(room->name())).arg(success));
};

void IM::handleMUCRequest(MUCRoom *room, const DataForm &form) {
  DEBUG_LOG(("IM::handleMUCRequest"));
};

MUCRoom *IM::findRoom(const std::string &username) const {
  for (const auto &item : m_roomMap) {
    if (item.first.username() == username) {
      return item.second;
    }
  }
  return nullptr;
}

bool IM::sendToRoom(const QString &to, const QString &msg, const QString &id) {

  //  QMutexLocker ml{&m_clientMutex};

  DEBUG_LOG(("sendToRoom:%1=>%2").arg(msg).arg(to));

  MUCRoom *room = findRoom(stdstring(to));
  if (!room){
    DEBUG_LOG(("房间不存在:%1!").arg((to)));
    return false;
  }

  std::string nid = !id.isEmpty() ? id.toStdString() : getClient()->getID();
  sendIds.emplace_back(nid);

  room->send(msg.toStdString(), nid);
  return true;
}

void IM::setRoomSubject(const std::string &groupId, const std::string &nick) {
  QMutexLocker ml{&m_clientMutex};
  auto it = m_roomMap.find(groupId);
  if (it != m_roomMap.end()) {
    it->second->setSubject(nick);
  }
}

void IM::setRoomName(const std::string &groupId, const std::string &roomName) {
  QMutexLocker ml{&m_clientMutex};
  auto it = m_roomMap.find(groupId);
  if (it != m_roomMap.end()) {
    this->roomName = roomName;
    it->second->requestRoomConfig();
  }
}

/**
 * 处理个人信息（VCard）
 * @param jid
 * @param vcard
 */
void IM::handleVCard(const JID &jid, const VCard *vcard) {
  DEBUG_LOG(("jid：%1").arg(qstring(jid.full())));

#ifndef Q_OS_WIN
  VCard::Photo photo = vcard->photo();
  if (!photo.binval.empty()) {
    DEBUG_LOG(("photo size:%1").arg(photo.binval.size()))
    std::string av = photo.binval;
    emit receiveFriendAvatarChanged(qstring(jid.username()),
                                    QByteArray::fromStdString(av));
  }
#endif

  auto &nickname = vcard->nickname();
  if (!nickname.empty()) {
    DEBUG_LOG(("nick:%1").arg(qstring(nickname)))
    emit receiveNicknameChange(qstring(jid.username()), qstring(nickname));
  }
}

void IM::handleVCardResult(VCardContext context, const JID &jid,
                           StanzaError se) {
  DEBUG_LOG(("context:%1 jid:%2 StanzaError:%3")
                .arg(context)
                .arg(qstring(jid.full()))
                .arg(se));
}

void IM::handleTag(Tag *tag) {
  DEBUG_LOG(("tag：%1").arg(qstring(tag->xml())));
}

bool IM::handleIq(const IQ &iq) {
  DEBUG_LOG(("iq：%1").arg(qstring(iq.tag()->name())));

  FriendId friendId(iq.from());

  //  auto items = iq.findExtension<Disco::Items>(ExtDiscoItems);
  //  if (items) {
  //    for (auto item : items->items()) {
  //      DEBUG_LOG(("jid：%1").arg(qstring(item->jid().full())));
  //      sendServiceDiscoveryInfo(item->jid());
  //    }
  //  }

  const InBandBytestream::IBB *ibb =
      iq.findExtension<InBandBytestream::IBB>(ExtIBB);
  if (ibb) {
    DEBUG_LOG(("ibb流:%1").arg(qstring(ibb->sid())));

    switch (ibb->type()) {
    case InBandBytestream::IBBOpen: {
      DEBUG_LOG(("Open"))
      break;
    }
    case InBandBytestream::IBBData: {
      DEBUG_LOG(("Data seq:%1").arg(ibb->seq()))
      emit receiveFileChunk(friendId, ibb->sid(), ibb->seq(), ibb->data());
      break;
    }
    case InBandBytestream::IBBClose: {
      DEBUG_LOG(("Close"))
      emit receiveFileFinished(friendId, ibb->sid());
      break;
    }
    default: {
    }
    }

    IQ riq(IQ::IqType::Result, iq.from(), iq.id());
    _client->send(riq);
  }
  return true;
}

void IM::handleIqID(const IQ &iq, int context) {}

void IM::handleBookmarks(const BookmarkList &bList,   //
                         const ConferenceList &cList) //
{

  //缓存群聊书签列表（新增加群聊加入该书签一起保存）
  mConferenceList = cList;
  mBookmarkList = bList;

  std::list<backend::UserId> list;
  for (auto &c : cList) {

    DEBUG_LOG(("room:%1").arg(qstring(c.name)));
    JID roomJid(c.jid);
    emit groupListReceived(roomJid);
  }

}

// Disco handler
void IM::handleDiscoInfo(const JID &from,         //
                         const Disco::Info &info, //
                         int context) {

  QString _from = QString::fromStdString(from.full());
  DEBUG_LOG(("from=%1 context=%2").arg(_from).arg(context));

  const StringList features = info.features();
  for (auto feature : features) {
    DEBUG_LOG(("feature=%1").arg(QString::fromStdString(feature)));
  }

  const Disco::IdentityList &identities = info.identities();
  for (auto identity : identities) {
    DEBUG_LOG(("identity=%1").arg(qstring(identity->name())));
  }
}

/**
 * 获取服务发现
 * @param from
 * @param items
 * @param context
 */
void IM::handleDiscoItems(const JID &from,           //
                          const Disco::Items &items, //
                          int context) {

  QString _from = QString::fromStdString(from.full());
  DEBUG_LOG(("from=%1 context=%2").arg(_from).arg(context));

  if (context == GROUP_LIST) {
    /**
     * 处理服务发现的群聊列表
     */
    //    const Disco::ItemList &localItems = items.items();
    //    for (auto item : localItems) {
    //      DEBUG_LOG(("item node:%1 name:%2")
    //                    .arg(qstring(item->node()))
    //                    .arg(qstring(item->name())));

    //      auto roomJid = item->jid();
    //
    //      roomJid.setResource(_client->jid().username());
    //      auto room = std::make_unique<MUCRoom>(_client.get(), roomJid, //
    //                                            this, this);
    //      room->join();
    //      room->getRoomInfo();
    //      DEBUG_LOG(("room:%1 nick:%2")
    //                    .arg(qstring(roomJid.full())) //
    //                    .arg(qstring(room->nick()))); //
    //
    //      m_roomMap.emplace(roomJid.username(), room.release());
    //      emit groupListReceived(qstring(roomJid.username()));
    //    }
  }
}

void IM::handleDiscoError(const JID &from,           //
                          const gloox::Error *error, //
                          int context) {

  QString _from = qstring(from.full());
  DEBUG_LOG(("from=%1 context=%2 error=%3")
                .arg(_from)
                .arg(context)
                .arg(qstring(error->text())));
}

// Presence Handler
void IM::handlePresence(const Presence &presence) {
  DEBUG_LOG(("presence from:%1 type:%2")
                .arg(qstring(presence.from().full()))
                .arg(presence.presence()));

  updateOnlineStatus(presence.from().username(),
                     presence.from().resource(),
                     presence.presence());

}

/**
 * Roster
 */
/**
 * 好友本地增加事件
 *
 * @param jid
 * @return
 */
void IM::handleItemAdded(const gloox::JID &jid) {
  DEBUG_LOG(("jid:%1").arg(qstring(jid.full())));
  UserId userId(jid.username(), jid.username());
  auto rosterManager = _client->rosterManager();

  /**
   * 订阅好友
   */
  if (m_addFriendMsg.isEmpty()) {
    rosterManager->subscribe(jid);
  } else {
    rosterManager->subscribe(jid, jid.username(), StringList(),
                             stdstring(m_addFriendMsg));
  }
}

/**
 * 好友被删除事件
 * @param jid
 */
void IM::handleItemRemoved(const JID &jid) {
  /**
   * subscription='remove'
   * TODO 需要通知到页面
   */
  DEBUG_LOG(("被对方删除:%1").arg(qstring(jid.full())));
  //  emit receiveFriendRemoved(qstring(jid.username()));
}

// 好友更新
void IM::handleItemUpdated(const JID &jid) {
  DEBUG_LOG(("jid:%1").arg(qstring(jid.full())));

  auto userId = qstring(jid.username());

  auto item = _client->rosterManager()->getRosterItem(jid);
  auto subType = item->subscription();
  DEBUG_LOG(("type:%1").arg(subType));

  switch (subType) {

  case gloox::S10nNone: {
    DEBUG_LOG(("none"))
    //    _client->rosterManager()->subscribe(jid);
    break;
  }
  case gloox::S10nNoneOut: {
    DEBUG_LOG(("已发送订阅请求，等待对方确认！"))
    break;
  }
  case gloox::S10nNoneIn: {
    DEBUG_LOG(("收到对方订阅请求，等待自己确认！"))
    break;
  }
  case S10nNoneOutIn: {
    DEBUG_LOG(("双方发起订阅，等待双方接受！"))
    break;
  }
  case gloox::S10nTo: {
    DEBUG_LOG(("已订阅对方，等待对方接受！"))
    // 加到联系人列表
    emit receiveFriend(FriendId(jid));
    break;
  }
  case gloox::S10nToIn: {
    DEBUG_LOG(("已订阅对方，对方接受订阅，等待自己确认！"))
    //    _client->rosterManager()->subscribe(jid);
    break;
  }
  case gloox::S10nFrom: {
    DEBUG_LOG(("对方已订阅自己！"))
    break;
  }
  case gloox::S10nFromOut: {
    DEBUG_LOG(("对方已订阅自己，已接受对方订阅，待自己确认！"))
  }
  case gloox::S10nBoth: {
    DEBUG_LOG(("互相订阅成功！"))
    //    emit receiveFriend(userId);
    break;
  }
  }
}

/**
 * 订阅好友
 * @param jid
 */
void IM::handleItemSubscribed(const JID &jid) {
  DEBUG_LOG(("jid:%1").arg(qstring(jid.full())));
}

/**
 * 取消订阅好友
 * @param jid
 */
void IM::handleItemUnsubscribed(const JID &jid) {
  DEBUG_LOG(("jid:%1").arg(qstring(jid.full())));
}

void IM::updateOnlineStatus(const std::string &bare,
                            const std::string &resource,
                            Presence::PresenceType presenceType) {

  DEBUG_LOG(("user:%1 resource:%2 presenceType:%3")
                .arg(qstring(bare))
                .arg(qstring(resource))
                .arg(presenceType));

  if (resource.empty()) {
    return;
  }
  auto it = onlineMap.find(bare);
  if (it == onlineMap.end()) { //第一次
    if (presenceType != gloox::Presence::Unavailable) {
      std::set<std::string> resources;
      resources.insert(resource);
      onlineMap.emplace(bare, resources);
      emit receiveFriendStatus(qstring(bare), gloox::Presence::Available);
    }
    return;
  } else {  //第二次+
    std::set<std::string> &resources = it->second;
    if (presenceType != gloox::Presence::Unavailable) {
      // multi online endpoint
      resources.insert(resource);
      onlineMap.emplace(bare, resources);
      emit receiveFriendStatus(qstring(bare), gloox::Presence::Available);
    } else {
      // one offline
      resources.erase(resource);
      if (resources.empty()) {
        // all offline
        onlineMap.erase(bare);
        emit receiveFriendStatus(qstring(bare), gloox::Presence::Unavailable);
      }
    }
  }
}

bool IM::removeFriend(JID jid) {
  _client->rosterManager()->remove(jid);
  return true;
}

void IM::addRosterItem(const gloox::JID &jid, const QString &msg) {
  DEBUG_LOG(("jid:%1").arg(qstring(jid.full())));
  m_addFriendMsg = msg;
  StringList group;
  _client->rosterManager()->add(jid, jid.username(), group);
}

void IM::acceptFriendRequest(const QString &friendId) {
  DEBUG_LOG(("friend:%1").arg(friendId))
  _client->rosterManager()->ackSubscriptionRequest(wrapJid(friendId), true);
}

void IM::rejectFriendRequest(const QString &friendId) {
  DEBUG_LOG(("friend:%1").arg(friendId))
  _client->rosterManager()->ackSubscriptionRequest(wrapJid(friendId), false);
}

size_t IM::getRosterCount() {
  return _client->rosterManager()->roster()->size();
}

void IM::getRosterList(std::list<FriendId> &list) {
  auto rosterManager = _client->rosterManager();
  gloox::Roster *rosterMap = rosterManager->roster();
  for (const auto &itr : *rosterMap) {
    auto jid = itr.second;
    FriendId peerId(jid->jidJID());
    list.push_back(peerId);
  }
}

void IM::handleRoster(const Roster &roster) {
  QMutexLocker ml{&m_clientMutex};
  DEBUG_LOG(("size:%1").arg(roster.size()));

  for (auto &it : roster) {
    auto &key = it.first;
    auto &jid = it.second->jidJID();

    DEBUG_LOG(("roster:%1 jid:%2 subscription:%3")
                  .arg(qstring(key))            //
                  .arg(qstring(jid.username())) //
                  .arg(it.second->subscription()));

    _client->rosterManager()->subscribe(jid);
    emit receiveFriend(FriendId(jid));
  }

  for (auto &it : roster) {
    // 获取联系人个人信息
    vCardManager->fetchVCard(it.second->jidJID(), this);
  }
  // 获取自己个人信息
  vCardManager->fetchVCard(_client->jid(), this);
};

/**
 * 接收联系人在线状态
 * @param item 联系人
 * @param resource 终端
 * @param presenceType
 * @param msg
 * @return
 */
void IM::handleRosterPresence(const RosterItem &item,              //
                              const std::string &resource,         //
                              Presence::PresenceType presenceType, //
                              const std::string &msg) {

  DEBUG_LOG(("item:%1 presenceType:%2 msg:%3")
                .arg(qstring(item.jidJID().username()))
                .arg(presenceType)
                .arg(qstring(msg)));
  // 获取订阅状态
  gloox::SubscriptionType subType = item.subscription();
  DEBUG_LOG(("对方订阅状态:%1").arg(subType));

  //  if (subType == gloox::S10nBoth) {
  //    DEBUG_LOG(("对方同意好友 %1").arg(qstring(item.jidJID().username())));
  //    return;
  //  }
  //
  //  if (subType == gloox::S10nNone) {
  //    DEBUG_LOG(("对方拒绝好友 %1").arg(qstring(item.jidJID().username())));
  //    return;
  //  }

  updateOnlineStatus(item.jidJID().username(), resource, presenceType);

  if (presenceType == gloox::Presence::Available) {
    for (auto &it : item.resources()) {
      auto sk = it.first;
      auto sr = it.second;
      for (auto &ext : sr->extensions()) {
        switch (ext->extensionType()) {
          //        case ExtCaps: {
          //          auto caps = const_cast<Capabilities *>(
          //              static_cast<const Capabilities *>(ext));
          //          DEBUG_LOG(("caps:%1").arg(qstring(caps->node())))
          //          break;
          //        }
        case ExtVCardUpdate: {
          // VCard个人信息更新
          /**
           * <pubsub xmlns='http://jabber.org/protocol/pubsub'>
          <items node='urn:xmpp:avatar:data'>
            <item id='111f4b3c50d7b0df729d299bc6f8e9ef9066971f'/>
          </items>
          </pubsub>
           */
          auto vCardUpdate =
              const_cast<VCardUpdate *>(static_cast<const VCardUpdate *>(ext));
          if (vCardUpdate && vCardUpdate->hasPhoto()) {
            //            auto avatar = new
            //            Avatar(std::list<std::string>{vCardUpdate->hash()});

            ItemList items;
            Item *item0 = new Item();
            item0->setID(vCardUpdate->hash());
            items.emplace_back(item0);

            pubSubManager->requestItems(item.jidJID(), XMLNS_AVATAR, "", items,
                                        this);
          }
          break;
        }
        }
      }
    }
  }
}

void IM::handleSelfPresence(const RosterItem &item,              //
                            const std::string &resource,         //
                            Presence::PresenceType presenceType, //
                            const std::string &msg) {

  DEBUG_LOG(("item:%1 resource:%2 presenceType:%3 msg:%4")
                .arg(qstring(item.jidJID().full()))
                .arg(qstring(resource))
                .arg(presenceType)
                .arg(qstring(msg)));

  selfPresType = presenceType;

  emit selfIdChanged(qstring(item.jidJID().username()));
  emit selfStatusChanged(
      _client->resource() == resource ? selfPresType : gloox::Presence::Available,
                         msg);

  if (presenceType == gloox::Presence::Available) {
    for (auto &it : item.resources()) {
      auto sk = it.first;
      auto sr = it.second;
      for (auto &ext : sr->extensions()) {
        switch (ext->extensionType()) {
        case ExtVCardUpdate: {
          // VCard个人信息更新
          /**
           * <pubsub xmlns='http://jabber.org/protocol/pubsub'>
          <items node='urn:xmpp:avatar:data'>
            <item id='111f4b3c50d7b0df729d299bc6f8e9ef9066971f'/>
          </items>
          </pubsub>
           */
          auto vCardUpdate =
              const_cast<VCardUpdate *>(static_cast<const VCardUpdate *>(ext));
          if (vCardUpdate && vCardUpdate->hasPhoto()) {

            ItemList items;
            Item *item0 = new Item();
            item0->setID(vCardUpdate->hash());
            items.emplace_back(item0);

            pubSubManager->requestItems(item.jidJID().bareJID(), XMLNS_AVATAR,
                                        "", items, this);
          }
          break;
        }
        }
      }
    }
  }
};

/**
 * 好友订阅（加好友）请求
 * @param jid
 * @param msg
 * @return
 */
bool IM::handleSubscriptionRequest(const JID &jid, const std::string &msg) {
  DEBUG_LOG(("好友订阅（加好友）请求，来自:%1 消息:%2")
                .arg(qstring(jid.full()))
                .arg(qstring(msg)));
  emit receiveFriendRequest(FriendId(jid), qstring(msg));
  return true;
};

/**
 * 好友取消订阅（被删除）请求
 * @param jid
 * @param msg
 * @return
 */
bool IM::handleUnsubscriptionRequest(const JID &jid, const std::string &msg) {
  DEBUG_LOG(("jid:%1 msg:%2").arg(qstring(jid.full())).arg(qstring(msg)));
  return true;
};

void IM::handleNonrosterPresence(const Presence &presence) {
  DEBUG_LOG(("presence:%1").arg(qstring(presence.from().full())));
};

void IM::handleRosterError(const IQ &iq) {
  DEBUG_LOG(("text:%1").arg(qstring(iq.error()->text())));
};

void IM::handleRegistrationFields(const JID &from, int fields,
                                  std::string instructions){};

/**
 * This function is called if @ref Registration::createAccount() was called on
 * an authenticated stream and the server lets us know about this.
 */
void IM::handleAlreadyRegistered(const JID &from){};

/**
 * This funtion is called to notify about the result of an operation.
 * @param from The server or service the result came from.
 * @param regResult The result of the last operation.
 */
void IM::handleRegistrationResult(const JID &from,
                                  RegistrationResult regResult){};

/**
 * This function is called additionally to @ref handleRegistrationFields() if
 * the server supplied a data form together with legacy registration fields.
 * @param from The server or service the data form came from.
 * @param form The DataForm containing registration information.
 */
void IM::handleDataForm(const JID &from, const DataForm &form){};

/**
 * This function is called if the server does not offer in-band registration
 * but wants to refer the user to an external URL.
 * @param from The server or service the referal came from.
 * @param oob The OOB object describing the external URL.
 */
void IM::handleOOB(const JID &from, const OOB &oob){};

PeerId IM::getSelfId() {
  if (!_client) {
    return {};
  }

  auto status = _client->state();
  if (status != gloox::StateConnected) {
    return {};
  }

  PeerId peerId(_client->jid());
  return peerId;
}

QString IM::getSelfUsername() {
  if (!m_selfInfo.nickname.empty()) {
    return qstring(m_selfInfo.nickname);
  }
  if(_client){
    return qstring(_client->jid().username());
  }
  return {};
}

void IM::setNickname(const QString &nickname) {
  DEBUG_LOG(("nickname:%1").arg(nickname))

  gloox::Nickname nick(nickname.toStdString());

  ItemList items;
  Item *item = new Item();
  item->setID("current");
  item->setPayload(nick.tag());
  items.emplace_back(item);

  pubSubManager->publishItem(JID(), XMLNS_NICKNAME, items, nullptr, this);
}

void IM::setAvatar(const QByteArray &avatar) {
  if (avatar.isEmpty())
    return;

  QMutexLocker ml{&m_clientMutex};

  QCryptographicHash hash(QCryptographicHash::Algorithm::Sha1);
  hash.addData(avatar);
  QString sha1(hash.result().toHex());
  //  QString sha1("c4bf323ce996bd5423274187c2ead3a9606b97bd");

  DEBUG_LOG(("avatar size:%1 sha1:%2").arg(avatar.size()).arg(sha1))

  auto base64 = avatar.toBase64().toStdString();

  /**
   * <data xmlns='urn:xmpp:avatar:data'>
qANQR1DBwU4DX7jmYZnncm...
</data>
   */
  std::string payload;
  int pos = 0;
  do {
    if (base64.size() <= pos) {
      //      std::remove(payload.begin(), payload.end(), payload.size()-1);
      break;
    }
    std::string line = base64.substr(pos, 76);
    payload += line + "\n";
    pos += 76;
  } while (true);

  auto avt = new AvatarData(payload);

  ItemList items;
  Item *item = new Item();
  item->setID(sha1.toStdString());
  item->setPayload(avt->tag());
  items.emplace_back(item);

  pubSubManager->publishItem(JID(), XMLNS_AVATAR, items, nullptr, this);
  delete avt;

  selfAvatar.clear();
  selfAvatar.append(avatar);
}

void IM::changePassword(const QString &password) {
  DEBUG_LOG(("password:%1").arg(password));
  if (password.isEmpty())
    return;

  // changing password
  mRegistration->changePassword(_client->username(), stdstring(password));
}

void IM::getGroupList() {
  delayCaller_->call(2000, [this]() {
    auto disco = _client->disco();
    disco->getDiscoItems(JID(XMPP_CONF_SERVER_HOST), "", this, GROUP_LIST);
  });
}

Presence::PresenceType IM::getPresenceType() { return selfPresType; }

void IM::sendPresence() {
  QMutexLocker ml{&m_clientMutex};
  Presence pres(Presence::PresenceType::Available, JID());
  pres.addExtension(new Capabilities);
  _client->sendPresence(pres);
}

void IM::sendPresence(const JID &to) {
  QMutexLocker ml{&m_clientMutex};
  Presence pres(Presence::PresenceType::Available, to);
  pres.addExtension(new Capabilities);
  _client->sendPresence(pres);
}

void IM::sendReceiptReceived(const std::string &id, QString receiptNum) {
  QMutexLocker ml{&m_clientMutex};
  // <received xmlns='urn:xmpp:receipts' id='richard2-4.1.247'/>

  Message m(gloox::Message::MessageType::Chat, wrapJid(qstring(id)));
  m.setFrom(_client->jid());

  m.addExtension(
      new Receipt(Receipt::ReceiptType::Received, receiptNum.toStdString()));

  _client->send(m);
}

void IM::sendServiceDiscoveryItems() {
  QMutexLocker ml{&m_clientMutex};

  auto tag = new Tag("query");
  tag->setXmlns(XMLNS_DISCO_ITEMS);

  IQ *iq = new IQ(gloox::IQ::Get, loginJid.server(), _client->getID());
  iq->setFrom(self());

  auto iqt = iq->tag();
  iqt->addChild(tag);
  _client->send(iqt);
}

void IM::sendServiceDiscoveryInfo(const JID &item) {
  QMutexLocker ml{&m_clientMutex};

  auto tag = new Tag("query");
  tag->setXmlns(XMLNS_DISCO_INFO);

  IQ *iq = new IQ(gloox::IQ::Get, item, _client->getID());
  iq->setFrom(self());

  auto iqt = iq->tag();
  iqt->addChild(tag);
  _client->send(iqt);
}

void IM::handleItem(const JID &service, const std::string &node,
                    const Tag *entry) {
  DEBUG_LOG(
      ("service:%1 node:%2").arg(qstring(service.full())).arg(qstring(node)));
}

void IM::handleItems(const std::string &id, //
                     const JID &service,    //
                     const std::string &node, //
                     const gloox::PubSub::ItemList &itemList, //
                     const gloox::Error *error) {

  auto friendId = jid2Name(service);

  DEBUG_LOG(("id:%1 friendId:%2 service:%3 node:%4")
                .arg(qstring(id))
                .arg(friendId)
                .arg(qstring(service.full()))
                .arg(qstring(node)));

  for (auto &item : itemList) {

    auto data = item->payload();
    auto tagName = data->name();

    DEBUG_LOG(("PubSub::Item tagName:%1 node:%2")
                  .arg(qstring(tagName))
                  .arg(qstring(node)));

    if (node == XMLNS_NICKNAME) {
      gloox::Nickname nickname(data);
      if (friendId.isEmpty())
        emit selfNicknameChanged(nickname.nick());
      else {
        emit receiveNicknameChange(friendId, qstring(nickname.nick()));
      }
    }

    if (node == XMLNS_AVATAR) {
#ifndef Q_OS_WIN
      std::string binval = data->cdata();
      if (!binval.empty()) {
        std::string::size_type pos = 0;
        while ((pos = binval.find('\n')) != std::string::npos)
          binval.erase(pos, 1);
        while ((pos = binval.find('\r')) != std::string::npos)
          binval.erase(pos, 1);

        DEBUG_LOG(("photo size:%1").arg(binval.size()))

        std::string avt = Base64::decode64(binval);
        if (friendId.isEmpty()) {
          emit selfAvatarChanged(QByteArray::fromStdString(avt));
        } else {
          emit receiveFriendAvatarChanged(friendId,
                                          QByteArray::fromStdString(avt));
        }
      }
#endif
    }
  }
}

void IM::handleItemPublication(const std::string &id,    //
                               const JID &service,       //
                               const std::string &node,  //
                               const ItemList &itemList, //
                               const gloox::Error *error) {

  DEBUG_LOG(("node:%1").arg(qstring(node)));
  if (node == XMLNS_AVATAR) {
    // 更新头像元信息
    //  https://xmpp.org/extensions/xep-0084.html#process-pubmeta
    for (auto &item : itemList) {
      DEBUG_LOG(("itemId:%1").arg(qstring(item->id())));
      /**
       * AvatarMeta(long bytes,
    int height,
    int width,
    const std::string &type,
    const std::string &id);
       */
      if (selfAvatar.isEmpty())
        return;

      QPixmap pixmap;
      bool y = pixmap.loadFromData(selfAvatar);
      if (!y)
        return;
      auto f = pixmap.toImage();

      auto avt =
          new AvatarMeta(selfAvatar.size(), pixmap.height(), pixmap.width(),
                         "image/png", // TODO 暂时PNG
                         item->id());
      ItemList items;
      Item *item0 = new Item();
      item0->setID(item->id());
      item0->setPayload(avt->tag());
      items.emplace_back(item0);

      pubSubManager->publishItem(JID(), XMLNS_META_AVATAR, items, nullptr,
                                 this);

      selfAvatar.clear();
    }
  }
}

void IM::handleItemDeletion(const std::string &id, const JID &service,
                            const std::string &node, const ItemList &itemList,
                            const gloox::Error *error) {
  DEBUG_LOG(("id:%1 service:%2").arg(qstring(id)).arg(qstring(service.full())))
}

void IM::handleSubscriptionResult(const std::string &id, const JID &service,
                                  const std::string &node,
                                  const std::string &sid, const JID &jid,
                                  const gloox::PubSub::SubscriptionType subType,
                                  const gloox::Error *error) {

  DEBUG_LOG(("id:%1 jid:%2").arg(qstring(id)).arg(qstring(jid.full())))
}
void IM::handleUnsubscriptionResult(const std::string &id, const JID &service,
                                    const gloox::Error *error) {}
void IM::handleSubscriptionOptions(const std::string &id, const JID &service,
                                   const JID &jid, const std::string &node,
                                   const DataForm *options,
                                   const std::string &sid,
                                   const gloox::Error *error) {}
void IM::handleSubscriptionOptionsResult(const std::string &id,
                                         const JID &service, const JID &jid,
                                         const std::string &node,
                                         const std::string &sid,
                                         const gloox::Error *error) {}
void IM::handleSubscribers(const std::string &id, const JID &service,
                           const std::string &node,
                           const SubscriptionList &list,
                           const gloox::Error *error) {}
void IM::handleSubscribersResult(const std::string &id, const JID &service,
                                 const std::string &node,
                                 const SubscriberList *list,
                                 const gloox::Error *error) {}
void IM::handleAffiliates(const std::string &id, const JID &service,
                          const std::string &node, const AffiliateList *list,
                          const gloox::Error *error) {}
void IM::handleAffiliatesResult(const std::string &id, const JID &service,
                                const std::string &node,
                                const AffiliateList *list,
                                const gloox::Error *error) {}
void IM::handleNodeConfig(const std::string &id, const JID &service,
                          const std::string &node, const DataForm *config,
                          const gloox::Error *error) {}
void IM::handleNodeConfigResult(const std::string &id, const JID &service,
                                const std::string &node,
                                const gloox::Error *error) {}
void IM::handleNodeCreation(const std::string &id, const JID &service,
                            const std::string &node,
                            const gloox::Error *error) {}
void IM::handleNodeDeletion(const std::string &id, const JID &service,
                            const std::string &node,
                            const gloox::Error *error) {}
void IM::handleNodePurge(const std::string &id, const JID &service,
                         const std::string &node, const gloox::Error *error) {}
void IM::handleSubscriptions(const std::string &id, const JID &service,
                             const SubscriptionMap &subMap,
                             const gloox::Error *error) {}
void IM::handleAffiliations(const std::string &id, const JID &service,
                            const AffiliationMap &affMap,
                            const gloox::Error *error) {}

void IM::handleDefaultNodeConfig(const std::string &id, const JID &service,
                                 const DataForm *config,
                                 const gloox::Error *error) {
  Q_UNUSED(config);
  DEBUG_LOG(("id:%1 service:%2 error:%3")
                .arg(qstring(id))
                .arg(qstring(service.full()))
                .arg(qstring(error->text())));
}

std::string IM::getOnlineResource(const std::string &bare) {
  auto it = onlineMap.find(bare);
  if (it == onlineMap.end()) {
    return std::string{};
  }
  for (auto r : it->second) {
    return r;
  }
  return std::string{};
}

std::set<std::string> IM::getOnlineResources(const std::string &bare) {
  auto it = onlineMap.find(bare);
  if (it != onlineMap.end()) {
    return it->second;
  }
  return {};
}

void IM::doJingleMessage(const PeerId &peerId,
                         const Jingle::JingleMessage *jm) {

  DEBUG_LOG(("JingleMessage id:%1 action:%2")
                .arg(qstring(jm->id()))
                .arg(Jingle::ActionValues[jm->action()]));

  auto friendId = peerId.username;
  auto callId = qstring(jm->id());

  switch (jm->action()) {
  case Jingle::JingleMessage::propose: {
    /**
     * 接收到呼叫
     * 推送铃声
     * <message from='juliet@capulet.example/phone'
        to='romeo@montague.example'
        type='chat'>
        <ringing xmlns='urn:xmpp:jingle-message:0'
        id='ca3cf894-5325-482f-a412-a6e9f832298d'/>
        <store xmlns="urn:xmpp:hints"/>
      </message>
     */
    if(!mPeerRequestMedias.empty()){
      rejectJingleMessage(friendId,callId);
      return ;
    }
    mPeerRequestMedias.insert(peerId, jm->medias());

    emit receiveFriendCall(friendId, callId, true, jm->medias().size() > 1);
    break;
  }
  case Jingle::JingleMessage::reject: {
    /**
     * 对方接受
     */
    auto ms = jm->medias();
    emit receiveCallStateRejected(peerId, callId, ms.size() > 1);
    emit receiveFriendHangup(friendId);
    break;
  }
  case Jingle::JingleMessage::accept: {

    break;
  }
  case Jingle::JingleMessage::retract: {
    /**
     * 发起方挂断，挂断自己
     */
    emit receiveFriendHangup(friendId);
    break;
  }
  case Jingle::JingleMessage::proceed:
    if (friendId == qstring(self().username())) {
      /**
       * 自己的其他终端接受处理，挂断自己
       */
      emit receiveFriendHangup(friendId);
    } else {
      /**
       * 对方接受
       */
      auto medias = mPeerRequestMedias.value(peerId);
      emit receiveCallStateAccepted(peerId, callId, medias.size() > 1);
    }
    break;
  case Jingle::JingleMessage::finish:

    mPeerRequestMedias.remove(peerId);
    break;
  }
}

void IM::proposeJingleMessage(const QString &friendId, const QString &callId,
                              bool video) {

  auto it = sessionIdMap.find(stdstring(friendId));
  if (it == sessionIdMap.end())
    return;

  auto sit = sessionMap.find(it->second);
  if (sit == sessionMap.end())
    return;

  StanzaExtensionList exts;
  auto *jm = new Jingle::JingleMessage(Jingle::JingleMessage::propose,
                                       stdstring(callId));
  jm->addMedia(Jingle::RTP::Media::audio);
  if (video)
    jm->addMedia(Jingle::RTP::Media::video);
  exts.push_back(jm);

  // 缓存发起媒体
  mPeerRequestMedias.insert(PeerId(friendId), jm->medias());

  auto session = sit->second;
  session->setResource(currentJID.resource());
  session->send("", "", exts);
}

void IM::rejectJingleMessage(const QString &friendId, const QString &callId) {

  auto it = sessionIdMap.find(stdstring(friendId));
  if (it == sessionIdMap.end())
    return;

  auto sit = sessionMap.find(it->second);
  if (sit == sessionMap.end())
    return;

  StanzaExtensionList exts;
  auto *jm = new Jingle::JingleMessage(Jingle::JingleMessage::reject,
                                       stdstring(callId));
  exts.push_back(jm);

  auto session = sit->second;
  session->setResource(currentJID.resource());
  session->send("", "", exts);
}

void IM::acceptJingleMessage(const QString &friendId, const QString &callId) {
  auto it = sessionIdMap.find(stdstring(friendId));
  if (it == sessionIdMap.end())
    return;

  auto sit = sessionMap.find(it->second);
  if (sit == sessionMap.end())
    return;

  auto session = sit->second;
  auto *jm1 = new Jingle::JingleMessage(Jingle::JingleMessage::proceed,
                                        stdstring(callId));
  StanzaExtensionList exts1;
  exts1.push_back(jm1);
  session->setResource(currentJID.resource());
  session->send("", "", exts1);

  sendPresence(currentJID);
}

void IM::retractJingleMessage(const QString &friendId, const QString &callId) {
  auto it = sessionIdMap.find(stdstring(friendId));
  if (it == sessionIdMap.end())
    return;

  auto sit = sessionMap.find(it->second);
  if (sit == sessionMap.end())
    return;

  auto session = sit->second;
  auto *jm1 = new Jingle::JingleMessage(Jingle::JingleMessage::retract,
                                        stdstring(callId));
  StanzaExtensionList exts1;
  exts1.push_back(jm1);
  session->send("", "", exts1);
}

void IM::retry() { _connectThread->retry(); }

bool IM::removeGroup(JID jid) {
  auto r = findRoom(jid.username());
  if (!r) {
    return false;
  }
  r->leave();
  return true;
}

Disco::ItemList IM::handleDiscoNodeItems(const JID &from, const JID &to,
                                         const std::string &node) {

  DEBUG_LOG(("from:%1").arg(from.full().c_str()))

  return gloox::Disco::ItemList();
}
Disco::IdentityList IM::handleDiscoNodeIdentities(const JID &from,
                                                  const std::string &node) {
  return gloox::Disco::IdentityList();
}
StringList IM::handleDiscoNodeFeatures(const JID &from,
                                       const std::string &node) {
  return gloox::StringList();
}

void IM::handleDiscoItemsResult(const JID &from, const Disco::ItemList &items) {
  DEBUG_LOG(("from:%1").arg(from.full().c_str()))
  for (auto item : items) {
    DEBUG_LOG(("jid:%1").arg(qstring(item->jid().full())))
    sendServiceDiscoveryInfo(item->jid());
  }
}

void IM::onRoomReceived(const JID& jid){
  Q_UNUSED(jid);
}

void IM::onSelfNicknameChanged(const std::string &nickname) {

  m_selfInfo.nickname = nickname;

  for(auto &c : mConferenceList){

    JID roomJid(c.jid);
    roomJid.setResource(m_selfInfo.nickname);
    auto room = std::make_unique<MUCRoom>(_client.get(), roomJid, //
                                          this, this);

    room->join();
    room->getRoomInfo();
    DEBUG_LOG(("Join Room:%1 nick:%2")
                  .arg(qstring(roomJid.full())) //
                  .arg(qstring(room->nick()))); //

    m_roomMap.emplace(roomJid, room.release());
  }
}

} // namespace IM
} // namespace lib
