/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/

#pragma once

#include <QDateTime>
#include <QObject>
#include <QString>

namespace gloox {
class JID;
}

namespace lib {
namespace IM {
/**
 * @brief 连接状态
 *
 */
enum CONNECT_STATUS {
  CS_NONE = 0,
  CONNECTING,
  CONNECTED,
  DISCONNECTED,
  TIMEOUT,
  SUCCESS,
  FAILURE,
};

enum IMMsgType {
  Chat = 1,
  Groupchat = 4,
};

struct IMMessage {
public:
  IMMessage();
  IMMessage(IMMsgType type_,  //
            QString from_,    //
            QString body_,    //
            QString id_ = "", //
            QDateTime time_ = QDateTime::currentDateTime());

  IMMsgType type;
  QString id;
  QString from;
  QString body;
  QDateTime time;
};

struct SelfInfo {
  std::string nickname;
};

struct FriendId {
  /**
   * [username]@[server]
   */
  QString username;
  QString server;

  bool operator==(const FriendId &friendId) const;
  bool operator!=(const FriendId &friendId) const;
  bool operator<(const FriendId &friendId) const;

  FriendId();
  ~FriendId();
  explicit FriendId(const gloox::JID &);
  explicit FriendId(const FriendId &);
  explicit FriendId(const QString &jid);

  [[nodiscard]] QString toString() const { return username + "@" + server; }
};

struct PeerId : public FriendId {
  /**
   * [username]@[server]/[resource]
   */
  QString resource;

  PeerId();
  virtual ~PeerId();
  explicit PeerId(const QString &peerId);
  explicit PeerId(const gloox::JID &jid);
  explicit PeerId(const FriendId &friendId);
  bool operator==(const PeerId &peerId) const;
  bool operator==(const QString &username) const;

  [[nodiscard]] const QString toString() const {
    return username + "@" + server + "/" + resource;
  }
};

// Q_DECLARE_METATYPE(IMMessage)

} // namespace IM
} // namespace lib
