/*
 * Copyright (c) 2022 船山科技 chuanshantech.com
 * OkEDU-Classroom is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan
 * PubL v2. You may obtain a copy of Mulan PubL v2 at:
 *          http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the
 * Mulan PubL v2 for more details.
 */

#include "ok_rtc_defs.h"

namespace lib {
namespace ortc {
JingleContext::JingleContext() {

};

JingleContext::JingleContext(JingleSdpType sdpType_,     //
                             const std::string &peerId_, //
                             const std::string &sId,      //
                             const std::string &sVer,     //
                             const PluginList &plugins)
    : sdpType(sdpType_), peerId(peerId_), sessionId(sId), sessionVersion(sVer) {
  fromJingleSdp(plugins);
}

JingleContext::JingleContext(lib::ortc::JingleSdpType sdpType_, //
                             const std::string &peerId_,        //
                             const std::string &sId,            //
                             const std::string &sVer,           //
                             const lib::ortc::JingleContents &contents_)
: sdpType(sdpType_), peerId(peerId_),
      sessionId(sId), sessionVersion(sVer),
      contents(contents_) {

}

} // namespace ortc
} // namespace lib