﻿/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/
#pragma once

// #define GIT_VERSION "unkown"
// #define GIT_DESCRIBE "OkEDU-Classroom-Desktop"

#define ORGANIZATION_NAME "HNCS"
#define ORGANIZATION_DOMAIN "www.chuanshaninfo.com"

#define APPLICATION_ID "com.chuanshaninfo.okedu-classroom-desktop"
#define APPLICATION_NAME "OkEDU-Classroom-Desktop"

#define MAJOR_VERSION "2019"
#define MINOR_VERSION "1"
#define VERSION_SERIAL "10000"

#define LOGIN_WINDOW_WIDTH 1024
#define LOGIN_WINDOW_HEIGHT 520

//#define MAIN_WIDTH 1440
//#define MAIN_HEIGHT 860

//#define TOP_BAR_HEIGHT 60
//#define MENU_BAR_WIDTH 80

#define VIDEO_WIDGET_HEIGHT 170

#define IM_WIDTH 400
#define IM_MARGIN 10
#define IM_VIEW_HEIGHT 400

#define WHITEBOARD_WIDTH 1030

#define MATERIAL_VIEWPORT_WIDTH 1200
#define MATERIAL_VIEWPORT_HEIGHT 560

#define DEFAULT_BG_COLOR "#EFEFEF"

#define PAGE_HEADER_HEIGHT 60

#define SETTING_VIEW_WIDTH 850

// 登录地址
#define LOGIN_URL "https://meet.chuanshaninfo.com/api/ua/passport/login"
#define BACKEND_BASE_URL "https://meet.chuanshaninfo.com/api/classroom/v1"

// #define LOGIN_URL "http://localhost:3210/api/ua/passport/login"
// #define BACKEND_BASE_URL "http://localhost:3210/api/classroom/v1"

#define XMPP_SERVER_HOST "meet.chuanshaninfo.com"
#define XMPP_SERVER_PORT_HTTPS 443
#define XMPP_SERVER_PORT_SCOKET 5280

#define XMPP_FOCUS_NAME "focus"
#define XMPP_CONFERENCE_FOCUS "focus.meet.chuanshaninfo.com"

#define XMPP_CONF_SERVER_HOST "conference.meet.chuanshaninfo.com"
#define XMPP_PUBSUB_SERVICE "pubsub.meet.chuanshaninfo.com"

#define FILE_PROFILE_EXT ".profile"
#define FILE_INIT_EXT ".ini"

#define STUN_SERVER_URIS "stun:chuanshaninfo.com:3478,turn:chuanshaninfo.com:3478"
#define STUN_SERVER_USERNAME "gaojie"
#define STUN_SERVER_PASSWORD "hncs"
