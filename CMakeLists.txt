cmake_minimum_required(VERSION 3.22)

if(POLICY CMP0091)
    cmake_policy(SET CMP0091 NEW)
endif (POLICY CMP0091)

# 在QtCreator下禁用CONAN
set(QT_CREATOR_SKIP_CONAN_SETUP TRUE)
# 取消CONAN对编译器的检查
set(CONAN_DISABLE_CHECK_COMPILER ON)

project(OkEDU-Classroom-Desktop)



include(ProcessorCount)
ProcessorCount(N)
message(STATUS "ProcessorCount=" ${N})


set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

option(PLATFORM_EXTENSIONS "Enable platform specific extensions, requires extra dependencies" ON)

message(STATUS "PROJECT_SOURCE_DIR=" ${PROJECT_SOURCE_DIR})
message(STATUS "CMAKE_CURRENT_SOURCE_DIR=" ${CMAKE_CURRENT_SOURCE_DIR})
message(STATUS "CMAKE_HOST_SYSTEM_NAME=" ${CMAKE_HOST_SYSTEM_NAME})
message(STATUS "CMAKE_GENERATOR_PLATFORM=" ${CMAKE_GENERATOR_PLATFORM})
message(STATUS "CMAKE_SYSTEM_PROCESSOR=" ${CMAKE_SYSTEM_PROCESSOR})
message(STATUS "CMAKE_SYSTEM_VERSION=" ${CMAKE_SYSTEM_VERSION})


execute_process(
        COMMAND git describe --tags
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        OUTPUT_VARIABLE GIT_DESCRIBE
        ERROR_QUIET
        OUTPUT_STRIP_TRAILING_WHITESPACE
)

if (NOT GIT_DESCRIBE)
    set(GIT_DESCRIBE "Nightly")
endif ()

add_definitions(
        -DGIT_DESCRIBE="${GIT_DESCRIBE}"
)

execute_process(
        COMMAND git rev-parse HEAD
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        OUTPUT_VARIABLE GIT_VERSION
        ERROR_QUIET
        OUTPUT_STRIP_TRAILING_WHITESPACE
)

add_definitions(
        -DGIT_VERSION="${GIT_VERSION}"
)
message(STATUS "GIT_VERSION=" ${GIT_VERSION})


set(PLATFORM_ARCH ${CMAKE_C_PLATFORM_ID}-${CMAKE_SYSTEM_PROCESSOR})
message(STATUS "PLATFORM_ARCH=" ${PLATFORM_ARCH})


if (NOT N EQUAL 0)
    set(CTEST_BUILD_FLAGS -j${N})
    set(ctest_test_args ${ctest_test_args} PARALLEL_LEVEL ${N})
endif ()

# include(CheckAtomic)


# 设置编译器相关
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

# 设置Qt配置参数
if (DEFINED ENV{QTDIR})
#     请配置 QTDIR 环境变量，如：QTDIR=/opt/Qt-5.15.6-static
    set(QT_DIR $ENV{QTDIR})
else ()
    #手动设置Qt路径:
#    set(QT_DIR "E:/Qt5/5.15.2/msvc2019_64")
endif ()

# 根据Qt类型，设置动态(安装默认)或者静态(下载的静态版)
set(LINK_STATIC_QT TRUE)

# 构建类型=Debug,Release
set(CMAKE_BUILD_TYPE Debug)

set(CMAKE_PREFIX_PATH ${QT_DIR})
message(STATUS "CMAKE_PREFIX_PATH=" ${CMAKE_PREFIX_PATH})



if (UNIX)
    find_package(PkgConfig REQUIRED)
endif ()

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

find_package(QT NAMES Qt6 Qt5  COMPONENTS
        Core Concurrent Widgets Gui Multimedia MultimediaWidgets
        Network Xml Svg OpenGL DBus
        LinguistTools UiTools
        REQUIRED)

find_package(Qt${QT_VERSION_MAJOR} COMPONENTS
        Core Concurrent Widgets Gui Multimedia MultimediaWidgets
        Network Xml Svg OpenGL DBus
        LinguistTools UiTools
        REQUIRED)

# 设置Qt模块包含头文件和库
include_directories(${CMAKE_PREFIX_PATH}/include)
link_directories(${CMAKE_PREFIX_PATH}/lib)

set(QtModules Core Widgets Gui Multimedia MultimediaWidgets Network Concurrent Svg Xml OpenGL ANGLE)
if(UNIX)
    set(QtModules ${QtModules} DBus)
endif(UNIX)

foreach (Module ${QtModules})
    set(Qt${Module}${QT_VERSION_MAJOR}_INCLUDES ${CMAKE_PREFIX_PATH}/include/Qt${Module})
    include_directories(${Qt${Module}${QT_VERSION_MAJOR}_INCLUDES})
endforeach (Module)

IF (WIN32)

    set(ARCH "x64")
    string(TOLOWER ${CMAKE_C_PLATFORM_ID}-${ARCH} PLATFORM_ARCH)
    message(STATUS "PLATFORM_ARCH=" ${PLATFORM_ARCH})

    # Windows暂时构建类型Release
    set(CMAKE_BUILD_TYPE "Release")
    set(CMAKE_CONFIGURATION_TYPES "Debug;Release;MinSizeRel;RelWithDebInfo")


    # Conan检查
    if (NOT EXISTS build/deps)
        execute_process(COMMAND conan install . -s compiler.runtime=MT --build=missing)
    endif ()

    # 设置Gloox位置
    set(GLOOX_DIR ${CMAKE_SOURCE_DIR}/3rdparty/gloox/libgloox-${PLATFORM_ARCH})

    # 设置WebRTC静态库位置
    set(WebRTC_DIR ${PROJECT_SOURCE_DIR}/3rdparty/webrtc/libwebrtc-${PLATFORM_ARCH})

    # OpenSSL 头文件
    set(OpenSSL_INC_DIR ${WebRTC_DIR}/include/third_party/boringssl/src/include)
    include_directories(${OpenSSL_INC_DIR})

    # 设置相关宏定义
    add_definitions(
            -DWEBRTC_WIN
            # windows预处理宏
            -DNOMINMAX
            -DWIN32_LEAN_AND_MEAN

            # OpenAL
            -DAL_LIBTYPE_STATIC
    )
    set(MSVC_VERSION)


    IF (MSVC)
        OPTION(USE_MP "use multiple" ON)
        OPTION(ProjectConfig_Global_COMPILE_FLAGS_WITH_MP
                "Set The Global Option COMPILE_FLAGS /MP to target." ON)
        if (ProjectConfig_Global_COMPILE_FLAGS_WITH_MP OR USE_MP)
            set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /MP")
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP")
        endif ()
        set(VS_STARTUP_PROJECT ${PROJECT_NAME})
    ENDIF (MSVC)

ELSEIF (UNIX)
    string(TOLOWER ${PLATFORM_ARCH} PLATFORM_ARCH)

    # Unix暂时构建类型Debug
    set(CMAKE_BUILD_TYPE "Debug")

    # 设置Gloox位置
    set(GLOOX_DIR ${CMAKE_SOURCE_DIR}/3rdparty/gloox/libgloox-${PLATFORM_ARCH})

    # 设置WebRTC静态库位置
    set(WebRTC_DIR ${CMAKE_SOURCE_DIR}/3rdparty/webrtc/libwebrtc-${PLATFORM_ARCH})

    # 设置相关宏定义
    add_definitions(
            -DWEBRTC_POSIX
            -DWEBRTC_LINUX
            -DQ_OS_POSIX
            -D_GLIBCXX_USE_CXX11_ABI=1
            -DOPENSSL_IS_BORINGSSL=1
    )
ELSEIF (APPLE)
    # do something related to APPLE
    message(ERROR "暂不支持 Not supported temporarily")
ENDIF ()

message(STATUS "WebRTC_DIR=" ${WebRTC_DIR})
message(STATUS "GLOOX_DIR=" ${GLOOX_DIR})

# conan
include_directories(build/deps/include)
link_directories(build/deps/lib)

#Gloox
add_definitions(-DQSSLSOCKET_DEBUG -DLOG_XMPP -DLOG_TO_FILE -DHAVE_OPENSSL)
#if(LINK_STATIC_QT)
#    add_definitions(-DLOG_TO_FILE)
#endif ()

string(TOLOWER ${CMAKE_BUILD_TYPE} LOWER_CMAKE_BUILD_TYPE)

if (UNIX)
    include_directories(${Qt5LinuxAccessibilitySupport_INCLUDES})
    set(Qt5LinuxAccessibilitySupport_INCLUDES ${CMAKE_PREFIX_PATH}/include/QtLinuxAccessibilitySupport)
endif ()

# -pedantic -fsanitize=address,undefined,leak,integer -Wextra -Wall -Wmacro-redefined -Wbuiltin-macro-redefined
if(UNIX)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fstack-protector-all -Wunused-parameter -Wunused-function -Wstrict-overflow -Wstrict-aliasing -Wstack-protector")
endif(UNIX)

if (CMAKE_C_COMPILER_ID STREQUAL "Clang")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -Wextra -Wall")
    if (WIN32)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /EHsc /W0")
        if(LINK_STATIC_QT)
            set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")
            set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
        else ()
            set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MDd")
            set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MD")
        endif ()
    endif ()
elseif (CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -Wextra -Wall")
elseif (CMAKE_C_COMPILER_ID STREQUAL "MSVC")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /EHsc /W0")
    if(LINK_STATIC_QT)
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")
        set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
    else ()
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MDd")
        set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MD")
    endif ()
else ()
    message(FATAL_ERROR "Not support compiler: ${CMAKE_C_COMPILER_ID} yet.")
endif ()

message(STATUS "CMAKE_C_COMPILER_ID=" ${CMAKE_C_COMPILER_ID})
message(STATUS "CMAKE_C_COMPILER=" ${CMAKE_C_COMPILER})
message(STATUS "CMAKE_C_FLAGS=" ${CMAKE_C_FLAGS})

message(STATUS "CMAKE_CXX_COMPILER_ID=" ${CMAKE_CXX_COMPILER_ID})
message(STATUS "CMAKE_CXX_COMPILER=" ${CMAKE_CXX_COMPILER})
message(STATUS "CMAKE_CXX_FLAGS=" ${CMAKE_CXX_FLAGS})
message(STATUS "CMAKE_CXX_FLAGS_DEBUG=" ${CMAKE_CXX_FLAGS_DEBUG})
message(STATUS "CMAKE_CXX_FLAGS_RELEASE=" ${CMAKE_CXX_FLAGS_RELEASE})



include_directories(.)
include_directories(./lib)
include_directories(./modules)
include_directories(./modules/im)
include_directories(./modules/im/src)
include_directories(./UI)
include_directories(classroom)
include_directories(src)
include_directories(src/net)

add_subdirectory(UI)
add_subdirectory(classroom)
add_subdirectory(modules)
add_subdirectory(lib)
add_subdirectory(base)

# Add RC files.
set(${PROJECT_NAME}_RC_FILES
        icon/icon.rc)

# Add QRC files.
set(${PROJECT_NAME}_QRC_FILES
        UI/resources/resources.qrc)

set(${PROJECT_NAME}_RESOURCES
        ${${PROJECT_NAME}_RC_FILES}
        ${${PROJECT_NAME}_QRC_FILES}
        )


set(${PROJECT_NAME}_SOURCES
        main.cpp

        )

qt5_add_translation(${PROJECT_NAME}_QM_FILES translations/zh_CN.ts)

file(WRITE "${PROJECT_BINARY_DIR}/translations.qrc.in" "<!DOCTYPE RCC>
<RCC version=\"1.0\">
  <qresource prefix=\"/translations/${PROJECT_NAME}\">
")

foreach (qm ${${PROJECT_NAME}_QM_FILES})
    get_filename_component(qm_name ${qm} NAME)
    file(APPEND "${PROJECT_BINARY_DIR}/translations.qrc.in"
            "    <file alias=\"${qm_name}\">${qm}</file>\n")
endforeach (qm)

file(APPEND "${PROJECT_BINARY_DIR}/translations.qrc.in"
        "  </qresource>
</RCC>")

execute_process(COMMAND ${CMAKE_COMMAND} -E copy_if_different
        ${PROJECT_BINARY_DIR}/translations.qrc.in
        ${PROJECT_BINARY_DIR}/translations.qrc)

qt5_add_resources(
        ${PROJECT_NAME}_RESOURCES
        ${PROJECT_BINARY_DIR}/translations.qrc
)

if (WIN32)
    set(PLATFORM_LIBS

            swresample
            swscale
            avdevice
            avutil
            avcodec
            avfilter
            avformat

            brotlicommon-static
            brotlidec-static
            brotlienc-static

            charset
            exif
            fdk-aac
#            freetype
            iconv
            lzma
            mp3lame
            ogg
            openh264
            openjp2
            opus
            postproc
            qrencode
            sqlite3
            vorbis
            vorbisenc
            vorbisfile

            webp
            webpdecoder
            webpdemux
            webpmux
            x264
            x265
            sharpyuv

            OpenAL32
            vpxmt


            bz2

            d3d11
            dxgi
            shlwapi
            Iphlpapi
            dwmapi
            dmoguids
            wmcodecdspuuid
            Winmm
            amstrmid
            msdmo
            strmiids
            psapi
            Vfw32
            shell32
            oleaut32
            ole32
            user32
            advapi32
            bcrypt
            Mf
            Mfplat
            Mfreadwrite
            mfuuid
            dxva2
            evr
            strmbase
            )
elseif (UNIX)
    set(PLATFORM_LIBS
            z
            ssl
            crypto
            atomic
            )
endif ()

set(ALL_LIBRARIES


        ${Qt5Core_LIBRARIES}
        ${Qt5Network_LIBRARIES}
        ${Qt5Gui_LIBRARIES}
        ${Qt5Widgets_LIBRARIES}
        ${Qt5MultimediaWidgets_LIBRARIES}
        ${Qt5Multimedia_LIBRARIES}
        ${Qt5Xml_LIBRARIES}
        ${Qt5Svg_LIBRARIES}
        ${PLATFORM_LIBS}
        )

#add_library(${PROJECT_NAME}_static
#        ${${PROJECT_NAME}_SOURCES}
#        ${${PROJECT_NAME}_RESOURCES})
#
add_executable(${PROJECT_NAME}
        ${${PROJECT_NAME}_SOURCES}
        ${${PROJECT_NAME}_RESOURCES})

target_link_libraries(
        ${PROJECT_NAME}
        Classroom
        UIWindow
        UI
        UIWidget
        UICore
        Smartboard
        IM
        IMCore
        Painter
        OkRTC
        OkRTC_webrtc
        Session
        Settings
        Network
        Backend

        OkBase
        ${ALL_LIBRARIES}
)

if(MSVC)
    set_property(TARGET ${PROJECT_NAME} PROPERTY MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
endif (MSVC)

if (LOWER_CMAKE_BUILD_TYPE STREQUAL debug)
    target_compile_definitions(${PROJECT_NAME} PRIVATE DEBUG_BUILD)
endif()

# include(Testing)
