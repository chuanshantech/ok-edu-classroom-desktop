#pragma once

#include <QJsonObject>
#include <QJsonValue>
#include <QString>

class OJSON {
public:
  OJSON() {}
  ~OJSON() {}
  virtual void fromJSON(const QJsonObject &data) = 0;
  virtual QJsonObject toJSON() = 0;
};