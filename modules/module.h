/** Copyright (c) 2022 船山科技 chuanshantech.com
OkEDU-Classroom is licensed under Mulan PubL v2.
You can use this software according to the terms and conditions of the Mulan
PubL v2. You may obtain a copy of Mulan PubL v2 at:
         http://license.coscl.org.cn/MulanPubL-2.0
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PubL v2 for more details.
*/

#ifndef OKEDU_CLASSROOM_DESKTOP_MODULE_H
#define OKEDU_CLASSROOM_DESKTOP_MODULE_H

#include "im/src/persistence/profile.h"
#include <QString>
#include <QWidget>

class Module {
public:
  static QString Name();
  virtual void init(Profile *p) = 0;
  virtual QString name() = 0;
  virtual void start(QWidget *parent = nullptr) = 0;
  //  virtual void destroy(Module*) = 0;
  [[nodiscard]] virtual bool isStarted() = 0;
  virtual void hide() = 0;
};

#endif // OKEDU_CLASSROOM_DESKTOP_MODULE_H
