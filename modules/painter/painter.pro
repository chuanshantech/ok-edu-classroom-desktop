TEMPLATE = lib
TARGET = painter
CONFIG += qt c++17 staticlib

QT += core network gui widgets
# av avwidgets

INCLUDEPATH += $$PWD \
    $$PWD/src   \
    $$PWD/include   \
    $$PWD/..    \
    $$PWD/../../    \
    $$PWD/../../3rdparty    \
    $$PWD/../../3rdparty/gloox \
                /lvm/1/code/github              \
                /lvm/1/code/github/pdfium       \
                /lvm/1/code/github/QtAV/src/\
                /lvm/1/code/github/QtAV/src/QtAV \

DEFINES +=
QMAKE_CXXFLAGS += -w

HEADERS += \
    $$PWD/Base.h \
    $$PWD/ColorPanel.h \
    $$PWD/DefferedCaller.h \
    $$PWD/Painter.h \
    $$PWD/PaintItem.h \
    $$PWD/PaintItemFactory.h \
    $$PWD/PaintUser.h \
    $$PWD/SharedPaintCommand.h \
    $$PWD/SharedPaintCommandManager.h \
    $$PWD/SharedPainterScene.h \
    $$PWD/SharedPaintManagementData.h \
    $$PWD/SharedPaintManager.h \
    $$PWD/SharedPaintPolicy.h \
    $$PWD/SharedPaintTask.h \
    $$PWD/SharedPaintTaskFactory.h \
    $$PWD/SystemPacketBuilder.h \
    $$PWD/TaskPacketBuilder.h \
    $$PWD/Util.h \
    $$PWD/WhiteboardController.h \
    $$PWD/isharedpaintevent.h \
    painterdispatcher.h \
    painterrenderer.h \
    painterevent.h \
    paintitemline.h \
    oqgraphicsitem.h \
    PainterWidgetProxy.h \
    PainterMdiArea.h \
    smartboard.h \
    smartboardcontroller.h \
    smartboardcontrollerselect.h \
    smartboardcontrollervoice.h \
    smartboarddraw.h \
    smartboarddrawline.h \
    smartboarddrawmove.h \
    smartboarddrawremove.h \
    smartboarddrawtext.h

SOURCES += \
    $$PWD/DefferedCaller.cpp \
    $$PWD/Painter.cpp \
    $$PWD/SharedPaintCommand.cpp \
    $$PWD/SharedPaintCommandManager.cpp \
    $$PWD/SharedPainterScene.cpp \
    $$PWD/SharedPaintManager.cpp \
    $$PWD/SharedPaintTask.cpp \
    $$PWD/Util.cpp \
    $$PWD/WhiteboardController.cpp \
    painterdispatcher.cpp \
    painterrenderer.cpp \
    painterevent.cpp \
    oqgraphicsitem.cpp \
    PainterWidgetProxy.cpp \
    PainterMdiArea.cpp \
    smartboard.cpp \
    smartboardcontroller.cpp \
    smartboardcontrollerselect.cpp \
    smartboardcontrollervoice.cpp \
    smartboarddraw.cpp \
    smartboarddrawline.cpp \
    smartboarddrawmove.cpp \
    smartboarddrawremove.cpp \
    smartboarddrawtext.cpp


