#include "smartboard.h"

#include <gloox/okplugin.h>

namespace SmartBoard {
DrawItem::DrawItem()
    //: Plugin()
    {}

DrawItem::~DrawItem() {}

const DrawId &DrawItem::id() const { return _id; }

Action DrawItem::action() const { return _action; }

void DrawItem::setAction(Action action) { _action = action; }

const std::vector<Point> &DrawItem::points() const { return _points; }

DrawType DrawItem::type() { return _type; }

/**
 * ControllerItem
 *
 */
ControllerItem::ControllerItem() /*: Plugin()*/ {}

ControllerItem::ControllerItem(const DrawId &id) : _id(id)/*, Plugin()*/ {}

ControllerItem::~ControllerItem() {}

Action ControllerItem::action() const { return _action; }

void ControllerItem::setAction(Action action) { _action = action; }
ControllerType ControllerItem::type() const { return _type; }
} // namespace SmartBoard
