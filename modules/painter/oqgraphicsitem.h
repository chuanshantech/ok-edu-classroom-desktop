﻿#ifndef OQGRAPHICSITEM_H
#define OQGRAPHICSITEM_H

#include <QGraphicsTextItem>
#include <QPen>

QT_BEGIN_NAMESPACE
class QFocusEvent;
class QGraphicsItem;
class QGraphicsScene;
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE


class OQGraphicsTextItem : public QGraphicsTextItem
{
    Q_OBJECT

public:
    enum { Type = UserType + 3 };

    OQGraphicsTextItem(QGraphicsItem *parent = nullptr);

    virtual int type() const override { return Type; }

signals:
    void lostFocus(OQGraphicsTextItem *item);

    void itemChanged(OQGraphicsTextItem *item,
                        GraphicsItemChange change,
                        const QVariant &value);

protected:
    QVariant itemChange(GraphicsItemChange change,
                        const QVariant &value) override;

    void focusOutEvent(QFocusEvent *event) override;

    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
};

#endif // QGRAPHICSITEM_H
