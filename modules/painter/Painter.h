﻿#pragma once

#include <QClipboard>
#include <QString>
#include <QWidget>
#include <memory>

#include "Base.h"
#include "PaintItem.h"
#include "Painter.h"
#include "PainterMdiArea.h"
#include "SharedPaintManager.h"
#include "SharedPainterScene.h"
#include "painterdispatcher.h"
#include "painterevent.h"
#include "painterrenderer.h"

#include "lib/network/NetworkManager.h"
#include <base/timer.h>

class OPainterToolBox;

namespace painter {

class WhiteboardController;

class Painter : public QWidget {
public:
  Painter(QWidget *parent = nullptr);
  virtual ~Painter() override;

  static Painter *Get(QWidget *parent = nullptr);

  virtual const CSharedPaintManager *manager() const {
    return painterManager_.get();
  }

  virtual const WhiteboardController *controller() const {
    return _oController.get();
  }

protected:
  void showEvent(QShowEvent *event) override;

  void resizeEvent(QResizeEvent *e) override;

private:
  network::NetworkManager *networkManager_;
  // IMSmartBoard
  lib::smartboard::IMSmartBoard *_imSmartBoard;

  // 画板管理器
  std::unique_ptr<CSharedPaintManager> painterManager_;

  // 工具箱
  std::unique_ptr<OPainterToolBox> _oToolbox;

  // 控制面板
  std::unique_ptr<WhiteboardController> _oController;

  std::unique_ptr<base::DelayedCallTimer> _delayCaller;

  PainterMdiArea *mdiArea;

signals:

public slots:
  void onReceivedUrlInfo(network::FileResult);
  void onReceivedDraw(SmartBoard::SmartBoardDraw *);
  void onOpenFile(std::shared_ptr<CFileItem>);

  void setToolType(painter::ToolboxType toolboxType);

  void setTextColor(QColor clr);
  void setTextWeight(int weight);

  void setPenColor(QColor clr);
  void setPenWeight(int weight);

  void onCtrollerChecked(context::WB::WB_CTRL, bool);

  //  void onPubSubEvent(gloox::PubSub::ItemList &);
};

} // namespace painter
