﻿#include "oqgraphicsitem.h"

OQGraphicsTextItem::OQGraphicsTextItem(QGraphicsItem *parent)
    : QGraphicsTextItem(parent)
{
    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemIsSelectable);
}

QVariant OQGraphicsTextItem::itemChange(    
    GraphicsItemChange change,
    const QVariant &value)
{
    // emit itemChanged(this, change, value);
    if (change == QGraphicsItem::ItemSelectedHasChanged)
        emit itemChanged(this, change, value);
    return value;
}

void OQGraphicsTextItem::focusOutEvent(QFocusEvent *event)
{
    setTextInteractionFlags(Qt::NoTextInteraction);
    QGraphicsTextItem::focusOutEvent(event);
    emit lostFocus(this);
}

void OQGraphicsTextItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (textInteractionFlags() == Qt::NoTextInteraction)
        setTextInteractionFlags(Qt::TextEditorInteraction);

    QGraphicsTextItem::mouseDoubleClickEvent(event);
}
