﻿#include "application.h"

#include <memory>

#include <QApplication>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QString>
#include <QTranslator>

#include "UI/core/FontManager.h"
#include "UI/window/window/LoginWindow.h"
#include "base/logs.h"
#include "launcher.h"
#include "modules/im/src/persistence/settings.h"
#include "network/backend/domain/UserInfo.h"
#include "settings/translator.h"

using namespace session;
using namespace core;
using namespace base;

namespace core {

Application::Application(core::Launcher *launcher, int &argc, char *argv[])
    : QApplication(argc, argv), _launcher(launcher), _argc(argc), _argv(argv) {
  qDebug() << (qstring("Cleanup..."));

  DEBUG_LOG(("启动 argc:%1").arg(argc));
  for (int i = 0; i < argc; i++) {
    DEBUG_LOG(("argv:%1->%2").arg(argc).arg(argv[i]));
  }

#if QT_VERSION >= QT_VERSION_CHECK(5, 7, 0)
  setDesktopFileName(APPLICATION_ID);
#endif

  setApplicationName(qsl(APPLICATION_NAME));
  setApplicationVersion("\nGit commit: " + QString(GIT_VERSION));

  // Windows platform plugins DLL hell fix
  QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());
  addLibraryPath("platforms");

  // 统一注册类型
  qRegisterMetaType<UI::PageMenu>("PageMenu");
  qRegisterMetaType<backend::UserInfo>("backend::UserInfo");
  qRegisterMetaType<backend::UserJID>("backend::UserJID");
  qRegisterMetaType<backend::UserJID>("backend::UserJID&");
  qRegisterMetaType<backend::PeerStatus>("backend::PeerStatus");
  qRegisterMetaType<backend::PeerStatus>("backend::PeerStatus&");
  qRegisterMetaType<std::list<std::string>>("std::list<std::string>&");

  // 字体
  FontManager fm;
  fm.loadFonts();

  // 延时器
  _delayCaller = std::make_unique<DelayedCallTimer>();

  // 设置
  _settingManager = std::make_unique<SettingManager>(this);

  _auth = std::make_unique<session::AuthSession>(this);

  //
  //  connect(session(), SIGNAL(loginResult(network::CONNECT_STATUS)), this,
  //          SLOT(onConnectResult(network::CONNECT_STATUS)));

  connect(this, &QApplication::aboutToQuit, this, &Application::cleanup);

  // 国际化
  //  Settings &settings = Settings::getInstance();
  //  QString locale = settings.getTranslation();
  //  qDebug() << "locale" << locale;
  //  settings::Translator::translate(APPLICATION_NAME, locale);
}

Application *Application::Instance() {
  return qobject_cast<Application *>(qApp);
}

void Application::start() {

  // if (!session()->authenticated()) {
  this->createLoginUI();
  // } else {
  //   this->startMainUI();
  // }
  // nexus->start();
}

void Application::createLoginUI() {
  m_loginWindow = std::make_unique<UI::window::LoginWindow>();
  connect(m_loginWindow.get(), &UI::window::LoginWindow::loginResult,
          [&](session::SignInInfo &signInInfo, session::LoginResult &result) {
            if (result.status == SUCCESS) {
              onLoginSuccess(signInInfo);
            }
          });

  connect(this, &Application::createProfileFailed,
          m_loginWindow.get(), &LoginWindow::onProfileLoadFailed);

  m_loginWindow->show();

}
void Application::deleteLoginUI() {
  disconnect(m_loginWindow.get());
  disconnect(this, &Application::createProfileFailed,
          m_loginWindow.get(), &LoginWindow::onProfileLoadFailed);
  m_loginWindow.reset();
}

void Application::closeLoginUI() {
  // 关闭login窗口
  deleteLoginUI();
}

void Application::onLoginSuccess(session::SignInInfo &signInInfo) {
  DEBUG_LOG(qsl("onLoginSuccess account:%1").arg(signInInfo.account));

  QCommandLineParser parser;
  if (!Profile::exists(signInInfo.account)) {
    profile = Profile::createProfile(signInInfo.account, &parser,
                                     signInInfo.password);
  } else {
    profile =
        Profile::loadProfile(signInInfo.account, &parser, signInInfo.password);
  }

  if(!profile){
    qWarning() <<"不能创建新的Profile，个人信息初始化异常或者重复登录";
    emit createProfileFailed("个人信息初始化异常或者重复登录!");
    return ;
  }

  // 初始化 IM 模块
  initModuleIM();

  // 初始化截屏模块
  initScreenCaptor();

  // 启动主界面
  startMainUI();

  // 关闭登录界面
  closeLoginUI();
}

void Application::startMainUI() {
  DEBUG_LOG(qsl("启动主界面"));

  this->loadService();

  m_windowManager = UI::WindowManager::Instance();
  connect(m_windowManager, &WindowManager::menuPushed, this,
          &Application::onMenuPushed);
   m_windowManager->startMainUI();
}

void Application::stopMainUI() {
  m_windowManager->stopMainUI();
  delete m_windowManager;
  m_windowManager = nullptr;
}

void Application::loadService() {}

void Application::initScreenCaptor() {
//  DEBUG_LOG(("initScreenCaptor ..."));
//  auto _screenCaptor = new OEScreenshot();
//  m_moduleMap.insert(_screenCaptor->name(), _screenCaptor);
//  DEBUG_LOG(("initScreenCaptor finished"));
}

void Application::cleanup() {
  DEBUG_LOG(("Cleanup..."));
  qDebug() << (qstring("Cleanup..."));

  // force save early even though destruction saves, because Windows OS will
  // close qTox before cleanup() is finished if logging out or shutting down,
  // once the top level window has exited, which occurs in ~Widget within
  // ~Nexus. Re-ordering Nexus destruction is not trivial.
  auto &s = Settings::getInstance();
  s.saveGlobal();
  s.savePersonal();
  s.sync();

  Nexus::destroyInstance();
  // TODO
  //  CameraSource::destroyInstance();
  Settings::destroyInstance();

  DEBUG_LOG(("Cleanup success"));
}

void Application::finish() {}

void Application::onMenuPushed(PageMenu menu, bool checked) {
  DEBUG_LOG(("menu:%1").arg((int)menu).arg(checked));

  switch (menu) {
  case UI::PageMenu::chat: {
    auto container = m_windowManager->getContainer(menu);
    Module *m = m_moduleMap.value(Nexus::Name());
    if (checked) {
      if (!m->isStarted()) {
        m->start(container);
      }
    }
    //    else {
    //      m->hide();
    //    }
    break;
  }
  }
}

void Application::onMenuReleased(PageMenu menu, bool checked) {}

void Application::initModuleIM() {

  DEBUG_LOG(("IM..."))

  Nexus &nexus = Nexus::getInstance();
  nexus.init(static_cast<Profile *>(profile));
  //    nexus.bootstrapWithProfile(static_cast<Profile *>(profile));
  m_moduleMap.insert(nexus.name(), &nexus);

  connect(&nexus,
          &Nexus::updateAvatar,
          this,
          &Application::onAvatar);
}

void Application::onAvatar(const QPixmap &pixmap) {
  auto menu = m_windowManager->getMainMenu();
  if(!menu)
    return;

  menu->setAvatar(pixmap);
}

} // namespace core
