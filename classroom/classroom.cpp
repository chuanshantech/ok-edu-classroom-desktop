#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main0(int argc, char *argv[]) {
	QGuiApplication app(argc, argv);

	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/classroom.qml")));

	return app.exec();
}

