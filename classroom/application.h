﻿#pragma once

#include <QApplication>
#include <QObject>
#include <memory>

#include <base/timer.h>

#include <UI/core/ControllerManager.h>
#include <UI/core/SettingManager.h>
#include <UI/WindowManager.h>
#include "UI/window/window/LoginWindow.h"
#include <UI/window/window/MainWindow.h>

#include "launcher.h"
#include "lib/network/NetworkManager.h"
#include "modules/im/src/nexus.h"
#include "modules/module.h"

namespace core {

using namespace network;
using namespace UI::window;

class Application : public QApplication {
  Q_OBJECT
public:
  Application(core::Launcher *launcher, int &argc, char **argv);

  static Application *Instance();

  void start();

  void finish();

  inline SettingManager *settingManager() { return _settingManager.get(); }

  inline ControllerManager *controllerManager() {
    return _controllerManager.get();
  }

protected:
  void initScreenCaptor();

  void initIM();

private:
  core::Launcher *_launcher;

  QMap<QString, Module *> m_moduleMap;

  //  void *imWindow = nullptr;
  void *profile = nullptr;

  int _argc;
  char **_argv;

  std::unique_ptr<session::AuthSession> _auth;

  UI::window::LoginWindow* _loginWindow;

  std::unique_ptr<SettingManager> _settingManager;

  std::unique_ptr<ControllerManager> _controllerManager;

  std::unique_ptr<UI::window::LoginWindow> m_loginWindow;

  WindowManager *m_windowManager;

  std::unique_ptr<base::DelayedCallTimer> _delayCaller;

  QWidget *_main;

  void loadService();

  void createLoginUI();
  void deleteLoginUI();
  void closeLoginUI();

  void startMainUI();
  void stopMainUI();

signals:
  void createProfileFailed(QString msg);


public slots:
  void cleanup();

  void onLoginSuccess(session::SignInInfo&);

  void onMenuPushed(PageMenu menu, bool checked);
  void onMenuReleased(PageMenu menu, bool checked);
  void initModuleIM();

  void onAvatar(const QPixmap &);
};
} // namespace core
