# 协议

## 登录

```bash
curl 'https://meet.chuanshaninfo.com/api/ua/passport/login' \
-X POST \
-H 'Accept: application/json, text/plain, */*' \
-H 'Content-Type: application/json' \
--data-raw '{"loginId":"18910221510","password":"200791"}'
```

# 返回：

```json
{
  "type": "success",
  "data": "22256927-da29-4a3d-9170-08890a6d45c5",
  "msg": "登录成功",
  "extra": {},
  "warn": false,
  "verify": false,
  "success": true,
  "error": false
}
```

## 功能接收

```xml

<iq to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    id='ada529ea6bb33da70a60d20adf2824872fa612d50000000b' type='result'
    from='lobby.meet.chuanshaninfo.com'>
    <query xmlns='http://jabber.org/protocol/disco#info'>
        <identity category='conference' type='text' name='Prosody Chatrooms'/>
        <feature var='http://jabber.org/protocol/muc'/>
        <feature var='http://jabber.org/protocol/muc#unique'/>
        <feature var='http://jabber.org/protocol/disco#info'/>
        <feature var='http://jabber.org/protocol/disco#items'/>
        <feature var='http://jabber.org/protocol/commands'/>
    </query>
</iq>

<iq to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    id='ada529ea6bb33da70a60d20adf2824872fa612d50000000d' type='error'
    from='speakerstats.meet.chuanshaninfo.com'>
<error type='cancel'>
    <service-unavailable xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
</error>
</iq>

<iq to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    id='ada529ea6bb33da70a60d20adf2824872fa612d50000000e' type='result'
    from='pubsub.meet.chuanshaninfo.com'>
<query xmlns='http://jabber.org/protocol/disco#info'>
    <identity category='pubsub' type='service' name='Prosody PubSub Service'/>
    <feature var='http://jabber.org/protocol/pubsub'/>
    <feature var='http://jabber.org/protocol/pubsub#multi-items'/>
    <feature var='http://jabber.org/protocol/pubsub#delete-items'/>
    <feature var='http://jabber.org/protocol/pubsub#outcast-affiliation'/>
    <feature var='http://jabber.org/protocol/pubsub#purge-nodes'/>
    <feature var='http://jabber.org/protocol/pubsub#create-nodes'/>
    <feature var='http://jabber.org/protocol/pubsub#retrieve-subscriptions'/>
    <feature var='http://jabber.org/protocol/pubsub#meta-data'/>
    <feature var='http://jabber.org/protocol/pubsub#retract-items'/>
    <feature var='http://jabber.org/protocol/pubsub#publish'/>
    <feature var='http://jabber.org/protocol/pubsub#item-ids'/>
    <feature var='http://jabber.org/protocol/pubsub#subscription-options'/>
    <feature var='http://jabber.org/protocol/pubsub#instant-nodes'/>
    <feature var='http://jabber.org/protocol/pubsub#create-and-configure'/>
    <feature var='http://jabber.org/protocol/pubsub#retrieve-items'/>
    <feature var='http://jabber.org/protocol/pubsub#auto-create'/>
    <feature var='http://jabber.org/protocol/pubsub#config-node'/>
    <feature var='http://jabber.org/protocol/pubsub#subscribe'/>
    <feature var='http://jabber.org/protocol/pubsub#access-open'/>
    <feature var='http://jabber.org/protocol/pubsub#persistent-items'/>
    <feature var='http://jabber.org/protocol/pubsub#member-affiliation'/>
    <feature var='http://jabber.org/protocol/pubsub#modify-affiliations'/>
    <feature var='http://jabber.org/protocol/pubsub#publisher-affiliation'/>
    <feature var='http://jabber.org/protocol/pubsub#retrieve-default'/>
    <feature var='http://jabber.org/protocol/pubsub#delete-nodes'/>
    <feature var='http://jabber.org/protocol/pubsub#publish-options'/>
    <feature var='http://jabber.org/protocol/disco#info'/>
    <feature var='http://jabber.org/protocol/disco#items'/>
</query>
</iq>

<iq to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    id='ada529ea6bb33da70a60d20adf2824872fa612d50000000f' 
    type='result' 
    from='auth.meet.chuanshaninfo.com'>
<query xmlns='http://jabber.org/protocol/disco#info'>
    <identity category='server' type='im' name='Prosody'/>
    <identity category='pubsub' type='pep' name='Prosody'/>
    <feature var='urn:xmpp:ping'/>
    <feature var='jabber:iq:version'/>
    <feature var='msgoffline'/>
    <feature var='vcard-temp'/>
    <feature var='jabber:iq:register'/>
    <feature var='urn:xmpp:time'/>
    <feature var='jabber:iq:time'/>
    <feature var='jabber:iq:last'/>
    <feature var='jabber:iq:private'/>
    <feature var='http://jabber.org/protocol/disco#info'/>
    <feature var='http://jabber.org/protocol/disco#items'/>
    <feature var='http://jabber.org/protocol/pubsub#publish'/>
    <feature var='jabber:iq:roster'/>
    <feature var='urn:xmpp:carbons:2'/>
    <feature var='urn:xmpp:blocking'/>
    <feature var='http://jabber.org/protocol/commands'/>
</query>
</iq>

<iq to='conference.meet.chuanshaninfo.com'
    id='ada529ea6bb33da70a60d20adf2824872fa612d500000010' 
    type='error'
    from='conferenceduration.meet.chuanshaninfo.com'>
<error type='cancel'>
    <service-unavailable xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
</error>
</iq>

<iq from='focus.meet.chuanshaninfo.com'
    id='ada529ea6bb33da70a60d20adf2824872fa612d50000000c' 
    type='result'
    to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'>
<query xmlns='http://jabber.org/protocol/disco#info'>
    <identity name='Jitsi Meet Focus' category='component' type='generic'/>
    <feature var='http://jabber.org/protocol/disco#info'/>
    <feature var='urn:xmpp:ping'/>
    <feature var='jabber:iq:last'/>
    <feature var='urn:xmpp:time'/>
    <feature var='http://jitsi.org/protocol/focus'/>
</query>
</iq>
```
## 发起语音请求
```xml

<message to='18910221510@meet.chuanshaninfo.com' id='ac091cd9051327e44313eeee827f221f604788870000003a' type='chat'
         from='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop' xmlns='jabber:client'>
    <thread>glooxac091cd9051327e44313eeee827f221f6047888700000013</thread>
    <propose xmlns='urn:xmpp:jingle-message:0' id='1682520421678'>
        <description xmlns='urn:xmpp:jingle:apps:rtp:1' media='audio'/>
    </propose>
</message>
```

## 发起语言初始化
```xml

<iq to='18001278080@meet.chuanshaninfo.com/monocles chat[1.5.13].5nGY'
    id='cc343947a759a05b064e97234247355e0bc4471a0000004d' type='set'
    from='18910221510@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop' xmlns='jabber:client'>
    <jingle xmlns='urn:xmpp:jingle:1' action='session-initiate'
            initiator='18910221510@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop' sid='1682849551882'>
        <content creator='initiator' disposition='session' name='0' senders='both'>
            <description xmlns='urn:xmpp:jingle:apps:rtp:1' media='audio'>
                <payload-type id='111' name='opus' clockrate='48000' channels='2'>
                    <rtcp-fb xmlns='urn:xmpp:jingle:apps:rtp:rtcp-fb:0' type='transport-cc'/>
                </payload-type>
                <payload-type id='63' name='red' clockrate='48000' channels='2'/>
                <payload-type id='9' name='G722' clockrate='8000'/>
                <payload-type id='102' name='ILBC' clockrate='8000'/>
                <payload-type id='0' name='PCMU' clockrate='8000'/>
                <payload-type id='8' name='PCMA' clockrate='8000'/>
                <payload-type id='13' name='CN' clockrate='8000'/>
                <payload-type id='110' name='telephone-event' clockrate='48000'/>
                <payload-type id='126' name='telephone-event' clockrate='8000'/>
                <rtp-hdrext xmlns='urn:xmpp:jingle:apps:rtp:rtp-hdrext:0' id='1'
                            uri='urn:ietf:params:rtp-hdrext:ssrc-audio-level'/>
                <rtp-hdrext xmlns='urn:xmpp:jingle:apps:rtp:rtp-hdrext:0' id='2'
                            uri='http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time'/>
                <rtp-hdrext xmlns='urn:xmpp:jingle:apps:rtp:rtp-hdrext:0' id='3'
                            uri='http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01'/>
                <rtp-hdrext xmlns='urn:xmpp:jingle:apps:rtp:rtp-hdrext:0' id='4'
                            uri='urn:ietf:params:rtp-hdrext:sdes:mid'/>
                <source xmlns='urn:xmpp:jingle:apps:rtp:ssma:0' ssrc='86964338'>
                    <parameter name='cname' value='Kqv1MLGWczS2Szoh'/>
                    <parameter name='msid' value='okedu-audio-id -'/>
                    <parameter name='mslabel' value='okedu-audio-id'/>
                    <parameter name='label' value='-'/>
                </source>
                <ssrc-group xmlns='urn:xmpp:jingle:apps:rtp:ssma:0' semantics='FID'>
                    <source ssrc='86964338'/>
                </ssrc-group>
                <rtcp-mux/>
            </description>
            <transport xmlns='urn:xmpp:jingle:transports:ice-udp:1' pwd='C7YJlMw+Sh3NFeyKOz2dJfGS' ufrag='1Z9v'>
                <fingerprint xmlns='urn:xmpp:jingle:apps:dtls:0' hash='sha-256' setup='actpass'>
                    5A:A3:8E:7C:75:62:43:F6:79:F6:4D:CC:73:0D:2F:98:0F:64:1D:BF:36:0B:E7:45:1F:38:63:13:30:70:65:8B
                </fingerprint>
            </transport>
        </content>
        <group xmlns='urn:xmpp:jingle:apps:grouping:0' semantics='BUNDLE'>
            <content name='0'/>
        </group>
    </jingle>
</iq>

```
## setTransportInfo
```xml

<iq type='set' id='oMgSzFGt0EAl' to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    from='18001278080@meet.chuanshaninfo.com/monocles chat[1.5.13].5nGY'>
    <jingle sid='4965c7cd476dd152639cf02675b2c2fbd0bd08e200000030' 
            action='transport-info' xmlns='urn:xmpp:jingle:1'>
        <content creator='initiator' name='0'>
            <transport pwd='4F9ywNzHAIDYm58ASZV7qwjp' ufrag='3cTO' xmlns='urn:xmpp:jingle:transports:ice-udp:1'>
                <candidate ip='10.29.184.32' 
                           type='host' protocol='udp' foundation='1624237078' generation='0'
                           port='46297' priority='2122129151' component='1'/>
            </transport>
        </content>
    </jingle>
</iq>

<iq type='set' id='HspH8jmt99_C' to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    from='18001278080@meet.chuanshaninfo.com/monocles chat[1.5.13].5nGY'>
<jingle sid='4965c7cd476dd152639cf02675b2c2fbd0bd08e200000030' 
        action='transport-info' xmlns='urn:xmpp:jingle:1'>
    <content creator='initiator' name='0'>
        <transport pwd='4F9ywNzHAIDYm58ASZV7qwjp' ufrag='3cTO' 
                   xmlns='urn:xmpp:jingle:transports:ice-udp:1'>
            <candidate ip='2408:8406:24a0:d7e8:8d4f:31c8:872b:67d1' 
                       type='host' protocol='udp' foundation='1022514418' generation='0' 
                       port='44868' priority='2122197247' component='1'/>
        </transport>
    </content>
</jingle>
</iq>


```

## 修改昵称

```xml

<iq type="set" id="X14cJsEeJXvH"
    from="18910221510@meet.chuanshaninfo.com/monocles chat[1.5.14].5tVT">
    <pubsub xmlns="http://jabber.org/protocol/pubsub">
        <publish node="http://jabber.org/protocol/nick">
            <item id="current">
                <nick xmlns="http://jabber.org/protocol/nick">高杰1510</nick>
            </item>
        </publish>
    </pubsub>
</iq>
```

```xml

<iq id='638afadf1ac0551047f03586b81b6820c7c4cc0600000031' type='set'
    from='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop' xmlns='jabber:client'>
    <pubsub xmlns='http://jabber.org/protocol/pubsub'>
        <publish node='http://jabber.org/protocol/nick'>
            <item id='current'>
                <nick xmlns='http://jabber.org/protocol/nick'>1851024881011</nick>
            </item>
        </publish>
    </pubsub>
</iq>

```

## 创建会议

> 请求

```xml

<body rid='2576793214'
      xmlns='http://jabber.org/protocol/httpbind'
      sid='6c659cd5-0b23-4868-92e4-79ac286f31e5'>
    <iq to='focus.meet.chuanshaninfo.com'
        type='set'
        xmlns='jabber:client'
        id='73aa5505-db99-4fdb-b9aa-90242f8591fc:sendIQ'>
        <conference xmlns='http://jitsi.org/protocol/focus'
                    room='10001@conference.meet.chuanshaninfo.com'
                    machine-uid='4246372528e41bc9b8b133a4a87f0670'>
            <property name='disableRtx' value='false'/>
            <property name='enableLipSync' value='true'/>
            <property name='openSctp' value='true'/>
        </conference>
    </iq>
</body>
```

> 返回结果

```xml

<body xmlns:stream='http://etherx.jabber.org/streams' sid='41c91036-a210-4316-bf33-13a184838d40'
      xmlns='http://jabber.org/protocol/httpbind'>
    <iq type='result' from='focus.meet.chuanshaninfo.com' xmlns='jabber:client'
        to='18510248810@meet.chuanshaninfo.com/A-sqnM4e' id='71153a3a-755e-430b-8f07-8cc8d0e28c57:sendIQ'>
        <conference room='10001@conference.meet.chuanshaninfo.com'
                    ready='true'
                    focusjid='focus@auth.meet.chuanshaninfo.com'
                    xmlns='http://jitsi.org/protocol/focus'>
            <property name='authentication' value='false'/>
        </conference>
    </iq>
</body>
```

## 开启视频

> 请求

```xml

<body rid='1702442171'
      xmlns='http://jabber.org/protocol/httpbind'
      sid='41c91036-a210-4316-bf33-13a184838d40'>
    <presence to='10001@conference.meet.chuanshaninfo.com/18510248810-7389af'
              xmlns='jabber:client'>
        <x xmlns='http://jabber.org/protocol/muc'/>
        <videomuted xmlns='http://jitsi.org/jitmeet/video'>false</videomuted>
        <c xmlns='http://jabber.org/protocol/caps' hash='sha-1'
           node='http://jitsi.org/jitsimeet' ver='vehxP5PJ8h3VqJY2hcg2uVxps00='/>
    </presence>
</body>
```

> 返回

```xml

<body xmlns:stream='http://etherx.jabber.org/streams'
      sid='41c91036-a210-4316-bf33-13a184838d40'
      xmlns='http://jabber.org/protocol/httpbind'>
    <presence xmlns='jabber:client' id='LMLDt-2622494'
              to='18510248810@meet.chuanshaninfo.com/A-sqnM4e'
              from='10001@conference.meet.chuanshaninfo.com/focus'>
        <etherpad xmlns='http://jitsi.org/jitmeet/etherpad'>10001</etherpad>
        <versions xmlns='http://jitsi.org/jitmeet'>
            <component name='focus'>1.0.692-hf</component>
        </versions>
        <conference-properties xmlns='http://jitsi.org/protocol/focus'>
            <property key='support-terminate-restart' value='true'/>
            <property key='created-ms' value='1674859993608'/>
        </conference-properties>
        <c hash='sha-1' node='http://jitsi.org/jicofo' ver='6dKlNV2hTmKYwGcTHveFMk15Ydg='
           xmlns='http://jabber.org/protocol/caps'/>
        <x xmlns='vcard-temp:x:update'>
            <photo/>
        </x>
        <x xmlns='http://jabber.org/protocol/muc#user'>
            <item role='moderator' jid='focus@auth.meet.chuanshaninfo.com/focus30994358825966944' affiliation='owner'/>
        </x>
    </presence>
    <presence xmlns='jabber:client' to='18510248810@meet.chuanshaninfo.com/A-sqnM4e'
              from='10001@conference.meet.chuanshaninfo.com/18510248810-7389af'>
        <videomuted xmlns='http://jitsi.org/jitmeet/video'>false</videomuted>
        <c hash='sha-1' node='http://jitsi.org/jitsimeet' ver='vehxP5PJ8h3VqJY2hcg2uVxps00='
           xmlns='http://jabber.org/protocol/caps'/>
        <x xmlns='vcard-temp:x:update'>
            <photo>f8edc9c6c2e61fb11544c5d7ea0ff06a77c01c9a</photo>
        </x>
        <x xmlns='http://jabber.org/protocol/muc#user'>
            <status code='100'/>
            <item role='participant' jid='18510248810@meet.chuanshaninfo.com/A-sqnM4e'
                  affiliation='none'/>
            <status code='110'/>
        </x>
    </presence>
    <message xmlns='jabber:client' to='18510248810@meet.chuanshaninfo.com/A-sqnM4e'
             from='conferenceduration.meet.chuanshaninfo.com'>
        <json-message xmlns='http://jitsi.org/jitmeet'>
            {&quot;type&quot;:&quot;conference_duration&quot;,&quot;created_timestamp&quot;:1674859994000}
        </json-message>
    </message>
    <message xmlns='jabber:client' to='18510248810@meet.chuanshaninfo.com/A-sqnM4e'
             type='groupchat' from='10001@conference.meet.chuanshaninfo.com'>
        <subject/>
    </message>
</body>
```

## 发送功能

```xml

<iq to='b6324927@conference.meet.chuanshaninfo.com/focus'
    from='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    id='MTg1MTAyNDg4MTBAbWVldC5jaHVhbnNoYW5pbmZvLmNvbS9Pa0VEVS1DbGFzc3Jvb20tRGVza3RvcABEcDF0bi0xNTcyMTQAabXO5rO1iY8lKvnWl13j3Q=='
    type='result' xmlns='jabber:client'>
    <query xmlns='http://jabber.org/protocol/disco#info' node='http://camaya.net/gloox#jAd8aCB3fxsVpqd04u5x3E9pRoU='>
        <identity category='client' type='com.chuanshaninfo.okedu-classroom-desktop'/>
        <feature var='http://jabber.org/protocol/caps'/>
        <feature var='http://jabber.org/protocol/chatstates'/>
        <feature var='http://jabber.org/protocol/disco#info'/>
        <feature var='http://jabber.org/protocol/disco#info'/>
        <feature var='http://jabber.org/protocol/disco#items'/>
        <feature var='http://jabber.org/protocol/disco#items'/>
        <feature var='http://jabber.org/protocol/disco#publish'/>
        <feature var='http://jabber.org/protocol/muc'/>
        <feature var='http://jabber.org/protocol/muc#admin'/>
        <feature var='http://jabber.org/protocol/muc#owner'/>
        <feature var='http://jabber.org/protocol/muc#request'/>
        <feature var='http://jabber.org/protocol/muc#roominfo'/>
        <feature var='http://jabber.org/protocol/muc#rooms'/>
        <feature var='http://jabber.org/protocol/muc#unique'/>
        <feature var='http://jabber.org/protocol/muc#user'/>
        <feature var='http://jabber.org/protocol/nick'/>
        <feature var='http://jabber.org/protocol/pubsub'/>
        <feature var='http://jabber.org/protocol/pubsub#auto-create'/>
        <feature var='http://jabber.org/protocol/pubsub#auto-subscribe'/>
        <feature var='http://jabber.org/protocol/pubsub#event'/>
        <feature var='http://jabber.org/protocol/pubsub#owner'/>
        <feature var='http://jabber.org/protocol/pubsub#publish-options'/>
        <feature var='http://jitsi.org/protocol/focus'/>
        <feature var='http://jitsi.org/protocol/focus'/>
        <feature var='jabber:iq:private'/>
        <feature var='jabber:iq:version'/>
        <feature var='roster:delimiter'/>
        <feature var='storage:bookmarks'/>
        <feature var='storage:rosternotes'/>
        <feature var='urn:xmpp:jingle-message:0'/>
        <feature var='urn:xmpp:jingle:1'/>
        <feature var='urn:xmpp:jingle:1'/>
        <feature var='urn:xmpp:jingle:apps:dtls:0'/>
        <feature var='urn:xmpp:jingle:apps:grouping:0'/>
        <feature var='urn:xmpp:jingle:apps:grouping:0'/>
        <feature var='urn:xmpp:jingle:apps:rtp:1'/>
        <feature var='urn:xmpp:jingle:apps:rtp:1'/>
        <feature var='urn:xmpp:jingle:apps:rtp:audio'/>
        <feature var='urn:xmpp:jingle:apps:rtp:rtcp-fb:0'/>
        <feature var='urn:xmpp:jingle:apps:rtp:rtp-hdrext:0'/>
        <feature var='urn:xmpp:jingle:apps:rtp:ssma:0'/>
        <feature var='urn:xmpp:jingle:apps:rtp:ssma:0'/>
        <feature var='urn:xmpp:jingle:apps:rtp:video'/>
        <feature var='urn:xmpp:jingle:errors:1'/>
        <feature var='urn:xmpp:jingle:transports:ice-udp:1'/>
        <feature var='urn:xmpp:jingle:transports:ice-udp:1'/>
        <feature var='urn:xmpp:ping'/>
        <feature var='urn:xmpp:ping'/>
        <feature var='vcard-temp'/>
    </query>
</iq>
```

## 群聊信息

```xml

<iq type='result' from='test5@conference.meet.chuanshaninfo.com'
    to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    id='dd2563b648620b8da55211c08e23c82e7584c38100000017'>
    <query xmlns='http://jabber.org/protocol/disco#info' node=''>
        <feature var='muc_semianonymous'/>
        <feature var='muc_public'/>
        <feature var='http://jabber.org/protocol/muc#request'/>
        <feature var='muc_open'/>
        <feature var='muc_unsecured'/>
        <feature var='muc_unmoderated'/>
        <feature var='jabber:iq:register'/>
        <feature var='muc_persistent'/>
        <feature var='http://jabber.org/protocol/muc'/>
        <feature var='http://jabber.org/protocol/muc#stable_id'/>
        <feature var='http://jabber.org/protocol/muc#self-ping-optimization'/>
        <identity name='test5' category='conference' type='text'/>
        <x xmlns='jabber:x:data' type='result'>
            <field var='FORM_TYPE' type='hidden'>
                <value>http://jabber.org/protocol/muc#roominfo</value>
            </field>
            <field var='muc#roominfo_changesubject' type='boolean'/>
            <field var='muc#roominfo_description' label='Description' type='text-single'>
                <value/>
            </field>
            <field var='{http://prosody.im/protocol/muc}roomconfig_allowmemberinvites'
                   label='Allow members to invite new members' type='boolean'>
                <value>0</value>
            </field>
            <field var='muc#roominfo_meetingId' label='The meeting unique id.' type='text-single'>
                <value>3be7067c-f41e-4b45-9b0f-938d4806f33d</value>
            </field>
            <field var='muc#roominfo_occupants' label='Number of occupants' type='text-single'>
                <value>3</value>
            </field>
            <field var='muc#roominfo_lang' type='text-single'>
                <value/>
            </field>
            <field var='muc#roomconfig_roomname' label='Title' type='text-single'/>
        </x>
    </query>
</iq>
```

## requestRoomInfo

```xml

<iq type='result' from='test@conference.meet.chuanshaninfo.com'
    to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    id='f006845e96ba71b8c686f10c0302bf4ba0dcecbe0000005a'>
    <query xmlns='http://jabber.org/protocol/muc#owner'>
        <x xmlns='jabber:x:data' type='form'>
            <title>Configuration for test@conference.meet.chuanshaninfo.com</title>
            <instructions>Complete and submit this form to configure the room.</instructions>
            <field var='FORM_TYPE' type='hidden'>
                <value>http://jabber.org/protocol/muc#roomconfig</value>
            </field>
            <field type='fixed'>
                <value>Room information</value>
            </field>
            <field var='muc#roomconfig_roomname' label='Title' type='text-single'/>
            <field var='muc#roomconfig_roomdesc' label='Description' type='text-single'>
                <desc>A brief description of the room</desc>
                <value/>
            </field>
            <field var='muc#roomconfig_lang'
                   label='Language tag for room (e.g. &apos;en&apos;, &apos;de&apos;, &apos;fr&apos; etc.)'
                   type='text-single'>
                <desc>Indicate the primary language spoken in this room</desc>
                <value/>
            </field>
            <field var='muc#roomconfig_persistentroom' label='Persistent (room should remain even when it is empty)'
                   type='boolean'>
                <desc>Rooms are automatically deleted when they are empty, unless this option is enabled</desc>
                <value>1</value>
            </field>
            <field var='muc#roomconfig_publicroom' label='Include room information in public lists' type='boolean'>
                <desc>Enable this to allow people to find the room</desc>
                <value>1</value>
            </field>
            <field type='fixed'>
                <value>Access to the room</value>
            </field>
            <field var='muc#roomconfig_roomsecret' label='Password' type='text-private'>
                <value/>
            </field>
            <field var='muc#roomconfig_membersonly' label='Only allow members to join' type='boolean'>
                <desc>Enable this to only allow access for room owners, admins and members</desc>
            </field>
            <field var='{http://prosody.im/protocol/muc}roomconfig_allowmemberinvites'
                   label='Allow members to invite new members' type='boolean'/>
            <field var='muc#roominfo_meetingId' label='The meeting unique id.' type='text-single'>
                <value>c57447e9-8026-410a-878a-706540f63927</value>
            </field>
            <field type='fixed'>
                <value>Permissions in the room</value>
            </field>
            <field var='muc#roomconfig_changesubject' label='Allow anyone to set the room&apos;s subject'
                   type='boolean'>
                <desc>Choose whether anyone, or only moderators, may set the room&apos;s subject</desc>
            </field>
            <field var='muc#roomconfig_moderatedroom' label='Moderated (require permission to speak)' type='boolean'>
                <desc>In moderated rooms occupants must be given permission to speak by a room moderator</desc>
            </field>
            <field var='muc#roomconfig_whois' label='Addresses (JIDs) of room occupants may be viewed by:'
                   type='list-single'>
                <option label='Moderators only'>
                    <value>moderators</value>
                </option>
                <option label='Anyone'>
                    <value>anyone</value>
                </option>
                <value>moderators</value>
            </field>
            <field type='fixed'>
                <value>Other options</value>
            </field>
            <field var='muc#roomconfig_historylength' label='Maximum number of history messages returned by room'
                   type='text-single'>
                <desc>Specify the maximum number of previous messages that should be sent to users when they join the
                    room
                </desc>
                <value>20</value>
            </field>
            <field var='muc#roomconfig_defaulthistorymessages'
                   label='Default number of history messages returned by room' type='text-single'>
                <desc>Specify the number of previous messages sent to new users when they join the room</desc>
                <value>20</value>
            </field>
        </x>
    </query>
</iq>

```

## 修改聊天室信息

```xml

<iq type="set" id="ozxQpd3mXkbS" from="13975412395@meet.chuanshaninfo.com/monocles chat[1.5.14].GL3J"
    to="test7@conference.meet.chuanshaninfo.com">
    <query xmlns="http://jabber.org/protocol/muc#owner">
        <x type="submit" xmlns="jabber:x:data">
            <title xmlns="jabber:x:data">Configuration for test7@conference.meet.chuanshaninfo.com</title>
            <field var="FORM_TYPE" type="hidden" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">http://jabber.org/protocol/muc#roomconfig</value>
            </field>
            <field type="fixed" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">Room information</value>
            </field>
            <field var="muc#roomconfig_roomname" label="Title" type="text-single" xmlns="jabber:x:data">
                <value>test8y</value>
            </field>
            <field var="muc#roomconfig_roomdesc" label="Description" type="text-single" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data"/>
            </field>
            <field var="muc#roomconfig_lang"
                   label="Language tag for room (e.g. &apos;en&apos;, &apos;de&apos;, &apos;fr&apos; etc.)"
                   type="text-single" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data"/>
            </field>
            <field var="muc#roomconfig_persistentroom" label="Persistent (room should remain even when it is empty)"
                   type="boolean" xmlns="jabber:x:data">
                <value>1</value>
            </field>
            <field var="muc#roomconfig_publicroom" label="Include room information in public lists" type="boolean"
                   xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">1</value>
            </field>
            <field type="fixed" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">Access to the room</value>
            </field>
            <field var="muc#roomconfig_roomsecret" label="Password" type="text-private" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data"/>
            </field>
            <field var="muc#roominfo_meetingId" label="The meeting unique id." type="text-single" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">c09861d3-5c01-4b38-a9df-2286b6e6ff8a</value>
            </field>
            <field var="muc#roomconfig_membersonly" label="Only allow members to join" type="boolean"
                   xmlns="jabber:x:data"/>
            <field var="{http://prosody.im/protocol/muc}roomconfig_allowmemberinvites"
                   label="Allow members to invite new members" type="boolean" xmlns="jabber:x:data"/>
            <field type="fixed" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">Permissions in the room</value>
            </field>
            <field var="muc#roomconfig_changesubject" label="Allow anyone to set the room&apos;s subject" type="boolean"
                   xmlns="jabber:x:data"/>
            <field var="muc#roomconfig_moderatedroom" label="Moderated (require permission to speak)" type="boolean"
                   xmlns="jabber:x:data"/>
            <field var="muc#roomconfig_whois" label="Addresses (JIDs) of room occupants may be viewed by:"
                   type="list-single" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">moderators</value>
            </field>
            <field type="fixed" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">Other options</value>
            </field>
            <field var="muc#roomconfig_historylength" label="Maximum number of history messages returned by room"
                   type="text-single" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">20</value>
            </field>
            <field var="muc#roomconfig_defaulthistorymessages"
                   label="Default number of history messages returned by room" type="text-single" xmlns="jabber:x:data">
                <value xmlns="jabber:x:data">20</value>
            </field>
        </x>
    </query>
</iq>
```

```xml

<iq to='test@conference.meet.chuanshaninfo.com' id='74e09afe1a63a172780bdff22d1f325f95c5ec3100000022' type='set'
    from='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop' xmlns='jabber:client'>
    <query xmlns='http://jabber.org/protocol/muc#owner'>
        <x xmlns='jabber:x:data' type='submit'>
            <title>Configuration for test@conference.meet.chuanshaninfo.com</title>
            <instructions>Complete and submit this form to configure the room.</instructions>
            <field type='hidden' var='FORM_TYPE'>
                <value>http://jabber.org/protocol/muc#roomconfig</value>
            </field>
            <field type='fixed'>
                <value>Room information</value>
            </field>
            <field type='text-single' var='muc#roomconfig_roomname' label='Title'>
                <value>哦哦二哈哈哈er</value>
            </field>
            <field type='text-single' var='muc#roomconfig_roomdesc' label='Description'>
                <desc>A brief description of the room</desc>
                <value/>
            </field>
            <field type='text-single' var='muc#roomconfig_lang'
                   label='Language tag for room (e.g. &apos;en&apos;, &apos;de&apos;, &apos;fr&apos; etc.)'>
                <desc>Indicate the primary language spoken in this room</desc>
                <value/>
            </field>
            <field type='boolean' var='muc#roomconfig_persistentroom'
                   label='Persistent (room should remain even when it is empty)'>
                <desc>Rooms are automatically deleted when they are empty, unless this option is enabled</desc>
                <value>1</value>
            </field>
            <field type='boolean' var='muc#roomconfig_publicroom' label='Include room information in public lists'>
                <desc>Enable this to allow people to find the room</desc>
                <value>1</value>
            </field>
            <field type='fixed'>
                <value>Access to the room</value>
            </field>
            <field type='text-private' var='muc#roomconfig_roomsecret' label='Password'>
                <value/>
            </field>
            <field type='text-single' var='muc#roominfo_meetingId' label='The meeting unique id.'>
                <value>1afcbdc9-97bb-4d5e-a17a-3b8e7cb4a5a2</value>
            </field>
            <field type='boolean' var='muc#roomconfig_membersonly' label='Only allow members to join'>
                <desc>Enable this to only allow access for room owners, admins and members</desc>
                <value>0</value>
            </field>
            <field type='boolean' var='{http://prosody.im/protocol/muc}roomconfig_allowmemberinvites'
                   label='Allow members to invite new members'>
                <value>0</value>
            </field>
            <field type='fixed'>
                <value>Permissions in the room</value>
            </field>
            <field type='boolean' var='muc#roomconfig_changesubject'
                   label='Allow anyone to set the room&apos;s subject'>
                <desc>Choose whether anyone, or only moderators, may set the room&apos;s subject</desc>
                <value>0</value>
            </field>
            <field type='boolean' var='muc#roomconfig_moderatedroom' label='Moderated (require permission to speak)'>
                <desc>In moderated rooms occupants must be given permission to speak by a room moderator</desc>
                <value>0</value>
            </field>
            <field type='list-single' var='muc#roomconfig_whois'
                   label='Addresses (JIDs) of room occupants may be viewed by:'>
                <option label='Anyone'>
                    <value>anyone</value>
                </option>
                <option label='Moderators only'>
                    <value>moderators</value>
                </option>
                <value>moderators</value>
            </field>
            <field type='fixed'>
                <value>Other options</value>
            </field>
            <field type='text-single' var='muc#roomconfig_historylength'
                   label='Maximum number of history messages returned by room'>
                <desc>Specify the maximum number of previous messages that should be sent to users when they join the
                    room
                </desc>
                <value>20</value>
            </field>
            <field type='text-single' var='muc#roomconfig_defaulthistorymessages'
                   label='Default number of history messages returned by room'>
                <desc>Specify the number of previous messages sent to new users when they join the room</desc>
                <value>20</value>
            </field>
            <field type='text-single' var='muc#roomconfig_roomname' label='Title'>
                <value>哦哦二哈哈哈er8</value>
            </field>
        </x>
    </query>
</iq>

```


# 文件
## 请求
```xml

<iq id='9mIqudPbaFOB' to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    from='18001278080@meet.chuanshaninfo.com/monocles chat[1.5.13].5nGY' type='set'>
    <jingle action='session-initiate' sid='IIKW3cq76AqOulj6YUTWRg' xmlns='urn:xmpp:jingle:1'>
        <content senders='initiator' name='y5HsPrmglOY6TGytEhkE1Q' creator='initiator'>
            <description xmlns='urn:xmpp:jingle:apps:file-transfer:3'>
                <offer>
                    <file>
                        <size>82095</size>
                        <name>mmexport1677594591495.jpg</name>
                    </file>
                </offer>
            </description>
            <transport xmlns='urn:xmpp:jingle:transports:ibb:1' 
                        sid='k9XnltRw/0YxyC2sq6pjdQ' 
                        block-size='8192'/>
        </content>
    </jingle>
</iq>

```

```xml

<iq to='13661358184@meet.chuanshaninfo.com/monocles chat[1.5.14].Uf7P'
    id='5dc46f3c60a99a26afb9c9d7440e49e9295ef7e500000036' type='set'
    from='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop' xmlns='jabber:client'>
    <jingle xmlns='urn:xmpp:jingle:1' action='session-initiate'
            initiator='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
            sid='5dc46f3c60a99a26afb9c9d7440e49e9295ef7e500000035'>
        <description xmlns='urn:xmpp:jingle:apps:file-transfer:3'>
            <offer>
                <file>
                    <date/>
                    <name>replay_pid4573.log</name>
                    <desc/>
                    <size>2695864</size>
                    <range offset='-1'/>
                </file>
            </offer>
        </description>
        <transport xmlns='urn:xmpp:jingle:transports:ibb:1' sid='replay_pid4573.log' block-size='4096'/>
    </jingle>
</iq>

```

## 接受
```xml

<iq to='18001278080@meet.chuanshaninfo.com/monocles chat[1.5.13].5nGY'
    id='22cf03d7a7b878f8ec6f34e6e7b0a8c31a6a7e8700000035' type='set'
    from='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop' xmlns='jabber:client'>
    <jingle xmlns='urn:xmpp:jingle:1' action='session-accept'
            responder='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop' sid='7dDod5iRvni2ZXyRs2tiig'>
        <content creator='initiator' disposition='session' name='file' senders='both'>
            <description xmlns='urn:xmpp:jingle:apps:file-transfer:3'>
                <request>
                    <file>
                        <date/>
                        <name>OIP-C.jpg</name>
                        <desc/>
                        <size>31026</size>
                        <range offset='-1'/>
                    </file>
                </request>
            </description>
            <transport xmlns='urn:xmpp:jingle:transports:ibb:1' sid='wmEBp4Xszb7WbkfPMLeI2w' block-size='8192'/>
        </content>
    </jingle>
</iq>


```
## 开启流
```xml
<iq id='k3A0O_3CYZ98'
    to='18510248810@meet.chuanshaninfo.com/OkEDU-Classroom-Desktop'
    type='set'
    from='13661358184@meet.chuanshaninfo.com/monocles chat[1.5.14].Uf7P'>
<open sid='dPrnJ2Zut5tNB12ucxEHQw'
      xmlns='http://jabber.org/protocol/ibb'
      stanza='iq'
      block-size='8192'/>
</iq>


```