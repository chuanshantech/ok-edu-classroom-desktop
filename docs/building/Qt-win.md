# 静态 Qt 
## 下载静态版 Qt
由于需要静态版Qt，网上已经有现成编译好的无需自己再浪费时间构建，
位置：[https://build-qt.fsu0413.me](https://build-qt.fsu0413.me)，
找到版本**5.15.x**最新**静态64位**、**静态64位**、**静态64位**版本，
比如我的：Qt5.15.6-Windows-x86_64-VS2019-16.11.18-**staticFull**-20220915.7z

## 解压到对应位置即可
放置位置：E:\Qt，比如我的：E:\Qt\Qt5.15.6-Windows-x86_64-VS2019-16.11.18-staticFull

## 配置环境变量
该步骤就需要配置环境变量指向Qt安装目录，以便于CMake能识别到Qt，请配置环境变量Qt5Static_HOME，方法如下：
```shell
# 桌面打开命令行（或者按快捷键CTRL+r运行），输入命令`SystemPropertiesAdvanced`回车
QTDIR=E:\Qt\Qt5.15.6-Windows-x86_64-VS2019-16.11.18-staticFull
```

# 动态 Qt
按照Qt安装包正常安装选择：
- msvc64 位运行库即可。


