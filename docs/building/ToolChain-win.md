# Windows 平台编译
> 目前在Windows 10测试通过，最好在Win10以上系统编译开发。

## 安装依赖
下载如下依赖库
- Python  (3+)        https://www.python.org/downloads/
- Perl    (latest)    https://strawberryperl.com/releases.html
- CMake   (latest)    https://cmake.org/download/
- MinGW64 (latest)    https://sourceforge.net/projects/mingw-w64/files/mingw-w64/mingw-w64-release/

## 配置环境变量
上一步，安装完依赖程序之后，打开命令行（或者按快捷键CTRL+r运行），输入命令`SystemPropertiesAdvanced`回车，
设置系统环境变量（用户级和系统级均可），如下
```cmd
# 在原有PATH之上，增加如下（具体路径根据自己的安装路径修改）
PATH=C:\strawberry-perl\perl\bin;C:\Program Files\CMake\bin;C:\MinGW\bin;
```

## Visual studio MSVC构建 
- 安装 Visual Studio 2022（社区版即可）
- 配置选择[桌面开发]
- SDK选择[Windows SDK 10]